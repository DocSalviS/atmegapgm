; Programma di esempio:

.include "atmega_registers.hs"
.section .text

.org 0x0000
.global main

.ifdef DEBUG                   ; Per il debug, loop accorciati
MAXLOOP0=2
MAXLOOP1=2
MAXLOOP2=2
.else
MAXLOOP0=10
MAXLOOP1=0
MAXLOOP2=0
.endif

; Vectors:

; multi-function shield:
; LED:
; D13
; D12
; D11
; D10
;
; beeper:
; D3
;
; Bottoni:
; A1 - A2 - A3
; Per testarli uso il registro A_IN

; Registri:
; r16 - byte alto contatore delay
; r17 - byte intermedio contatore delay
; r18 - byte basso contatore delay
; r19 - prima, maschera D3, poi maschera D10-D13 per il resto del programma
; r20 - Solo D10 ad 1 (valore 4) da aggiunger e al conteggio ad ogni cicolo
; r21 - Contatore da inviare ai LED

; Programa principale
main:
    LDI r19, 1 << D3_BIT        ; Accendo in r19 il solo bit corrispondente a D3 (BEEPER)
    OUT D0_7_MODE, r19          ; Metto in putput D3
    OUT D0_7_PORT, r19          ; Accendo D3 (beeper spento)
    LDI r19, (1 << D10_BIT) | (1 << D11_BIT) | (1 << D12_BIT) | (1 << D13_BIT)  ; Accendo in r19 i bit relativi a D10-D13 (LED)
    OUT D8_13_MODE, r19         ; Metto in output i pin D10-D13
    LDI r20, (1 << D10_BIT)     ; Imposto in r20 il numero da sommare (non esiste la somma ad 8 bit con operando immediato)
    LDI r21, 0x00               ; Imposto in r21 il conteggio a 0
loop:
    EOR r21, r19                ; Inverto i bit del conteggio (i LED sono accesi quando l'uscita è a 0)
    OUT D8_13_PORT, r21         ; Invio la configurazione ail LED
    EOR r21, r19                ; Rimetto il conteggio allo stato precedente
    CP r21, r19                 ; Confronto il conteggio con r19 che ha i bit D10-D13 ad 1 -> 1111 (ultima combinazione
    BRNE nobeep                 ; Se è diverso, salto a spegnere il beeper
    SBIS A_IN, A1_BIT           ; Controllo se il tasto 1 è rilasciato (A1 = 1), se si salto pa prossima istruzione
    CBI D0_7_PORT, D3_BIT       ; Metto a 0 (cancello, clear) il bit corrispondente a D3 nel registro di I/O -> accedo il beeper
    RJMP endbeep                ; Salto alla fine
nobeep:
    SBI D0_7_PORT, D3_BIT       ; Metto a 1 (imposto, set) il bit corrispondente a D3 nel registro di I/O -> spengo il beeper
endbeep:
    ADD r21, r20                ; Sommo un 1 su D10 al conteggio
    AND r21, r19                ; Spengo tramite AND i bit al di fuori di D10-D13 (r19 contiene una maschera con questi bit a 1)
;    CALL delay                  ; Chimao la funzione di ritaro
    RJMP loop                   ; torno al loop
end:
    RJMP end                    ; "loop per bloccare il programma" inutike qui per via del loop precedente

; Routnie di ritardo - esegue 255*255*10 cicli
; Usa r16 - r17 r18 come contatore a 24 bit
; Crea un ritardo di circa un secondo
delay:
    LDI r16, 0x00               ; Azzero il byte alto
dloop0:
    LDI r17, 0x00               ; Azzero il byte medio
dloop1:
    LDI r18, 0x00               ; Azzero il byte basso
dloop2:
    NOP                         ; 5 NOP per creare il "ritardo elementare"
    NOP
    NOP
    NOP
    NOP
    INC r18                     ; Incrementa il byte basso
    CPI r18, MAXLOOP2           ; confronta con il massimo
    BRNE dloop2                 ; Se non è al massimo, trona in loop

    INC r17                     ; se il byte basso è arrivato al massimo, incrementa il byte intermedio
    CPI r17, MAXLOOP1           ; confronta con il massimo
    BRNE dloop1                 ; se enon è al massimo, ritorna in loop

    INC r16                     ; se il byte intermedio è arrivato al massimo, incrementa il byte alto
    CPI r16, MAXLOOP0           ; confronta con il massimo
    BRNE dloop0                 ; se enon è al massimo, ritorna in loop

    ret                         ; Se anche il  byte alto è al massimo, ritorna al chiamante.

