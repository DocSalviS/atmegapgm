; Programma di esempio:

.include "atmega_registers.hs"
.section .text

RAM       = 2048/256  ; for 328P
; RAM       = 1024/256 ; for 168

.org 0x0000
.global main

.ifdef DEBUG                   ; Per il debug, loop accorciati
MAXLOOP0=2
MAXLOOP1=2
MAXLOOP2=2
.else
MAXLOOP0=10
MAXLOOP1=0
MAXLOOP2=0
.endif

; LED Onboard: D13
; Registri:
; r16 - byte alto contatore delay
; r17 - byte intermedio contatore delay
; r18 - byte basso contatore delay
; r19 - maschera D13
; r20 - Configurazione degli output (il bit 5 corrisponde a D13)

; Vectors:
    jmp main                ; RESET - Reset Handler
    jmp phony_isr           ; EXT_INT0 - IRQ0 Handler
    jmp phony_isr           ; EXT_INT1 - IRQ1 Handler
    jmp phony_isr           ; PCINT0 - PCINT0 Handler
    jmp phony_isr           ; PCINT1 - PCINT1 Handler
    jmp phony_isr           ; PCINT2 - PCINT2 Handler
    jmp phony_isr           ; WDT - Watchdog Timer Handler
    jmp phony_isr           ; TIM2_COMPA - Timer2 Compare A Handler
    jmp phony_isr           ; TIM2_COMPB - Timer2 Compare B Handler
    jmp phony_isr           ; TIM2_OVF - Timer2 Overflow Handler
    jmp phony_isr           ; TIM1_CAPT - Timer1 Capture Handler
    jmp phony_isr           ; TIM1_COMPA - Timer1 Compare A Handler
    jmp phony_isr           ; TIM1_COMPB - Timer1 Compare B Handler
    jmp phony_isr           ; TIM1_OVF - Timer1 Overflow Handler
    jmp phony_isr           ; TIM0_COMPA - Timer0 Compare A Handler
    jmp phony_isr           ; TIM0_COMPB - Timer0 Compare B Handler
    jmp phony_isr           ; TIM0_OVF - Timer0 Overflow Handler
    jmp phony_isr           ; SPI_STC - SPI Transfer Complete Handler
    jmp phony_isr           ; USART_RXC - USART, RX Complete Handler
    jmp phony_isr           ; USART_UDRE - USART, UDR Empty Handler
    jmp phony_isr           ; USART_TXC - USART, TX Complete Handler
    jmp phony_isr           ; ADC - ADC Conversion Complete Handler
    jmp phony_isr           ; EE_RDY - EEPROM Ready Handler
    jmp phony_isr           ; ANA_COMP - Analog Comparator Handler
    jmp phony_isr           ; TWI - 2-wire Serial Interface Handler
    jmp phony_isr           ; SPM_RDY - Store Program Memory Ready Handler

; phony ISR
phony_isr:
    RETI                        ; Returns from spurious interrupt

; Program
main:
; Al RESET potrebbe essere inutile perché è già impostato a RAMEND di default
;    SER r16                     ; Mette a 0xFF la parte bassa di SP
;    OUT SPH,r16
;    LDI r16, RAM
;    OUT SPL,r16                 ; Set Stack Pointer to top of RAM

    LDI r19, (1 << D13_BIT)     ; Predispone la maschera per D13
    OUT D8_13_MODE, r19         ; Mette in outputo il piedino D13
    LDI r20, 0x00               ; Azzera il byte degli output
loop:
    OUT D8_13_PORT, r20         ; Mette in output il dato
    EOR r20, r19                ; inverte il bit di D13 (r19 = 1 << D13_BIT)
    CALL delay                  ; Esegue il ritardo
    RJMP loop                   ; Resta in loop
end:
    RJMP end                    ; "loop per bloccare il programma" inutike qui per via del loop precedente

; Routnie di ritardo - esegue 255*255*10 cicli
; Usa r16 - r17 r18 come contatore a 24 bit
; Crea un ritardo di circa un secondo
delay:
    LDI r16, 0x00               ; Azzero il byte alto
dloop0:
    LDI r17, 0x00               ; Azzero il byte medio
dloop1:
    LDI r18, 0x00               ; Azzero il byte basso
dloop2:
    NOP                         ; 5 NOP per creare il "ritardo elementare"
    NOP
    NOP
    NOP
    NOP
    INC r18                     ; Incrementa il byte basso
    CPI r18, MAXLOOP2           ; confronta con il massimo
    BRNE dloop2                 ; Se non è al massimo, trona in loop

    INC r17                     ; se il byte basso è arrivato al massimo, incrementa il byte intermedio
    CPI r17, MAXLOOP1           ; confronta con il massimo
    BRNE dloop1                 ; se enon è al massimo, ritorna in loop

    INC r16                     ; se il byte intermedio è arrivato al massimo, incrementa il byte alto
    CPI r16, MAXLOOP0           ; confronta con il massimo
    BRNE dloop0                 ; se enon è al massimo, ritorna in loop

    ret                         ; Se anche il  byte alto è al massimo, ritorna al chiamante.

