.section .text

.org 0x0000
.global main

main:
    LDI r18, 0xff
    OUT 0x04, r18
    LDI r18, 0x00
loop:
    OUT 0x05, r18
    INC r18
    CALL delay
    RJMP loop
end:
    RJMP end 


delay:
    LDI r16, 0x00
    LDI r17, 0x00
dloop1:
dloop2:
    NOP
    NOP
    NOP
    NOP
    NOP
    INC r17
    CPI r17, 0xff
    BRNE dloop2

    INC r16
    CPI r16, 0xff
    BRNE dloop1
    ret


