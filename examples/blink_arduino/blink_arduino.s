; Programma di esempio:

.include "atmega_registers.hs"
.section .text


.org 0x0000
.global main

.ifdef DEBUG                   ; Per il debug, loop accorciati
MAXLOOP0=2
MAXLOOP1=2
MAXLOOP2=2
.else
MAXLOOP0=10
MAXLOOP1=0
MAXLOOP2=0
.endif

; Vectors:

; LED Onboard: D13
; Registri:
; r16 - byte alto contatore delay
; r17 - byte intermedio contatore delay
; r18 - byte basso contatore delay
; r19 - maschera D13
; r20 - Configurazione degli output (il bit 5 corrisponde a D13)

; Program
main:
    LDI r19, (1 << D13_BIT)     ; Predispone la maschera per D13
    OUT D8_13_MODE, r19         ; Mette in outputo il piedino D13
    LDI r20, 0x00               ; Azzera il byte degli output
loop:
    OUT D8_13_PORT, r20         ; Mette in output il dato
    EOR r20, r19                ; inverte il bit di D13 (r19 = 1 << D13_BIT)
    CALL delay                  ; Esegue il ritardo
    RJMP loop                   ; Resta in loop
end:
    RJMP end                    ; "loop per bloccare il programma" inutile qui per via del loop precedente

; Routnie di ritardo - esegue 255*255*10 cicli
; Usa r16 - r17 r18 come contatore a 24 bit
; Crea un ritardo di circa un secondo
delay:
    LDI r16, 0x00               ; Azzero il byte alto
dloop0:
    LDI r17, 0x00               ; Azzero il byte medio
dloop1:
    LDI r18, 0x00               ; Azzero il byte basso
dloop2:
    NOP                         ; 5 NOP per creare il "ritardo elementare"
    NOP
    NOP
    NOP
    NOP
    INC r18                     ; Incrementa il byte basso
    CPI r18, MAXLOOP2           ; confronta con il massimo
    BRNE dloop2                 ; Se non è al massimo, trona in loop

    INC r17                     ; se il byte basso è arrivato al massimo, incrementa il byte intermedio
    CPI r17, MAXLOOP1           ; confronta con il massimo
    BRNE dloop1                 ; se enon è al massimo, ritorna in loop

    INC r16                     ; se il byte intermedio è arrivato al massimo, incrementa il byte alto
    CPI r16, MAXLOOP0           ; confronta con il massimo
    BRNE dloop0                 ; se enon è al massimo, ritorna in loop

    RET                         ; Se anche il  byte alto è al massimo, ritorna al chiamante.

