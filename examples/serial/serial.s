; Programma di esempio:
; Seriale:
; Baud Rate: 9600   UCSR0A: 2 UBRR0: 0  - 207
; Baud Rate: 19200  UCSR0A: 2 UBRR0: 0  - 103
; Baud Rate: 38400  UCSR0A: 2 UBRR0: 0  - 51
; Baud Rate: 57600  UCSR0A: 0 UBRR0: 0  - 16
; Baud Rate: 115200 UCSR0A: 2 UBRR0: 0  - 16
; Baud Rate: 230400 UCSR0A: 2 UBRR0: 0  - 8


.include "atmega_registers.hs"
.section .text

; R17 - R16 per boudtate e inizializzazione registri

.org 0x0000
.global main

; Vectors:


; Program
main:
; Serial init
    ldi r17, 0
    ldi r16, 16; 115200 Baud - 16, double rate (207; 9600 Baud double rate)
    ; Set baud rate
    sts UBRR0H, r17
    sts UBRR0L, r16
    ; Set Double clock
    ldi r16, (1<<U2X0)
    sts UCSR0A,r16
    ; Enable receiver and transmitter
    ldi r16, (1<<RXEN0)|(1<<TXEN0)
    sts UCSR0B,r16
    ; Set frame format: 8data, 1stop bit
    ldi r16, (3<<UCSZ00)
    sts UCSR0C,r16
loop:

USART_Receive:
; Wait for data to be received
    lds r16, UCSR0A
    sbrs r16, RXC0
    rjmp USART_Receive
    ; Get and return received data from buffer
    lds r17, UDR0

USART_Transmit:
    ; Wait for empty transmit buffer
    lds r16, UCSR0A
    sbrs r16, UDRE0
    rjmp USART_Transmit
    ; Put data (r17) into buffer, sends the data
    sts UDR0,r17

    RJMP loop                   ; Resta in loop
end:
    RJMP end                    ; "loop per bloccare il programma" inutile qui per via del loop precedente

