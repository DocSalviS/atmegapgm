/*
 * Linguaggio Javascript
 */

const MAXWORDS  = 2
const PROG_SIZE = 0x4000

 class PgmComponent {
    constructor(avr, size) {
        this.element = document.getElementById("codeWindow");
        this.avr = avr;
        this.size = size;
        this.cursor = 0;
        this.start_addr = 0;
        this.next_addr = 0;
        this.width = 45;
        this.height = 16;
    }

    hex16 (num) {
        return "0x" + ("0000" + num.toString(16)).slice(-4);
    }

    hex8 (num) {
        return "0x" + ("00" + num.toString(16)).slice(-2);
    }

    draw() {
        var cursor = this.start_addr;

        if (this.cursor < cursor || this.cursor > this.next_addr) {
            cursor = this.cursor;
        } else if (this.cursor == this.next_addr) {
            cursor += this.avr.getInstructionAtSize(cursor);
        }
    
        this.start_addr = cursor;
        var content = "";
    
        for (var i = 1; i < this.height-1 && cursor<this.size; i++) {    
            var size = this.avr.getInstructionAtSize(cursor);
            if (cursor == this.cursor) {
                content += '<span class="reverse">';
            }

            // SALVI 30/3/2024
            // Aggiunta la colonna con l'indirizzo
            content += this.hex16(cursor) + " - ";

            for(var j=0; j<MAXWORDS; j++) {
                content += (j< size) ? this.hex16(this.avr.getCodeWord(cursor + j)) + " " : "       ";
            }

            var mnemSize = this.avr.getInstructionAtAsmLen(cursor);
            for (var j = 0; j < mnemSize; j++) {
                content += String.fromCharCode(this.avr.getInstructionAtAsmChar(j));
            }
/*
            mvwprintw(this->win, i, width/2,
                "%s", op.to_string(this->data, cursor).c_str()

            );
 */    
            if (cursor == this.cursor) {
                content += '</span>';
            }
            content += "\n";
            cursor += size;
        }
        this.next_addr = cursor;
        // wattroff(this->win, COLOR_PAIR(STANDARD_PAIR));
        this.element.innerHTML=content;
    }

    set_cursor(cursor) {
        this.cursor = cursor;    
    }


    get_hex(s, start, len) {
        return parseInt(s.substring(start, start+len),16);
    }

    loadHex(hexFile) {
        this.avr.clearCode();
        // File hex Intel
        var file_size = 0;
        var cur = 0;
        var end = hexFile.length;
        while(cur < end) {
            if(hexFile.substring(cur, cur+1) == ":") {    // Data
                cur ++;
                var len = this.get_hex(hexFile, cur, 2);
                cur += 2;
                var addr = this.get_hex(hexFile, cur, 4);
                cur += 4
                if (this.get_hex(hexFile, cur, 2) == 1) {  // Record Fine dati
                    break;
                }
                cur += 2;
                for (var i = 0;i < len; i++) {
                    if (addr < PROG_SIZE) {
                        var byte = this.get_hex(hexFile, cur, 2);
                        this.avr.setCodeByte(addr, byte);
                        // message(this.hex16(addr) + ":" + this.hex8(byte));
                        addr ++;
                        cur += 2;
                        if (addr > file_size) {
                            file_size = addr;
                        }
                    }
                }
                // Non verifico il checksum - mi fido
            } else {    // Salta i caratteri extra fino a trovare il ":"
                cur ++;
            }
        }
        return file_size;
    }
 }