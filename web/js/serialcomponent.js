/*
 * Linguaggio Javascript
 */

const SERIAL_LINELEN = 40;
const SERIAL_LINENUM = 4;

class SerialComponent {
    constructor(avr) {
        this.avr = avr;
        this.inbuffer = "";
        this.outlines = [ "", "", "", ""];
        this.inbuffsize = SERIAL_LINELEN;
        this.scrolled = false;   
        this.input=document.getElementById("SerialInText");   
        this.inputOverflow=document.getElementById("SerialInOverflow");
        this.output=document.getElementById("SerialOutWindow");
        avr.setBufferSize(SERIAL_LINELEN*SERIAL_LINENUM);
    }

    read(ch) {
        if(ch.charCodeAt(0) == 10 || ch.charCodeAt(0) == 13) {  // CR o LF
            this.scrolled = false;
            this.inbuffer="";
        } else {
            if (ch >= ' ') {
                this.inbuffer += ch;
            } else {
                this.inbuffer += '[' + ("00" + ch.charCodeAt(0).toString(16)).slice(-2) + ']';
            }
            while (this.inbuffer.length >= this.inbuffsize-1) {
                this.inbuffer = this.inbuffer.split(1);
                this.scrolled = true;
            }
        }
        this.draw();
        this.avr.serial_receive(ch.charCodeAt(0));
    }

    draw() {
        var inprefix="";
        this.inputOverflow.innerHTML = (this.scrolled) ? "&gt;" : "";
        this.input.innerHTML = inprefix + this.inbuffer;
        var content = "";
        var len= this.avr.getSerialCharsAvailable()
        for (var i=0; i < len;i++) {
            content += String.fromCharCode(this.avr.getSerialTransmittedCharAt(i));
        }
        var rawlines = content.split(/\r\n|\r|\n/);
        var len = rawlines.length;
        var lines = [];
        for (var i = 0; i < len; i++) {
            var line = rawlines[i];
            while (line.length > SERIAL_LINELEN) {
                lines.push(line.slice(0, SERIAL_LINELEN));
                line=line.slice(SERIAL_LINELEN);
            }
            lines.push (line);
        }
        len = lines.length;
        var start = (len > SERIAL_LINENUM) ? len - SERIAL_LINENUM : 0;
        var term = "";
        content = "";
        for (var i = start; i < len; i++) {
            content += term +lines[i]
            term = "\n";
        }
        
        // var endline="";
        // for (var i = 0; i < this.outlines.length; i++) {
        //     content += endline + this.outlines[i];
        //     endline = "\n";
        // }
        this.output.innerHTML = content;
    }

    refresh() {

    }
}