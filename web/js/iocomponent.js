/*
 * Linguaggio Javascript
 */

/* IO Ports 8 bits each
 *
 * Arduino ports      PORT    DDR     BIT
 * Porta C -> A0-A5   0x08    0x07    0x06
 * Porta B -> D8-D13  0x05    0x04    0x03
 * Porta D -> D0-D7   0x0B    0x0A    0x09
 *
 */
const PORTBITS        = 8;

class ioPort {
    constructor(avr, element, base, port, lines) {
        this.avr = avr;
        this.element = document.getElementById(element);
        this.base=base + 0x20;  // indirizzo nell'area di I/O
        this.port=port;
        this.lines = lines;
    }

    draw () {
        var content = "";
        for (var i = 0; i < this.lines.length; i++) {
            content += "  " + this.lines[i] + "\n";
        }
        content += " " + this.port.toString();
        for(var i = 0; i < PORTBITS; i++) {
            var mask = 0x80 >> i;
            content += (this.avr.getDataByte(this.base)&mask) ? ((this.avr.getDataByte(this.base-1)&mask) ? '*' : 'U') : ((this.avr.getDataByte(this.base-1)&mask) ? 'O' : '-');
        }
        content += "\n  ";
        for(var i = 0; i < PORTBITS; i++) {
            var mask = 0x80 >> i;
            content += (this.avr.getDataByte(this.base-2)&mask) ? ((this.avr.getDataByte(this.base-1)&mask) ? ' ' : 'I') : ((this.avr.getDataByte(this.base-1)&mask) ? ' ' : '_');
          }
      
        this.element.innerHTML=content;
    }
    setInput(bit, value) {
        var mask = 1 << bit;
        if(!(this.avr.getDataByte(this.base-1)&mask)) {
            this.avr.setDataByte(this.base-2, this.avr.getDataByte(this.base-2) | mask);
            if (value == 0) {
                this.avr.setDataByte(this.base-2, this.avr.getDataByte(this.base-2) ^ mask);
            }
            this.draw();
        }
    }
    
    update_io_state() {
    }
    
}

class I_OComponent {
    constructor(avr) {
        this.ports = [
            new ioPort(avr, "i_oPortCData", 0x08, "C",[ "        ", "--AAAAAA", "--543210"]),
            new ioPort(avr, "i_oPortBData", 0x05, "B",[ "--DDDDDD","--111100","--321098"]),
            new ioPort(avr, "i_oPortDData", 0x0B, "D",[ "DDDDDDDD","00000000","76543210"])
        ]
    }

    draw() {
        for (var i=0; i<this.ports.length; i++) {
            this.ports[i].draw();
        }
    }

    update_io_state() {
        for(var i=0; i<this.ports.length; i++) {
            this.ports[i].update_io_state();
        }
    }

    setInput(button, value) {
        this.ports[0].setInput(button, value);
    }
   
}