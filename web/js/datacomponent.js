/*
 * Linguaggio Javascript
 */
/*
 * Linguaggio Javascript
 */
const REGLIMIT       = 0x20;
const IOLIMIT        = 0x60;
const RD_OPERATION   = 1;
const WR_OPERATION   = 2;
const RDWR_OPERATION = 3;
const NOADDRESS      = 0xffffffff;

class DataComponent {
    constructor (avr, size) {
        this.width = 45;
        this.height = 14;
        this.avr = avr;
        this.size = size;
        this.mem_wr = 0;
        this.mem_rd = 0;
        this.selected = 0;
        this.dreg = 0;
        this.dreg_op = 0;
        this.dataHead = document.getElementById("dataHead");
        this.dataWindow = document.getElementById("dataWindow");
    }

    get_num_bytes(width) {
        var widths = [ 32, 16, 8, 4 ];// Only allowed widths = 4 / 8 / 16 / 32
        var num_bytes = (width-9)/4;
        var i;
        for (i = 0; i < widths.length; i++) {
            if (num_bytes >= widths[i]) {
                return widths[i];
            }
        }
        return num_bytes;
    }

    hex16 (num) {
        return "0x" + ("0000" + num.toString(16)).slice(-4);
    }
 
    hex8 (num) {
        return ("00" + num.toString(16)).slice(-2);
    }
   
    draw() {
        var i;
        var height = this.height - 2;
        var num_bytes = this.get_num_bytes(this.width);
    
        var cursor = this.selected;
    
        var content = this.dreg_op.toString() + "      ";
        for(i = 0; i < num_bytes; i++) {
            content += " x0" + i.toString();
        }
        this.dataHead.innerHTML = content;
        content = "";

        for (var i = 1; i < height; i++) {
            if ((cursor < IOLIMIT)) {
                content += '<span class="' + ((cursor < REGLIMIT) ? "registers" : "i_o" ) + '">';
            }
            content += this.hex16(cursor) + " ";
            for (var j = 0; j < num_bytes && cursor < this.size; j++, cursor++) {
                content += "  ";
                if ((cursor == this.mem_wr || cursor == this.mem_rd || cursor == this.dreg || cursor == this.dreg+1)) {
                    var hiclass = "";
                    if (cursor == this.mem_wr) {
                        hiclass = "reverse"
                    } else if (cursor == this.mem_rd) {
                        hiclass = "rd_mem"
                    } else {
                        switch (this.dreg_op) {
                            case RD_OPERATION:
                                hiclass = "rd_pair"
                                break;
                            case WR_OPERATION:
                                hiclass = "reverse"
                                break;
                            case RDWR_OPERATION:
                                hiclass = "rdwr_pair"
                                break;
                        }
                    }
                    content += '<span class="' + hiclass + '">';
                }

                content += this.hex8(this.avr.getDataByte(cursor))

                if ((cursor == this.mem_wr || cursor == this.mem_rd || cursor == this.dreg || cursor == this.dreg+1)) {
                    content += "</span>";
                }
            }
            content += "   ";
            if ((cursor < IOLIMIT)) {
                content += "</span>";
            }
            content += "\n"; 
        }

        this.dataWindow.innerHTML = content;
    }
    
    select(index) {
        var num_bytes = this.get_num_bytes(this.width);
        var height = this.height - 3;
        var index = (index / num_bytes) | 0;    // Converte in intero
        index *= num_bytes; // maintain alignment to num_bytes
    
        if (index < num_bytes*this.height) {    // If it remains in the first page
            this.selected = 0;
        } else if (index > this.size - num_bytes*this.height) {  // If it is in the last page
            this.selected = this.size - num_bytes*this.height;
        } else if (this.selected < index) { // If it's before te firs line
            this.selected = index;
        } else if (index >= this.selected + num_bytes*this.height) {    // It's after the last line
            this.selected = index - num_bytes*(this.height - 1);
        }
    }
    
    set_wr_address(index) {
        this.mem_wr = index;
    }
    
    set_rd_address(index) {
        this.mem_rd = index;
    }
    
    set_dreg_address(index, operation) {
        this.dreg = index;
        this.dreg_op = operation;
    }    
}
