/*
 * Linguaggio Javascript
 */


function onLoad() {
    msg = document.getElementById("msgPanel");
}

function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}
  
function getFileDialog() {
    var inputFile = document.getElementById("file-input");
    inputFile.value="";
    inputFile.click();
}

function handleSelect(e) {
    var file = e.target.files[0];
    if (!file) {
      return;
    }
    loadFile(file);
}

function message(m) {
    msg.innerHTML += m + "<br>";
    msg.scrollIntoView(false);
}

function clearMsg() {
    msg.innerHTML="";
}

var runTimer = null;

function runProgram() {
    if (runTimer == null) {
      runTimer = setInterval(function () { emulator.step(); } , 100);
      document.getElementById("buttonRun").value="Stop";
    } else {
      clearInterval(runTimer);
      runTimer = null;
      document.getElementById("buttonRun").value="Run";
    }
}

function handleFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();

    var files = evt.dataTransfer.files; // FileList object.

    // files is a FileList of File objects. List some properties.
    loadFile(files[0]);
}

function assemble(name, hexFile) {
  var ajax = new XMLHttpRequest();
  var formData = new FormData();
  formData.append('name', name);
  formData.append('content', hexFile);
  ajax.open('POST', 'assemble.php', true);
  ajax.onreadystatechange = function() {
    if (this.readyState == 4) {
      if (this.status == 200) {
        try {
          var r = JSON.parse(this.responseText);
          for (var i = 0; i < r.messages.length; i++) {
            message(r.messages[i]);
          }
          if (r.errore == 0) {
            var pgmLen = emulator.loadHex(r.hexfile);
            message('Programma lungo ' + pgmLen.toString() + ' bytes');
          }
        } catch(e) {
          message("<span class=\"error\">ERRORE: " + e.message + "</span><br>" + this.responseText);
        }
      } else {
        message("<span class=\"error\">ERRORE HTML: " + this.status + "<br>" + this.responseText + "</span>");
      }
    }
  };
  message('<span class=\"message\">Caricato un sorgente assembler, Lo invio all&apos;assemblatore</span>');
  ajax.send(formData);
}

function loadFile(f) {
    clearMsg();
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(f.name)[1];
    message('Carico <strong>' + escape(f.name) + '</strong> (' +  ext + ") " + f.size.toString() + ' bytes');
    if (ext == "hex") {
      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          var hexFile = e.target.result;
          var pgmLen = emulator.loadHex(hexFile);
          message('Programma lungo ' + pgmLen.toString() + ' bytes');
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsText(f);
    } else if(ext == "s") {
      if(window.location.protocol == 'file:') {
        message('La pagina &egrave; eseguita in locale, quindi non posso richiamare l&apos;assemblatore');
      } else {
        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function(theFile) {
          return function(e) {
            assemble(f.name, e.target.result);
          };
        })(f);

        reader.readAsText(f);

      }
    } else {
      message('File di tipo non utilizzabile. Ignoro');
    }
}

function buttonChange(element, button) {
    var state = element.checked;
    emulator.setInput(button, state);
}

