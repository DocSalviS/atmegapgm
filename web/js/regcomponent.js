/*
 * Linguaggio Javascript
 */

class RegComponent {
    constructor () {
        var i;
        this.pc = 0;
        this.sp = 0;
        this.sreg = 0;
        this.sp_highlight = false;
        this.sreg_highlight = 0;

        this.reg_pc = document.getElementById("reg_pc");
        this.reg_sp = document.getElementById("reg_sp");
        this.reg_flags = [];
        for (i = 0; i < 8; i++) {
            this.reg_flags[i] = document.getElementById("reg_flags_" + structuredClone(i));
        }
    }

    hex16 (num) {
        return "0x" + ("0000" + num.toString(16)).slice(-4);
    }

    draw () {
        var i;
        this.reg_pc.innerHTML=this.hex16(this.pc);
        this.reg_sp.style.class= (this.sp_highlight) ? "reverse" : "";
        this.reg_sp.innerHTML=this.hex16(this.sp);
     
        var sreg_names = [
            ["c", "z", "n", "v", "s", "h", "t", "i"],
            ["C", "Z", "N", "V", "S", "H", "T", "I"]
        ];
        for (i = 0; i < 8; i++) {
            this.reg_flags[i].style.class = (this.sreg_highlight & (1 << i)) ? "reverse" : "";
            this.reg_flags[i].innerHTML = sreg_names[(this.sreg & (1 << i)) ? 1 : 0][i];
        }
    }

    set_pc(pc) {
        this.pc = pc;
    }
    
    set_sp(sp) {
        this.sp = sp;
    }
    
    set_sreg(sreg) {
        this.sreg = sreg;
    }
    
    set_sp_highlight(highlight) {
        this.sp_highlight = highlight;
    }
    
    set_sreg_highlight(highlight) {
        this.sreg_highlight = highlight;
    }
}