
.section .text

.org 0x0000
.global main

main:
    ldi r18, 0x00
    ldi r19, 0x08
    rcall increase
    inc r20
    ;break
    rjmp main
    ret

increase:
    inc r18
    cpse r18, r19
    rjmp increase
    ret
