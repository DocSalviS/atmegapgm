<?php
/* assemble.php
 * Riceve dall'emulatore un file .s e lo assembla producendo un .hex
 * Richiede che siano installati avr-gcc e avr-objcopy
 * Occorre installare gcc-avr e	binutils-avr
 * apt install gcc-avr binutils-avr
 * Occorre copiare il file 
 * /usr/share/atmegapgm/include/atmega_registers.hs
 * che fa parte del pacchetto dell'emulatore.
 * 
 * Ingresso (post):
 * name -> nome file
 * content -> contenuto file
 * Uscita:
 * un JSON con questa struttura:
 * errore: booleano
 * hexfile: stringa - file compilato
 * messages: array di stringhe - messaggi di compilazione
 * 
 * Comandi per atmega (per 'emulate' si 'serial.o')
 * avr-gcc -mmcu=atmega328p -nostdlib -Xassembler -I -Xassembler '/usr/share/atmegapgm/include' -Xassembler -aln=target.lst -Xassembler --defsym -Xassembler DEBUG=1 -o serial_dbg.o serial.s
 * avr-objcopy -O ihex --only-section=.text serial_dbg.o serial_dbg.hex
 */

function error ($msg) {
  return array("errore"=>-1,"hexfile"=>"", "hexname"=>"", "messages"=>array("<span class=\"error\">ERRORE: $msg</span>"));
}

function tempdir(int $mode = 0700): string {
    do { $tmp = sys_get_temp_dir() . '/' . mt_rand(); }
    while (!@mkdir($tmp, $mode));
    return $tmp;
}

function launch($command, &$return_value, $stdin = null) {
  $descriptorspec = array(
     0 => array("pipe", "r"),  // stdin
     1 => array("pipe", "w"),  // stdout
     2 => array("pipe", "w"),  // stderr
  );
  $process = proc_open($command, $descriptorspec, $pipes, dirname(__FILE__), null);

  if ($stdin != null) {
    fwrite($pipes[0], $stdin);
  }

  $stdout = stream_get_contents($pipes[1]);

  $stderr = stream_get_contents($pipes[2]);

  fclose($pipes[0]);
  fclose($pipes[1]);
  fclose($pipes[2]);
  $return_value = proc_close($process);
  if ($stdin == null) {
    $msgs = array_merge(explode("\n", $stdout), explode("\n", $stderr));
  } else {
    $msgs = explode("\n", $stderr);
  }

  return $msgs;
}

function assemble () {
  if (!array_key_exists("name", $_REQUEST) || !array_key_exists("content", $_REQUEST)) {
    return error("parametri scorretti");
  }
  $name = $_REQUEST["name"];
  $content = $_REQUEST["content"];
  $messages = array();
  $hexfile = "";
  $errore = -1;
  $assembler = "/usr/bin/avr-gcc";
  $objcopy = "/usr/bin/avr-objcopy";

  //Canonicizza il nome
  $name = preg_replace("|^(.*[/])?([^/]*)$|", "\\2", $name);
  $name = preg_replace("/[^-_.a-zA-Z]/","_", $name);
  $ext = preg_replace("/^.*\\.([^.]*)$/","\\1", $name);
  if (strtolower($ext) != 's') {
    return error("Il file $name non &egrave; un sorgente assembler: non ha l'estensione corretta");
  }
  $base = preg_replace("/^(.*)\\.[^.]*$/","\\1", $name);
  $relname = $base . "_dbg.o";
  $hexname = $base . "_dbg.hex";
  // Controllo se esistono un assembler (as-hc08 o sdas6808) ed un linker (aslink o sdld)
  if (!is_executable($assembler)) {
    $assembler = null;
    $messages[] = "<span class=\"error\">ERRORE: non trovo un assemblatore utilizzabile</span>";
  }
  if (!is_executable($objcopy)) {
    $objcopy = null;
    $messages[] = "<span class=\"error\">ERRORE: non trovo un utility per estrarre il codice utilizzabile</span>";
  }
  if ($assembler != null && $objcopy != null) {
    // Creo una directory temporanea
    $dir = tempdir();
    // Ci creo dentro il file $name con il contenuto $content
    file_put_contents("$dir/$name", $content);
    // lancio l'assemblatore catturandone l'output
    $messages[] = "<span class=\"message\">Avvio l'assemblatore</span>";
    // $output = launch("$assembler  -lpo $dir/$name", $return_var);
    $output = launch("$assembler  -mmcu=atmega328p -nostdlib -Xassembler -I -Xassembler '/usr/share/atmegapgm/include' -Xassembler --defsym -Xassembler DEBUG=1 -o $dir/$relname $dir/$name", $return_var);
    $messages = array_merge($messages, $output);
    // se l'assemblatore ha dato OK
    if ($return_var) {
      $messages[] = "<span class=\"error\">ERRORE $return_var: L'assemblatore ha trovato errori nel sorgente</span>";
    } else {
      // richiamo il linker, catturandone i messaggi e salvando in $hexfilename
      $messages[] = "<span class=\"message\">Avvio il linker</span>";
      // $output = launch("$linker  -c ", $return_var, "-s\n$dir/$relname; echo \n-e\n");
      $output = launch("$objcopy -O ihex --only-section=.text $dir/$relname $dir/$hexname", $return_var);
      $messages = array_merge($messages, $output);
      if ($return_var) {
        $messages[] = "<span class=\"error\">ERRORE $return_var: Il Linker ha trovato errori</span>";
      } else {
        // leggo il file $hexfilename mettendone il contenuto in $hexfile
        $file = null;
        if (!is_readable("$dir/$hexname")) {
          $hexname="";
        }
        if ($hexname == "") {
          $messages[] = "<span class=\"error\">ERRORE Non trovo il file hex estratto dal file oggetto</span>";
        } else {
          $hexfile = file_get_contents("$dir/$hexname");
          // imposto l'errore a 0
          $errore = 0;
          $messages[] = "<span class=\"message\">Elaborazione terminata</span>";
        }
      }
    }
    // Cancello la directory temporanea
    foreach (scandir ($dir) as $f) {
      if ($f != '.' && $f != '..') {
        // $messages[] = "Cancello $f";
        unlink ("$dir/$f");
      }
    }
    rmdir($dir);
  }
  return array("errore"=>$errore,"hexfile"=>$hexfile, "hexname"=>$hexname, "messages"=>$messages);
}

print json_encode(assemble());

?>
