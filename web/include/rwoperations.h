#ifndef __RWOPERATIONS_H__
#define __RWOPERATIONS_H__

#define REGLIMIT        0x20
#define IOLIMIT         0x60
#define RD_OPERATION    1
#define WR_OPERATION    2
#define RDWR_OPERATION  3
#define NOADDRESS       0xffffffff

#endif

