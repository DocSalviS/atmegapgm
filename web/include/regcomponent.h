#ifndef __REG_COMPONENT_H__
#define __REG_COMPONENT_H__

#include "component.h"

class RegComponent : public Component {
    public:
        RegComponent();
        void draw() override;

        void set_pc(uint16_t pc);
        void set_sp(uint16_t sp);
        void set_sreg(uint8_t sreg);
        
        void set_sp_highlight(bool highlight);
        void set_sreg_highlight(uint8_t highlight);

    private:
        uint16_t pc;
        uint16_t sp;
        uint8_t sreg;

        bool sp_highlight;
        uint8_t sreg_highlight;
};

#endif
