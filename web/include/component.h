#ifndef __COMPONENT_H__
#define __COMPONENT_H__

#include <ncurses.h>
#include <string>

class Component {
    public:
        virtual ~Component();
        virtual void draw();
        virtual void refresh();
        void set_win(WINDOW* win, int x, int y, int width, int height);
        virtual void set_elements_win(WINDOW* win);
        void set_name(std::string name);
        void set_active(bool active);
    protected:
        WINDOW* win;
        std::string name;
        bool active = false;
};

#endif
