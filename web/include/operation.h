#ifndef __OPCODE_H__
#define __OPCODE_H__

#include <cstdint>
#define IO_REG_OFFSET 0x20

struct op_t {
    const char* mnemonic;

    uint16_t type;

    uint16_t mask1;
    uint16_t mask2;

    uint8_t cycles;
    uint8_t size; //words

    uint8_t addr_mode;
};

class Operation {

public:
    Operation(uint16_t opcode);

    uint8_t get_cycles();
    uint8_t get_size();
    uint16_t get_type();
    const char *get_mnemonic();
    std::string to_string(uint16_t* prog, uint16_t pc);

    void get_two_reg_addr_5(uint8_t *rr, uint8_t *rd);
    void get_two_reg_addr_4(uint8_t *rr, uint8_t *rd);
    void get_two_reg_addr_3(uint8_t *rr, uint8_t *rd);
    void get_reg_imm_addr(uint8_t *rd, uint8_t *imm);
    void get_reg_imm_word_addr(uint8_t *rd, uint8_t *imm);
    void get_reg_addr_5(uint8_t *rd);
    void get_reg_addr_4(uint8_t *rd);
    void get_abs_prog_addr(uint32_t *addr, const uint16_t next);
    void get_rel_prog_addr(int16_t *addr);
    void get_reg_abs_addr(uint8_t *rd, uint16_t *addr, const uint16_t next);
    void get_reg_bit_addr(uint8_t *rr, uint8_t *bit);
    void get_ior_bit_addr(uint8_t *ior, uint8_t *bit);
    void get_rel_prog_bit_addr(int8_t *addr, uint8_t *bit);
    void get_bit_addr(uint8_t *bit);
    void get_two_reg_pair_addr(uint8_t *rr, uint8_t *rd);
    void get_reg_ior_addr(uint8_t *rd, uint8_t *ior);
    void get_reg_inc_addr(uint8_t *rr, uint8_t *inc);
    void get_reg_offs_addr(uint8_t *rr, uint8_t *q);

    enum Code {
        ADD,
        ADC,
        ADIW,
        SUB,
        SUBI,
        SBC,
        SBCI,
        SBIW,
        AND,
        ANDI,
        OR,
        ORI,
        EOR,
        COM,
        NEG,
        INC,
        DEC,
        SER,
        MUL,
        MULS,
        MULSU,
        FMUL,
        FMULS,
        FMULSU,
        RJMP,
        IJMP,
        JMP,
        RCALL,
        ICALL,
        CALL,
        RET,
        RETI,
        CPSE,
        CP,
        CPC,
        CPI,
        SBRC,
        SBRS,
        SBIC,
        SBIS,
        BRBS,
        BRBC,
        SBI,
        CBI,
        LSR,
        ROR,
        ASR,
        SWAP,
        BSET,
        BCLR,
        BST,
        BLD,
        MOV,
        MOVW,
        LDI,
        ST_X,
        ST_Y,
        STD_Y,
        ST_Z,
        STD_Z,
        STS,
        LPM,
        LPM_R0,
        LD_X,
        LD_Y,
        LDD_Y,
        LD_Z,
        LDD_Z,
        LDS,
        SPM,
        IN,
        OUT,
        PUSH,
        POP,
        NOP,
        SLEEP,
        WDR,
        BREAK,
        NONE,
    };

    enum AddrMode {
        NONE_ADDR,
        REG_ADDR_5,
        REG_ADDR_4,
        REG_IMM_ADDR,
        REG_IMM_WORD_ADDR,
        TWO_REG_ADDR_5,
        TWO_REG_ADDR_4,
        TWO_REG_ADDR_3,
        ABS_PROG_ADDR,
        REL_PROG_ADDR,
        REG_ABS_ADDR,
        REG_BIT_ADDR,
        IOR_BIT_ADDR,
        REL_PROG_BIT_ADDR,
        BIT_ADDR,
        TWO_REG_PAIR_ADDR,
        REG_IOR_ADDR,
        REG_INC_ADDR,
        REG_OFFS_ADDR,
        
    };

private:
    const struct op_t* type;
    uint16_t opcode;

    static const op_t op_list[];
};

#endif
