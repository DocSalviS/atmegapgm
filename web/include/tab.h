#ifndef __TAB_H__
#define __TAB_H__

#include <ncurses.h>
#include <string>
#include "component.h"

class Tab {
    public:
        Tab();
        virtual void draw();
        virtual void handle_input(int ch);
        void set_win(WINDOW* win);
        void clear();
        void refresh();
        std::string get_name();
        void set_name(std::string name);
        
    protected:
        WINDOW *win;
        std::string name;

        virtual void update_window(WINDOW* win);
};

#endif
