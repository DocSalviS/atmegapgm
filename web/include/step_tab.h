#ifndef __STEP_TAB_H__
#define __STEP_TAB_H__

#include <ncurses.h>
#include <thread>
#include <mutex>
#include "tab.h"
#include "avr.h"
#include "datacomponent.h"
#include "commcomponent.h"
#include "pgmcomponent.h"
#include "regcomponent.h"
#include "basiciocomponent.h"
#include "rwoperations.h"

class StepTab : public Tab {
    public:
        StepTab(AVR* cpu);
        ~StepTab();
        void draw() override;
        void handle_input(int ch) override;
    private:

        void update();
        void update_window(WINDOW* win) override;

        void run();
        void continue_run();
        void stop_run();

        void set_active(bool active);

        bool running;
        std::thread* run_thread;
        std::mutex ncurses_mutex;

        AVR* cpu;
        DataComponent* data;
        PgmComponent* code;
        CommComponent* commands;
        BasicIOComponent* io;
        RegComponent* reg;
};

#endif
