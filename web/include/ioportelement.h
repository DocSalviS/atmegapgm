#ifndef __IOPORT_ELEMENT_H__
#define __IOPORT_ELEMENT_H__

#include "element.h"

#define PORTBITS        8
#define INTLINES        3

struct portdata {
    uint8_t base;
    char port;
    char *lines[INTLINES];
};

class IOPortElement : public Element
{
    public:
        IOPortElement(WINDOW* win, int x, int y, uint8_t* iomem, portdata data);
        ~IOPortElement() {};
        void draw();
        void set_elements_win(WINDOW* win);
        void update_io_state();
        void toggleInput(uint8_t bit);
    private:
        uint8_t* iomem;
        portdata data;
};


#endif

