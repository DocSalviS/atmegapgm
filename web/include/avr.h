#ifndef __AVR_H__
#define __AVR_H__

#include <cstdint>
#include <iostream>
#include <thread>
#include <map>
#include "operation.h"
#include "registers.h"
#include "rwoperations.h"


#define PROG_SIZE 0x4000
#define DATA_SIZE 0x900
#define EEPROM_SIZE 0x400

#define SP_ADDR     0x5D
#define X_ADDR      0x1a
#define Y_ADDR      0x1c
#define Z_ADDR      0x1e

class AVR {
public:
    AVR();

    enum Flag {
        CARRY = 0b00000001,
        ZERO = 0b00000010,
        NEGATIVE = 0b00000100,
        OVER = 0b00001000,
        SIGN = 0b00010000,
        HALF_CARRY = 0b00100000,
        TRANSFER = 0b01000000,
        INTERRUPT = 0b10000000
    };

    enum Interrupt {
        RESET           = 2 * RESET_vect,
        INT0            = 2 * INT0_vect,
        INT1            = 2 * INT1_vect,
        PCINT0          = 2 * PCINT0_vect,
        PCINT1          = 2 * PCINT1_vect,
        PCINT2          = 2 * PCINT2_vect,
        WDT             = 2 * WDT_vect,
        TIMER2_COMPA    = 2 * TIMER2_COMPA_vect,
        TIMER2_COMPB    = 2 * TIMER2_COMPB_vect,
        TIMER2_OVF      = 2 * TIMER2_OVF_vect,
        TIMER1_CAPT     = 2 * TIMER1_CAPT_vect,
        TIMER1_COMPA    = 2 * TIMER1_COMPA_vect,
        TIMER1_COMPB    = 2 * TIMER1_COMPB_vect,
        TIMER1_OVF      = 2 * TIMER1_OVF_vect,
        TIMER0_COMPA    = 2 * TIMER0_COMPA_vect,
        TIMER0_COMPB    = 2 * TIMER0_COMPB_vect,
        TIMER0_OVF      = 2 * TIMER0_OVF_vect,
        SPI_STC         = 2 * SPI_STC_vect,
        USART_RX        = 2 * USART_RX_vect,
        USART_UDRE      = 2 * USART_UDRE_vect,
        USART_TX        = 2 * USART_TX_vect,
        ADC             = 2 * ADC_vect,
        EE_READY        = 2 * EE_READY_vect,
        ANALOG_COMP     = 2 * ANALOG_COMP_vect,
        TWI             = 2 * TWI_vect,
        SPM_READY       = 2 * SPM_READY_vect
    };

    size_t load_program(const char *filename);
    Operation step();
    void step_n(int n);
    void exeop(Operation *op);

    void handle_interrupts();
    void interrupt(Interrupt vector);

    uint8_t get_data_word(uint16_t addr);
    void set_data_word(uint16_t addr, uint8_t data);

    uint16_t get_data_pair(uint16_t addr);
    void set_data_pair(uint16_t addr, uint16_t value);

    uint8_t get_flag(AVR::Flag flag);
    void set_flag(AVR::Flag flag, uint8_t value);

    uint16_t get_X();
    uint16_t get_Y();
    uint16_t get_Z();

    void set_X(uint16_t value);
    void set_Y(uint16_t value);
    void set_Z(uint16_t value);

    void dump();

    uint8_t* get_data_ptr();
    uint16_t* get_pgm_ptr();

    uint16_t get_pc();
    void set_pc(uint16_t pc);

    uint16_t get_sp();
    void set_sp(uint16_t sp);
    uint16_t watch_sp();

    uint8_t get_status();
    void set_status(uint8_t status);

    void reset();

    void clear_hl();

    size_t get_hl_mem_addr();   // rd or wr or dreg addr
    size_t get_hl_wr_addr();
    size_t get_hl_rd_addr();
    size_t get_dreg_addr();
    uint8_t get_dregop();
    uint8_t get_hl_status();
    bool get_hl_sp();

    uint8_t get_int_flag(Interrupt interrupt);
    void set_int_flag(Interrupt interrupt);

    uint8_t get_int_mask(Interrupt interrupt);
    void set_int_mask(Interrupt interrupt);

    void set_breakpoint(uint16_t bpc);
    void remove_breakpoint(uint16_t bpc);
    void toggle_breakpoint(uint16_t bpc);

    bool handle_breakpoint();

    std::map<uint16_t, uint16_t> *get_breakpoints();


private:
    int get_hex(char **s, int len);

    uint16_t program[PROG_SIZE];
    uint8_t data[DATA_SIZE];
    uint8_t eeprom[EEPROM_SIZE];

    uint16_t pc;
    // uint16_t sp;    // Mappato in memoria 0x5E0x5D

    uint8_t status;

    size_t hl_wr_addr;
    size_t hl_rd_addr;
    size_t hl_dreg_addr;
    uint8_t dregop;
    uint8_t hl_status;
    bool hl_sp;

    std::map<uint16_t, uint16_t> breakpoints;
};

#endif
