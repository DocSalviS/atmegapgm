/* Copyright (c) 2007 Atmel Corporation
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.

   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. 
*/

/* $Id: iom328p.h 2444 2014-08-11 22:10:47Z joerg_wunsch $ */

#ifndef __REGS_H__
#define __REGS_H__

/* Registers and associated bit numbers */

#ifndef IO_REG_OFFSET
#define IO_REG_OFFSET 0x20
#endif

#define _PINB (IO_REG_OFFSET + 0x03)
#define _PINB0 0
#define _PINB1 1
#define _PINB2 2
#define _PINB3 3
#define _PINB4 4
#define _PINB5 5
#define _PINB6 6
#define _PINB7 7

#define _DDRB (IO_REG_OFFSET + 0x04)
#define _DDB0 0
#define _DDB1 1
#define _DDB2 2
#define _DDB3 3
#define _DDB4 4
#define _DDB5 5
#define _DDB6 6
#define _DDB7 7

#define _PORTB (IO_REG_OFFSET + 0x05)
#define _PORTB0 0
#define _PORTB1 1
#define _PORTB2 2
#define _PORTB3 3
#define _PORTB4 4
#define _PORTB5 5
#define _PORTB6 6
#define _PORTB7 7

#define _PINC (IO_REG_OFFSET + 0x06)
#define _PINC0 0
#define _PINC1 1
#define _PINC2 2
#define _PINC3 3
#define _PINC4 4
#define _PINC5 5
#define _PINC6 6

#define _DDRC (IO_REG_OFFSET + 0x07)
#define _DDC0 0
#define _DDC1 1
#define _DDC2 2
#define _DDC3 3
#define _DDC4 4
#define _DDC5 5
#define _DDC6 6

#define _PORTC (IO_REG_OFFSET + 0x08)
#define _PORTC0 0
#define _PORTC1 1
#define _PORTC2 2
#define _PORTC3 3
#define _PORTC4 4
#define _PORTC5 5
#define _PORTC6 6

#define _PIND (IO_REG_OFFSET + 0x09)
#define _PIND0 0
#define _PIND1 1
#define _PIND2 2
#define _PIND3 3
#define _PIND4 4
#define _PIND5 5
#define _PIND6 6
#define _PIND7 7

#define _DDRD (IO_REG_OFFSET + 0x0A)
#define _DDD0 0
#define _DDD1 1
#define _DDD2 2
#define _DDD3 3
#define _DDD4 4
#define _DDD5 5
#define _DDD6 6
#define _DDD7 7

#define _PORTD (IO_REG_OFFSET + 0x0B)
#define _PORTD0 0
#define _PORTD1 1
#define _PORTD2 2
#define _PORTD3 3
#define _PORTD4 4
#define _PORTD5 5
#define _PORTD6 6
#define _PORTD7 7

#define _TIFR0 (IO_REG_OFFSET + 0x15)
#define _TIFR0_TOV0 0
#define _TIFR0_OCF0A 1
#define _TIFR0_OCF0B 2

#define _TIFR1 (IO_REG_OFFSET + 0x16)
#define _TIFR1_TOV1 0
#define _TIFR1_OCF1A 1
#define _TIFR1_OCF1B 2
#define _TIFR1_ICF1 5

#define _TIFR2 (IO_REG_OFFSET + 0x17)
#define _TIFR2_TOV2 0
#define _TIFR2_OCF2A 1
#define _TIFR2_OCF2B 2

#define _PCIFR (IO_REG_OFFSET + 0x1B)
#define _PCIF0 0
#define _PCIF1 1
#define _PCIF2 2

#define _EIFR (IO_REG_OFFSET + 0x1C)
#define _EIFR_INTF0 0
#define _EIFR_INTF1 1

#define _EIMSK (IO_REG_OFFSET + 0x1D)
#define _EIMSK_INT0 0
#define _EIMSK_INT1 1

#define _GPIOR0 (IO_REG_OFFSET + 0x1E)
#define _GPIOR00 0
#define _GPIOR01 1
#define _GPIOR02 2
#define _GPIOR03 3
#define _GPIOR04 4
#define _GPIOR05 5
#define _GPIOR06 6
#define _GPIOR07 7

#define _EECR (IO_REG_OFFSET + 0x1F)
#define _EECR_EERE 0
#define _EECR_EEPE 1
#define _EECR_EEMPE 2
#define _EECR_EERIE 3
#define _EECR_EEPM0 4
#define _EECR_EEPM1 5

#define _EEDR (IO_REG_OFFSET + 0x20)
#define _EEDR0 0
#define _EEDR1 1
#define _EEDR2 2
#define _EEDR3 3
#define _EEDR4 4
#define _EEDR5 5
#define _EEDR6 6
#define _EEDR7 7

#define _EEAR  (IO_REG_OFFSET + 0x21)

#define _EEARL (IO_REG_OFFSET + 0x21)
#define _EEARL_EEAR0 0
#define _EEARL_EEAR1 1
#define _EEARL_EEAR2 2
#define _EEARL_EEAR3 3
#define _EEARL_EEAR4 4
#define _EEARL_EEAR5 5
#define _EEARL_EEAR6 6
#define _EEARL_EEAR7 7

#define _EEARH (IO_REG_OFFSET + 0x22)
#define _EEARH_EEAR8 0
#define _EEARH_EEAR9 1

#define _GTCCR (IO_REG_OFFSET + 0x23)
#define _GTCCR_PSRSYNC 0
#define _GTCCR_PSRASY 1
#define _GTCCR_TSM 7

#define _TCCR0A (IO_REG_OFFSET + 0x24)
#define _TCCR0A_WGM00 0
#define _TCCR0A_WGM01 1
#define _TCCR0A_COM0B0 4
#define _TCCR0A_COM0B1 5
#define _TCCR0A_COM0A0 6
#define _TCCR0A_COM0A1 7

#define _TCCR0B (IO_REG_OFFSET + 0x25)
#define _TCCR0B_CS00 0
#define _TCCR0B_CS01 1
#define _TCCR0B_CS02 2
#define _TCCR0B_WGM02 3
#define _TCCR0B_FOC0B 6
#define _TCCR0B_FOC0A 7

#define _TCNT0 (IO_REG_OFFSET + 0x26)
#define _TCNT0_0 0
#define _TCNT0_1 1
#define _TCNT0_2 2
#define _TCNT0_3 3
#define _TCNT0_4 4
#define _TCNT0_5 5
#define _TCNT0_6 6
#define _TCNT0_7 7

#define _OCR0A (IO_REG_OFFSET + 0x27)
#define _OCR0A_0 0
#define _OCR0A_1 1
#define _OCR0A_2 2
#define _OCR0A_3 3
#define _OCR0A_4 4
#define _OCR0A_5 5
#define _OCR0A_6 6
#define _OCR0A_7 7

#define _OCR0B (IO_REG_OFFSET + 0x28)
#define _OCR0B_0 0
#define _OCR0B_1 1
#define _OCR0B_2 2
#define _OCR0B_3 3
#define _OCR0B_4 4
#define _OCR0B_5 5
#define _OCR0B_6 6
#define _OCR0B_7 7

#define _GPIOR1 (IO_REG_OFFSET + 0x2A)
#define _GPIOR10 0
#define _GPIOR11 1
#define _GPIOR12 2
#define _GPIOR13 3
#define _GPIOR14 4
#define _GPIOR15 5
#define _GPIOR16 6
#define _GPIOR17 7

#define _GPIOR2 (IO_REG_OFFSET + 0x2B)
#define _GPIOR20 0
#define _GPIOR21 1
#define _GPIOR22 2
#define _GPIOR23 3
#define _GPIOR24 4
#define _GPIOR25 5
#define _GPIOR26 6
#define _GPIOR27 7

#define _SPCR (IO_REG_OFFSET + 0x2C)
#define _SPCR_SPR0 0
#define _SPCR_SPR1 1
#define _SPCR_CPHA 2
#define _SPCR_CPOL 3
#define _SPCR_MSTR 4
#define _SPCR_DORD 5
#define _SPCR_SPE 6
#define _SPCR_SPIE 7

#define _SPSR (IO_REG_OFFSET + 0x2D)
#define _SPSR_SPI2X 0
#define _SPSR_WCOL 6
#define _SPSR_SPIF 7

#define _SPDR (IO_REG_OFFSET + 0x2E)
#define _SPDR0 0
#define _SPDR1 1
#define _SPDR2 2
#define _SPDR3 3
#define _SPDR4 4
#define _SPDR5 5
#define _SPDR6 6
#define _SPDR7 7

#define _ACSR (IO_REG_OFFSET + 0x30)
#define _ACSR_ACIS0 0
#define _ACSR_ACIS1 1
#define _ACSR_ACIC 2
#define _ACSR_ACIE 3
#define _ACSR_ACI 4
#define _ACSR_ACO 5
#define _ACSR_ACBG 6
#define _ACSR_ACD 7

#define _SMCR (IO_REG_OFFSET + 0x33)
#define _SMCR_SE 0
#define _SMCR_SM0 1
#define _SMCR_SM1 2
#define _SMCR_SM2 3

#define _MCUSR (IO_REG_OFFSET + 0x34)
#define _MCUSR_PORF 0
#define _MCUSR_EXTRF 1
#define _MCUSR_BORF 2
#define _MCUSR_WDRF 3

#define _MCUCR (IO_REG_OFFSET + 0x35)
#define _MCUCR_IVCE 0
#define _MCUCR_IVSEL 1
#define _MCUCR_PUD 4
#define _MCUCR_BODSE 5
#define _MCUCR_BODS 6

#define _SPMCSR (IO_REG_OFFSET + 0x37)
#define _SPMCSR_SPMEN 0
#define _SPMCSR_PGERS 1
#define _SPMCSR_PGWRT 2
#define _SPMCSR_BLBSET 3
#define _SPMCSR_RWWSRE 4
#define _SPMCSR_SIGRD 5
#define _SPMCSR_RWWSB 6
#define _SPMCSR_SPMIE 7

#define _WDTCSR (0x60)
#define _WDTCSR_WDP0 0
#define _WDTCSR_WDP1 1
#define _WDTCSR_WDP2 2
#define _WDTCSR_WDE 3
#define _WDTCSR_WDCE 4
#define _WDTCSR_WDP3 5
#define _WDTCSR_WDIE 6
#define _WDTCSR_WDIF 7

#define _CLKPR (0x61)
#define _CLKPS0 0
#define _CLKPS1 1
#define _CLKPS2 2
#define _CLKPS3 3
#define _CLKPCE 7

#define _PRR (0x64)
#define _PRADC 0
#define _PRUSART0 1
#define _PRSPI 2
#define _PRTIM1 3
#define _PRTIM0 5
#define _PRTIM2 6
#define _PRTWI 7

#define _OSCCAL (0x66)
#define _OSCCAL_CAL0 0
#define _OSCCAL_CAL1 1
#define _OSCCAL_CAL2 2
#define _OSCCAL_CAL3 3
#define _OSCCAL_CAL4 4
#define _OSCCAL_CAL5 5
#define _OSCCAL_CAL6 6
#define _OSCCAL_CAL7 7

#define _PCICR (0x68)
#define _PCICR_PCIE0 0
#define _PCICR_PCIE1 1
#define _PCICR_PCIE2 2

#define _EICRA (0x69)
#define _EICRA_ISC00 0
#define _EICRA_ISC01 1
#define _EICRA_ISC10 2
#define _EICRA_ISC11 3

#define _PCMSK0 (0x6B)
#define _PCMSK0_PCINT0 0
#define _PCMSK0_PCINT1 1
#define _PCMSK0_PCINT2 2
#define _PCMSK0_PCINT3 3
#define _PCMSK0_PCINT4 4
#define _PCMSK0_PCINT5 5
#define _PCMSK0_PCINT6 6
#define _PCMSK0_PCINT7 7

#define _PCMSK1 (0x6C)
#define _PCMSK1_PCINT8 0
#define _PCMSK1_PCINT9 1
#define _PCMSK1_PCINT10 2
#define _PCMSK1_PCINT11 3
#define _PCMSK1_PCINT12 4
#define _PCMSK1_PCINT13 5
#define _PCMSK1_PCINT14 6

#define _PCMSK2 (0x6D)
#define _PCMSK2_PCINT16 0
#define _PCMSK2_PCINT17 1
#define _PCMSK2_PCINT18 2
#define _PCMSK2_PCINT19 3
#define _PCMSK2_PCINT20 4
#define _PCMSK2_PCINT21 5
#define _PCMSK2_PCINT22 6
#define _PCMSK2_PCINT23 7

#define _TIMSK0 (0x6E)
#define _TIMSK0_TOIE0 0
#define _TIMSK0_OCIE0A 1
#define _TIMSK0_OCIE0B 2

#define _TIMSK1 (0x6F)
#define _TIMSK1_TOIE1 0
#define _TIMSK1_OCIE1A 1
#define _TIMSK1_OCIE1B 2
#define _TIMSK1_ICIE1 5

#define _TIMSK2 (0x70)
#define _TIMSK2_TOIE2 0
#define _TIMSK2_OCIE2A 1
#define _TIMSK2_OCIE2B 2

#define _ADCW    (0x78)

#define _ADCL (0x78)
#define _ADCL0 0
#define _ADCL1 1
#define _ADCL2 2
#define _ADCL3 3
#define _ADCL4 4
#define _ADCL5 5
#define _ADCL6 6
#define _ADCL7 7

#define _ADCH (0x79)
#define _ADCH0 0
#define _ADCH1 1
#define _ADCH2 2
#define _ADCH3 3
#define _ADCH4 4
#define _ADCH5 5
#define _ADCH6 6
#define _ADCH7 7

#define _ADCSRA (0x7A)
#define _ADCSRA_ADPS0 0
#define _ADCSRA_ADPS1 1
#define _ADCSRA_ADPS2 2
#define _ADCSRA_ADIE 3
#define _ADCSRA_ADIF 4
#define _ADCSRA_ADATE 5
#define _ADCSRA_ADSC 6
#define _ADCSRA_ADEN 7

#define _ADCSRB (0x7B)
#define _ADCSRB_ADTS0 0
#define _ADCSRB_ADTS1 1
#define _ADCSRB_ADTS2 2
#define _ADCSRB_ACME 6

#define _ADMUX (0x7C)
#define _ADMUX_MUX0 0
#define _ADMUX_MUX1 1
#define _ADMUX_MUX2 2
#define _ADMUX_MUX3 3
#define _ADMUX_ADLAR 5
#define _ADMUX_REFS0 6
#define _ADMUX_REFS1 7

#define _DIDR0 (0x7E)
#define _DIDR0_ADC0D 0
#define _DIDR0_ADC1D 1
#define _DIDR0_ADC2D 2
#define _DIDR0_ADC3D 3
#define _DIDR0_ADC4D 4
#define _DIDR0_ADC5D 5

#define _DIDR1 (0x7F)
#define _DIDR1_AIN0D 0
#define _DIDR1_AIN1D 1

#define _TCCR1A (0x80)
#define _TCCR1A_WGM10 0
#define _TCCR1A_WGM11 1
#define _TCCR1A_COM1B0 4
#define _TCCR1A_COM1B1 5
#define _TCCR1A_COM1A0 6
#define _TCCR1A_COM1A1 7

#define _TCCR1B (0x81)
#define _TCCR1B_CS10 0
#define _TCCR1B_CS11 1
#define _TCCR1B_CS12 2
#define _TCCR1B_WGM12 3
#define _TCCR1B_WGM13 4
#define _TCCR1B_ICES1 6
#define _TCCR1B_ICNC1 7

#define _TCCR1C (0x82)
#define _TCCR1C_FOC1B 6
#define _TCCR1C_FOC1A 7

#define _TCNT1 (0x84)

#define _TCNT1L (0x84)
#define _TCNT1L0 0
#define _TCNT1L1 1
#define _TCNT1L2 2
#define _TCNT1L3 3
#define _TCNT1L4 4
#define _TCNT1L5 5
#define _TCNT1L6 6
#define _TCNT1L7 7

#define _TCNT1H (0x85)
#define _TCNT1H0 0
#define _TCNT1H1 1
#define _TCNT1H2 2
#define _TCNT1H3 3
#define _TCNT1H4 4
#define _TCNT1H5 5
#define _TCNT1H6 6
#define _TCNT1H7 7

#define _ICR1 (0x86)

#define _ICR1L (0x86)
#define _ICR1L0 0
#define _ICR1L1 1
#define _ICR1L2 2
#define _ICR1L3 3
#define _ICR1L4 4
#define _ICR1L5 5
#define _ICR1L6 6
#define _ICR1L7 7

#define _ICR1H (0x87)
#define _ICR1H0 0
#define _ICR1H1 1
#define _ICR1H2 2
#define _ICR1H3 3
#define _ICR1H4 4
#define _ICR1H5 5
#define _ICR1H6 6
#define _ICR1H7 7

#define _OCR1A (0x88)

#define _OCR1AL (0x88)
#define _OCR1AL0 0
#define _OCR1AL1 1
#define _OCR1AL2 2
#define _OCR1AL3 3
#define _OCR1AL4 4
#define _OCR1AL5 5
#define _OCR1AL6 6
#define _OCR1AL7 7

#define _OCR1AH (0x89)
#define _OCR1AH0 0
#define _OCR1AH1 1
#define _OCR1AH2 2
#define _OCR1AH3 3
#define _OCR1AH4 4
#define _OCR1AH5 5
#define _OCR1AH6 6
#define _OCR1AH7 7

#define _OCR1B (0x8A)

#define _OCR1BL (0x8A)
#define _OCR1BL0 0
#define _OCR1BL1 1
#define _OCR1BL2 2
#define _OCR1BL3 3
#define _OCR1BL4 4
#define _OCR1BL5 5
#define _OCR1BL6 6
#define _OCR1BL7 7

#define _OCR1BH (0x8B)
#define _OCR1BH0 0
#define _OCR1BH1 1
#define _OCR1BH2 2
#define _OCR1BH3 3
#define _OCR1BH4 4
#define _OCR1BH5 5
#define _OCR1BH6 6
#define _OCR1BH7 7

#define _TCCR2A (0xB0)
#define _TCCR2A_WGM20 0
#define _TCCR2A_WGM21 1
#define _TCCR2A_COM2B0 4
#define _TCCR2A_COM2B1 5
#define _TCCR2A_COM2A0 6
#define _TCCR2A_COM2A1 7

#define _TCCR2B (0xB1)
#define _TCCR2B_CS20 0
#define _TCCR2B_CS21 1
#define _TCCR2B_CS22 2
#define _TCCR2B_WGM22 3
#define _TCCR2B_FOC2B 6
#define _TCCR2B_FOC2A 7

#define _TCNT2 (0xB2)
#define _TCNT2_0 0
#define _TCNT2_1 1
#define _TCNT2_2 2
#define _TCNT2_3 3
#define _TCNT2_4 4
#define _TCNT2_5 5
#define _TCNT2_6 6
#define _TCNT2_7 7

#define _OCR2A (0xB3)
#define _OCR2_0 0
#define _OCR2_1 1
#define _OCR2_2 2
#define _OCR2_3 3
#define _OCR2_4 4
#define _OCR2_5 5
#define _OCR2_6 6
#define _OCR2_7 7

#define _OCR2B (0xB4)
#define _OCR2_0 0
#define _OCR2_1 1
#define _OCR2_2 2
#define _OCR2_3 3
#define _OCR2_4 4
#define _OCR2_5 5
#define _OCR2_6 6
#define _OCR2_7 7

#define _ASSR (0xB6)
#define _ASSR_TCR2BUB 0
#define _ASSR_TCR2AUB 1
#define _ASSR_OCR2BUB 2
#define _ASSR_OCR2AUB 3
#define _ASSR_TCN2UB 4
#define _ASSR_AS2 5
#define _ASSR_EXCLK 6

#define _TWBR (0xB8)
#define _TWBR0 0
#define _TWBR1 1
#define _TWBR2 2
#define _TWBR3 3
#define _TWBR4 4
#define _TWBR5 5
#define _TWBR6 6
#define _TWBR7 7

#define _TWSR (0xB9)
#define _TWSR_TWPS0 0
#define _TWSR_TWPS1 1
#define _TWSR_TWS3 3
#define _TWSR_TWS4 4
#define _TWSR_TWS5 5
#define _TWSR_TWS6 6
#define _TWSR_TWS7 7

#define _TWAR (0xBA)
#define _TWAR_TWGCE 0
#define _TWAR_TWA0 1
#define _TWAR_TWA1 2
#define _TWAR_TWA2 3
#define _TWAR_TWA3 4
#define _TWAR_TWA4 5
#define _TWAR_TWA5 6
#define _TWAR_TWA6 7

#define _TWDR (0xBB)
#define _TWD0 0
#define _TWD1 1
#define _TWD2 2
#define _TWD3 3
#define _TWD4 4
#define _TWD5 5
#define _TWD6 6
#define _TWD7 7

#define _TWCR (0xBC)
#define _TWCR_TWIE 0
#define _TWCR_TWEN 2
#define _TWCR_TWWC 3
#define _TWCR_TWSTO 4
#define _TWCR_TWSTA 5
#define _TWCR_TWEA 6
#define _TWCR_TWINT 7

#define _TWAMR (0xBD)
#define _TWAM0 0
#define _TWAM1 1
#define _TWAM2 2
#define _TWAM3 3
#define _TWAM4 4
#define _TWAM5 5
#define _TWAM6 6

#define _UCSR0A (0xC0)
#define _UCSR0A_MPCM0 0
#define _UCSR0A_U2X0 1
#define _UCSR0A_UPE0 2
#define _UCSR0A_DOR0 3
#define _UCSR0A_FE0 4
#define _UCSR0A_UDRE0 5
#define _UCSR0A_TXC0 6
#define _UCSR0A_RXC0 7

#define _UCSR0B (0xC1)
#define _UCSR0B_TXB80 0
#define _UCSR0B_RXB80 1
#define _UCSR0B_UCSZ02 2
#define _UCSR0B_TXEN0 3
#define _UCSR0B_RXEN0 4
#define _UCSR0B_UDRIE0 5
#define _UCSR0B_TXCIE0 6
#define _UCSR0B_RXCIE0 7

#define _UCSR0C (0xC2)
#define _UCSR0C_UCPOL0 0
#define _UCSR0C_UCSZ00 1
#define _UCSR0C_UCPHA0 1
#define _UCSR0C_UCSZ01 2
#define _UCSR0C_UDORD0 2
#define _UCSR0C_USBS0 3
#define _UCSR0C_UPM00 4
#define _UCSR0C_UPM01 5
#define _UCSR0C_UMSEL00 6
#define _UCSR0C_UMSEL01 7

#define _UBRR0 (0xC4)

#define _UBRR0L (0xC4)
#define _UBRR0L_UBRR0_0 0
#define _UBRR0L_UBRR0_1 1
#define _UBRR0L_UBRR0_2 2
#define _UBRR0L_UBRR0_3 3
#define _UBRR0L_UBRR0_4 4
#define _UBRR0L_UBRR0_5 5
#define _UBRR0L_UBRR0_6 6
#define _UBRR0L_UBRR0_7 7

#define _UBRR0H (0xC5)
#define _UBRR0H_UBRR0_8 0
#define _UBRR0H_UBRR0_9 1
#define _UBRR0H_UBRR0_10 2
#define _UBRR0H_UBRR0_11 3

#define _UDR0 (0xC6)
#define _UDR0_0 0
#define _UDR0_1 1
#define _UDR0_2 2
#define _UDR0_3 3
#define _UDR0_4 4
#define _UDR0_5 5
#define _UDR0_6 6
#define _UDR0_7 7

/* Interrupt Vectors */
/* Interrupt Vector 0 is the reset vector. */
#define RESET_vect          0   /* External Pin, Power-on Reset, Brown-out Reset and Watchdog System Reset */
#define INT0_vect           1   /* External Interrupt Request 0 */
#define INT1_vect           2   /* External Interrupt Request 1 */
#define PCINT0_vect         3   /* Pin Change Interrupt Request 0 */
#define PCINT1_vect         4   /* Pin Change Interrupt Request 0 */
#define PCINT2_vect         5   /* Pin Change Interrupt Request 1 */
#define WDT_vect            6   /* Watchdog Time-out Interrupt */
#define TIMER2_COMPA_vect   7   /* Timer/Counter2 Compare Match A */
#define TIMER2_COMPB_vect   8   /* Timer/Counter2 Compare Match A */
#define TIMER2_OVF_vect     9   /* Timer/Counter2 Overflow */
#define TIMER1_CAPT_vect    10  /* Timer/Counter1 Capture Event */
#define TIMER1_COMPA_vect   11  /* Timer/Counter1 Compare Match A */
#define TIMER1_COMPB_vect   12  /* Timer/Counter1 Compare Match B */ 
#define TIMER1_OVF_vect     13  /* Timer/Counter1 Overflow */
#define TIMER0_COMPA_vect   14  /* TimerCounter0 Compare Match A */
#define TIMER0_COMPB_vect   15  /* TimerCounter0 Compare Match B */
#define TIMER0_OVF_vect     16  /* Timer/Couner0 Overflow */
#define SPI_STC_vect        17  /* SPI Serial Transfer Complete */
#define USART_RX_vect       18  /* USART Rx Complete */
#define USART_UDRE_vect     19  /* USART, Data Register Empty */
#define USART_TX_vect       20  /* USART Tx Complete */
#define ADC_vect            21  /* ADC Conversion Complete */
#define EE_READY_vect       22  /* EEPROM Ready */
#define ANALOG_COMP_vect    23  /* Analog Comparator */
#define TWI_vect            24  /* Two-wire Serial Interface */
#define SPM_READY_vect      25  /* Store Program Memory Read */

//  /* Fuses */
//  #define FUSE_MEMORY_SIZE 3
//  
//  /* Low Fuse Byte */
//  #define FUSE_CKSEL0 (unsigned char)~_BV(0)  /* Select Clock Source */
//  #define FUSE_CKSEL1 (unsigned char)~_BV(1)  /* Select Clock Source */
//  #define FUSE_CKSEL2 (unsigned char)~_BV(2)  /* Select Clock Source */
//  #define FUSE_CKSEL3 (unsigned char)~_BV(3)  /* Select Clock Source */
//  #define FUSE_SUT0   (unsigned char)~_BV(4)  /* Select start-up time */
//  #define FUSE_SUT1   (unsigned char)~_BV(5)  /* Select start-up time */
//  #define FUSE_CKOUT  (unsigned char)~_BV(6)  /* Clock output */
//  #define FUSE_CKDIV8 (unsigned char)~_BV(7) /* Divide clock by 8 */
//  #define LFUSE_DEFAULT (FUSE_CKSEL0 & FUSE_CKSEL2 & FUSE_CKSEL3 & FUSE_SUT0 & FUSE_CKDIV8)
//  
//  /* High Fuse Byte */
//  #define FUSE_BOOTRST (unsigned char)~_BV(0)
//  #define FUSE_BOOTSZ0 (unsigned char)~_BV(1)
//  #define FUSE_BOOTSZ1 (unsigned char)~_BV(2)
//  #define FUSE_EESAVE    (unsigned char)~_BV(3)  /* EEPROM memory is preserved through chip erase */
//  #define FUSE_WDTON     (unsigned char)~_BV(4)  /* Watchdog Timer Always On */
//  #define FUSE_SPIEN     (unsigned char)~_BV(5)  /* Enable Serial programming and Data Downloading */
//  #define FUSE_DWEN      (unsigned char)~_BV(6)  /* debugWIRE Enable */
//  #define FUSE_RSTDISBL  (unsigned char)~_BV(7)  /* External reset disable */
//  #define HFUSE_DEFAULT (FUSE_BOOTSZ0 & FUSE_BOOTSZ1 & FUSE_SPIEN)
//  
//  /* Extended Fuse Byte */
//  #define FUSE_BODLEVEL0 (unsigned char)~_BV(0)  /* Brown-out Detector trigger level */
//  #define FUSE_BODLEVEL1 (unsigned char)~_BV(1)  /* Brown-out Detector trigger level */
//  #define FUSE_BODLEVEL2 (unsigned char)~_BV(2)  /* Brown-out Detector trigger level */
//  #define EFUSE_DEFAULT  (0xFF)
//  
//  
//  
//  /* Lock Bits */
//  #define __LOCK_BITS_EXIST
//  #define __BOOT_LOCK_BITS_0_EXIST
//  #define __BOOT_LOCK_BITS_1_EXIST 
//  
//  
//  /* Signature */
//  #define SIGNATURE_0 0x1E
//  #define SIGNATURE_1 0x95
//  #if defined(__AVR_ATmega328__)
//  #  define SIGNATURE_2 0x14
//  #else /* ATmega328P */
//  #  define SIGNATURE_2 0x0F
//  #endif
//  
//  #define SLEEP_MODE_IDLE (0x00<<1)
//  #define SLEEP_MODE_ADC (0x01<<1)
//  #define SLEEP_MODE_PWR_DOWN (0x02<<1)
//  #define SLEEP_MODE_PWR_SAVE (0x03<<1)
//  #define SLEEP_MODE_STANDBY (0x06<<1)
//  #define SLEEP_MODE_EXT_STANDBY (0x07<<1)
//  
#endif  /* __REGS_H__ */
