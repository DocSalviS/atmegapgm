#include "basiciocomponent.h"

/* IO Ports 8 bits each
 *
 * Arduino ports      PORT    DDR     BIT
 * Porta C -> A0-A5   0x08    0x07    0x06
 * Porta B -> D8-D13  0x05    0x04    0x03
 * Porta D -> D0-D7   0x0B    0x0A    0x09
 *
 */


BasicIOComponent::BasicIOComponent(uint8_t* iomem) : iomem(iomem) {
    portdata data[] = {
        {0x08, 'C',{ (char*)"        ",(char*)"--AAAAAA",(char*)"--543210" }},
        {0x05, 'B',{ (char*)"--DDDDDD",(char*)"--111100",(char*)"--321098" }},
        {0x0B, 'D',{ (char*)"DDDDDDDD",(char*)"00000000",(char*)"76543210" }}
    };
    for(int i = 0; i < NPORTS; i++) {
        ports[i] = new IOPortElement(win, i*10+1, 1, iomem, data[i]);
    }
}

BasicIOComponent::~BasicIOComponent() {
    for(int i = 0; i < NPORTS; i++) {
        delete ports[i];
    }
}

void BasicIOComponent::draw() {
    if(active) {
        wattron(this->win, COLOR_PAIR(1));
    }

    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);

    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);
    mvwprintw(this->win, 0, 1, "[I/O]");

    wattroff(this->win, COLOR_PAIR(1));

    for(int i = 0; i < NPORTS; i++) {
        ports[i]->draw();
    }
}

void BasicIOComponent::set_elements_win(WINDOW* win) {
    for(int i = 0; i < NPORTS; i++) {
        ports[i]->set_win(win);
    }
}

void BasicIOComponent::toggleInput(uint8_t button) {
    ports[0]->toggleInput(button);
}

void BasicIOComponent::update_io_state() {
    for(int i=0; i<NPORTS; i++) {
        ports[i]->update_io_state();
    }
}
