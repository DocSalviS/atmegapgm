#include "regcomponent.h"
#include <cctype>

RegComponent::RegComponent() {
    this->pc = 0;
    this->sp = 0;
    this->sreg = 0;
}

void RegComponent::draw() {
    if(this->active)
        wattron(this->win, COLOR_PAIR(1));

    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);

    mvwprintw(this->win, 0, 1, "[Registers]");

    wattroff(this->win, COLOR_PAIR(1));

    mvwprintw(this->win, 1, 1, "PC: 0x%04x", this->pc);

    mvwprintw(this->win, 1, 13, "SP:");

    if (this->sp_highlight) {
        wattron(this->win, A_REVERSE);
    }

    mvwprintw(this->win, 1, 17, "0x%04x", this->sp);

    if (this->sp_highlight) {
        wattroff(this->win, A_REVERSE);
    }

    mvwprintw(this->win, 1, 25, "SR:");

    const char *sreg_names[][8] = {
        {"c", "z", "n", "v", "s", "h", "t", "i"},
        {"C", "Z", "N", "V", "S", "H", "T", "I"}
    };
    for (int i = 0; i < 8; i++) {
        if (this->sreg_highlight & (1 << i)) {
            wattron(this->win, A_REVERSE);
        }

        mvwprintw(this->win, 1, 29 + i, "%s", sreg_names[this->sreg & (1 << i) ? 1 : 0][i]);

        if (this->sreg_highlight & (1 << i)) {
            wattroff(this->win, A_REVERSE);
        }
    }

}

void RegComponent::set_pc(uint16_t pc) {
    this->pc = pc;
}

void RegComponent::set_sp(uint16_t sp) {
    this->sp = sp;
}

void RegComponent::set_sreg(uint8_t sreg) {
    this->sreg = sreg;
}

void RegComponent::set_sp_highlight(bool highlight) {
    this->sp_highlight = highlight;
}

void RegComponent::set_sreg_highlight(uint8_t highlight) {
    this->sreg_highlight = highlight;
}
