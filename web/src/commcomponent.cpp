#include "commcomponent.h"

CommComponent::CommComponent() {
    this->commands = std::vector<std::pair<char, std::string>>();
}

void CommComponent::draw() {
    if (this->active)
        wattron(this->win, COLOR_PAIR(1));

    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);
    mvwprintw(this->win, 0, 1, "[Commands]");

    wattroff(this->win, COLOR_PAIR(1));

    int cursor = 1;
    for (size_t i = 0; i < this->commands.size(); i++) {
        mvwprintw(this->win, 1, cursor, "[%c: %s]", 
                this->commands[i].first, 
                this->commands[i].second.c_str()
        );
        cursor += this->commands[i].second.length() + 6;
    }
}

void CommComponent::add_command(char ch, std::string name) {
    this->commands.push_back(std::make_pair(ch, name));
}
