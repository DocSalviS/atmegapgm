#include <iostream>
#include <ncurses.h>
#include "operation.h"
#include "avr.h"
#include "slogpp.h"
#include "terminal.h"
#include "step_tab.h"
#include "tab.h"

int main(const int argc, const char* argv[]) {
/*
    SLog::set_log_level(0
        | SLog::Level::DEBUG
        | SLog::Level::INFO
        | SLog::Level::WARN
        | SLog::Level::ERROR
    );
    SLog::info("Starting AVR emulator");

    AVR avr;
    avr.load_program(argc == 2 ? argv[1] : "ex.bin");
    for(int i = 0; i < 50; i++) {
        avr.step();
    }
    putc('\n', stdout);
*/

    
    Terminal term;

    AVR avr;
    avr.load_program(argc == 2 ? argv[1] : "ex.bin");
    
    StepTab step_tab(&avr);
    term.add_tab(&step_tab);

    term.run();

    return 0;
}
