#include "datacomponent.h"
#include "term_colors.h"

DataComponent::DataComponent(uint8_t* data, size_t size) {
    this->data = data;
    this->size = size;
    this->mem_wr = 0;
    this->mem_rd = 0;
    this->selected = 0;
}


int DataComponent::get_num_bytes(int width) {
    int widths[] = {  32, 16, 8, 4 };// Only allowed widths = 4 / 8 / 16 / 32
    int num_bytes = (width-9)/4;
    for (unsigned int i = 0; i < sizeof(widths)/sizeof(int); i++) {
        if (num_bytes >= widths[i]) {
            return widths[i];
        }
    }
    return num_bytes;
}

void DataComponent::draw() {
    int width, height;
    getmaxyx(this->win, height, width);

    wattron(this->win, COLOR_PAIR((this->active)? DISABLED_PAIR : 0));

    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);

    mvwprintw(this->win, 0, 1, "[Data]");

    wattroff(this->win, COLOR_PAIR((this->active) ? DISABLED_PAIR : 0));

    height -= 2;

    int num_bytes = get_num_bytes(width);

    size_t cursor = this->selected;

    mvwprintw(this->win, 1, 1, "%d", this->dreg_op);
    for(int i = 0; i < num_bytes; i++) {
        mvwprintw(this->win, 1, 4*i+8, "x%02x", i);
    }
    for (int i = 1; i < height; i++) {
        wattron(this->win, COLOR_PAIR((cursor < REGLIMIT) ? REGISTERS_PAIR : ((cursor < IOLIMIT) ? IO_PAIR : 0)));
        mvwprintw(this->win, i+1, 1, "0x%04lx ", cursor);
        for (int j = 0; j < num_bytes && cursor < this->size; j++, cursor++) {
            if ((cursor == this->mem_wr || cursor == this->mem_rd || cursor == this->dreg || cursor == this->dreg+1)) {
                wattroff(this->win, COLOR_PAIR((cursor < REGLIMIT) ? REGISTERS_PAIR : ((cursor < IOLIMIT) ? IO_PAIR : DEFAULT_PAIR)));

                if (cursor == this->mem_wr) {
                     wattron(this->win, A_REVERSE);
                } else if (cursor == this->mem_rd) {
                    wattron(this->win, COLOR_PAIR(RD_PAIR));
                } else {
                    switch (this->dreg_op) {
                        case RD_OPERATION:
                            wattron(this->win, COLOR_PAIR(RD_PAIR));
                            break;
                        case WR_OPERATION:
                            wattron(this->win, A_REVERSE);
                            break;
                        case RDWR_OPERATION:
                            wattron(this->win, COLOR_PAIR(RDWR_PAIR));
                            break;
                    }
                }
            }

            wprintw(this->win, " %02x ", this->data[cursor]);

            // wattron(this->win, COLOR_PAIR((cursor < REGLIMIT) ? REGISTERS_PAIR : ((cursor < IOLIMIT) ? IO_PAIR : DEFAULT_PAIR)));
            if ((cursor == this->mem_wr || cursor == this->mem_rd || cursor == this->dreg || cursor == this->dreg+1)) {
                if (cursor == this->mem_wr) {
                     wattroff(this->win, A_REVERSE);
                } else if (cursor == this->mem_rd) {
                    wattroff(this->win, COLOR_PAIR(RD_PAIR));
                } else {
                    switch (this->dreg_op) {
                        case RD_OPERATION:
                            wattroff(this->win, COLOR_PAIR(RD_PAIR));
                            break;
                        case WR_OPERATION:
                            wattroff(this->win, A_REVERSE);
                            break;
                        case RDWR_OPERATION:
                            wattroff(this->win, COLOR_PAIR(RDWR_PAIR));
                            break;
                    }
                }

                wattron(this->win, COLOR_PAIR((cursor < REGLIMIT) ? REGISTERS_PAIR : ((cursor < IOLIMIT) ? IO_PAIR : 0)));
            }
        }
        wattroff(this->win, COLOR_PAIR((cursor < REGLIMIT) ? REGISTERS_PAIR : ((cursor < IOLIMIT) ? IO_PAIR : DEFAULT_PAIR)));
    }
}

void DataComponent::select(size_t index) {
    int width, height;
    getmaxyx(this->win, height, width);

    int num_bytes = get_num_bytes(width);
    height -= 3;
    index /= num_bytes;
    index *= num_bytes; // maintain alignment to num_bytes

    if ((int)index < num_bytes*height) {    // If it remains in the first page
        this->selected = 0;
    } else if (index > this->size - num_bytes*height) {  // If it is in the last page
        this->selected = this->size - num_bytes*height;
    } else if (this->selected < index) { // If it's before te firs line
        this->selected = index;
    } else if (index >= this->selected + num_bytes*height) {    // It's after the last line
        this->selected = index - num_bytes*(height - 1);
    }
}

void DataComponent::set_wr_address(size_t index) {
    this->mem_wr = index;
}

void DataComponent::set_rd_address(size_t index) {
    this->mem_rd = index;
}

void DataComponent::set_dreg_address(size_t index, uint8_t operation) {
    this->dreg = index;
    this->dreg_op = operation;
}
