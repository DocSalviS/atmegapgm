#include "step_tab.h"
#include <bit>

StepTab::StepTab(AVR* cpu) {
    this->cpu = cpu;
    this->name = "Step Tab";
    this->data = new DataComponent(cpu->get_data_ptr(), DATA_SIZE);
    this->code = new PgmComponent(cpu->get_pgm_ptr(), PROG_SIZE, cpu->get_breakpoints());
    this->io = new BasicIOComponent(cpu->get_data_ptr()+0x20);

    this->reg = new RegComponent();

    this->commands = new CommComponent();
    this->commands->add_command('s', "Step");
    this->commands->add_command('r', "Restart");
    this->commands->add_command('q', "Quit");
    this->commands->add_command('t', "Toggle RUN");
    this->commands->add_command('f', "Refresh");
    this->commands->add_command('b', "Toggle BP");

    this->running = false;
    this->run_thread = nullptr;
}

StepTab::~StepTab() {
    delete this->data;
    delete this->code;
    delete this->commands;
    delete this->io;
    delete this->reg;

}

void StepTab::draw() {

    ncurses_mutex.lock();

    this->data->draw();
    this->data->refresh();

    this->code->draw();
    this->code->refresh();

    this->commands->draw();
    this->commands->refresh();

    this->io->draw();
    this->io->refresh();

    this->reg->draw();
    this->reg->refresh();

    ncurses_mutex.unlock();

}

void StepTab::handle_input(int ch) {
    switch(ch) {
        case 's':
            stop_run();
            cpu->step();
            this->code->hide_user_cursor();
            this->update();
            break;
        case 'r':
            this->cpu->reset();
            this->code->hide_user_cursor();
            this->update();
            break;
        case 't':
            if(running)
                stop_run();
            else
                continue_run();

            this->code->hide_user_cursor();
            this->update();
            break;
        case 'f':
            this->code->hide_user_cursor();
            this->update();
            break;
        case KEY_UP:
            this->code->dec_user_cursor();
            this->update();
        break;
        case KEY_DOWN:
            this->code->inc_user_cursor();
            this->update();
        break;
        case 'b':
            if(this->code->get_show_user_cursor()) {
                this->cpu->toggle_breakpoint(this->code->get_user_cursor());
                this->update();
            }
            break;
        case 0x019A:
            update_window(this->win);
            break;
        case '1':
        case '2':
        case '3':
            this->io->toggleInput(ch-'0');
            break;
        case 'q':
            stop_run();
            this->running = false;
            break;
        default:
            break;
    }

    this->draw();
}

void StepTab::update() {
    this->code->set_cursor(this->cpu->get_pc());

    this->reg->set_pc(this->cpu->get_pc());
    this->reg->set_sp(this->cpu->watch_sp());   // Reads SP value, but doesn'mark a read operation
    this->reg->set_sreg(this->cpu->get_status());
    this->reg->set_sp_highlight(this->cpu->get_hl_sp());
    this->reg->set_sreg_highlight(this->cpu->get_hl_status());

    this->data->select(this->cpu->get_hl_mem_addr());
    this->data->set_wr_address(this->cpu->get_hl_wr_addr());
    this->data->set_rd_address(this->cpu->get_hl_rd_addr());
    this->data->set_dreg_address(this->cpu->get_dreg_addr(), this->cpu->get_dregop());

    this->io->update_io_state();
}

void StepTab::update_window(WINDOW* win) {

    unsigned int width, height;
    getmaxyx(win, height, width);

    int io_h = 7;
    int regs_h = 3;
    int comm_h = 3;
    int mem_w = std::bit_floor(((width * 2/3)-9)/4)*4 + 9;
    int prog_w = width - mem_w;
    int prog_h = height - comm_h - io_h;
    int mem_h = height - comm_h - regs_h;

    this->code->set_win(win, 0, 0, prog_w, prog_h);
    this->data->set_win(win, prog_w, 0, mem_w, mem_h);

    this->io->set_win(win, 0, prog_h, prog_w, io_h);
    this->reg->set_win(win, prog_w, mem_h, mem_w, regs_h);

    this->commands->set_win(win, 0, mem_h+regs_h, width, comm_h);

    wclear(win);

    this->update();
}

void StepTab::run() {
    while(this->running) {
        uint16_t pc = this->cpu->get_pc();
        Operation op = this->cpu->step();
        if(op.get_type() == Operation::BREAK ||
            pc == this->cpu->get_pc()) {
            this->running = false;
        }

        if(this->cpu->get_hl_wr_addr() < NOADDRESS &&
                this->cpu->get_hl_wr_addr() >= 0x20 &&
                this->cpu->get_hl_wr_addr() <= 0x5f) {

            this->ncurses_mutex.lock();
            this->io->update_io_state();
            this->io->draw();
            this->io->refresh();
            this->ncurses_mutex.unlock();
        }

    }

    this->update();
    set_active(false);
}

void StepTab::continue_run() {
    if(!running) {

        running = true;
        set_active(true);

        if(this->run_thread!=nullptr) {
            this->run_thread->join();
            delete this->run_thread;
        }
        this->run_thread = new std::thread(&StepTab::run, this);
    }
}

void StepTab::stop_run() {
    running = false;

    if(!this->run_thread) return;
    this->run_thread->join();

    delete this->run_thread;
    run_thread = nullptr;

    this->update();
    set_active(false);

}

void StepTab::set_active(bool active) {

    if(active) {
        this->code->set_name("Code (RUNNING)");
    } else {
        this->code->set_name("Code");
    }

    this->code->set_active(active);
    this->data->set_active(active);
    this->commands->set_active(active);
    this->io->set_active(active);
    this->reg->set_active(active);

    this->draw();
}
