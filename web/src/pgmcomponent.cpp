#include <string>
#include <map>
#include "pgmcomponent.h"
#include "operation.h"

#define CURSOR_PADDING 4

PgmComponent::PgmComponent(uint16_t* data, size_t size, std::map<uint16_t, uint16_t> *breakpoints) {
    this->data = data;
    this->size = size;
    this->breakpoints = breakpoints;
    this->cursor = 0;
    this->user_cursor = 0;
    this->start_addr = 0;
    this->next_addr = 1;
    this->show_user_cursor = false;
    this->name = "Code";
}

void PgmComponent::draw() {

    wclear(this->win);

    int width, height;
    getmaxyx(this->win, height, width);

    if(active) {
        wattron(this->win, COLOR_PAIR(1));
    }

    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);

    mvwprintw(this->win, 0, 1, ("["+this->name+"]").c_str());

    wattroff(this->win, COLOR_PAIR(1));

    size_t cursor = this->start_addr;
    if (this->show_user_cursor) {
        if(this->user_cursor < cursor || this->user_cursor > this->next_addr) {
            cursor = this->user_cursor;
        } else if(this->user_cursor == this->next_addr) {
            cursor += Operation(this->data[cursor]).get_size();
        }
    } else {
        if (this->cursor < cursor || this->cursor > this->next_addr) {
            cursor = this->cursor;
        } else if (this->cursor == this->next_addr) {
            cursor += Operation(this->data[cursor]).get_size();
        }
    }

    this->start_addr = cursor;

    for (int i = 1; i < height-1 && cursor<this->size; i++) {
        Operation op = Operation(this->data[cursor]);


        if(breakpoints->contains(cursor-1) && Operation(breakpoints->at(cursor-1)).get_size() == 2){
            i--;
        } else {
            if (cursor == this->cursor) {
                wattron(this->win, A_REVERSE);
            }

            // SALVI 30/3/2024
            // Aggiunta la colonna con l'indirizzo
            mvwprintw(this->win, i, 1,  "%04x - ", (unsigned int) cursor);

            for(int j=0; j<op.get_size(); j++) {
                mvwprintw(this->win, i, j*5+8,
                        "%04x", this->data[cursor+j]
                );
            }
            mvwprintw(this->win, i, width/2,
                "%s", op.to_string(this->data, cursor).c_str()

            );

            if(cursor == this->user_cursor && this->show_user_cursor){
                wattroff(this->win, A_REVERSE);
                mvwprintw(this->win, i, width/2-4, "->");
            }
            if(breakpoints->contains(cursor)){
                wattroff(this->win, A_REVERSE);
                mvwprintw(this->win, i, width/2-2, "B");
            }


            wattroff(this->win, A_REVERSE);
        }


        cursor+=op.get_size();
    }
    this->next_addr = cursor;
}

void PgmComponent::set_cursor(size_t cursor) {
    this->cursor = cursor;
}

void PgmComponent::inc_user_cursor(){
    reset_user_cursor();
    show_user_cursor = true;
    if(breakpoints->contains(this->user_cursor)){
        Operation op = Operation(breakpoints->at(this->user_cursor));
        this->user_cursor += op.get_size();
    } else {
        Operation op = Operation(data[this->user_cursor]);
        this->user_cursor += op.get_size();
    }
    return;
}

void PgmComponent::dec_user_cursor(){
    reset_user_cursor();
    show_user_cursor = true;
    Operation op = Operation(0);
    if(user_cursor >= 1){
        if(user_cursor >= 2){
            op = Operation(data[user_cursor-2]);
            if(op.get_size() == 1){
                op = Operation(data[user_cursor-1]);
            }
        }else{
            op = Operation(data[user_cursor-1]);
        }

        user_cursor -= op.get_size();
        if(breakpoints->contains(user_cursor-1) && Operation(breakpoints->at(user_cursor-1)).get_size() == 2){
            user_cursor--;
        }
    }
}

size_t PgmComponent::get_user_cursor(){
    return user_cursor;
}

void PgmComponent::hide_user_cursor(){
    this->show_user_cursor = false;
}

bool PgmComponent::get_show_user_cursor(){
    return this->show_user_cursor;
}

void PgmComponent::reset_user_cursor(){
    if(!this->show_user_cursor){
        this->user_cursor = this->cursor;
    }
}
