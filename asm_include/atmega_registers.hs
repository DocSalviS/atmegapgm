; ATmega 328/168 register and register bit names
; Registers
UDR0      = 0xC6; Mem/EXT-IO - USART I/O Data Register
UBRR0H    = 0xC5; Mem/EXT-IO - USART Baud Rate Register High
UBRR0L    = 0xC4; Mem/EXT-IO - USART Baud Rate Register Low
UCSR0C    = 0xC2; Mem/EXT-IO - USART MSPIM Control and Status Register 0 C
UCSR0B    = 0xC1; Mem/EXT-IO - USART MSPIM Control and Status Register 0 B
UCSR0A    = 0xC0; Mem/EXT-IO - USART MSPIM Control and Status Register 0 A
TWAMR     = 0xBD; Mem/EXT-IO - 2-wire Serial Interface  Address Mask
TWCR      = 0xBC; Mem/EXT-IO - 2-wire Serial Interface  Control Register
TWDR      = 0xBB; Mem/EXT-IO - 2-wire Serial Interface Data Register
TWAR      = 0xBA; Mem /EXT-IO- 2-wire Serial Interface  Address Register
TWSR      = 0xB9; Mem/EXT-IO - 2-wire Serial Interface
TWBR      = 0xB8; Mem/EXT-IO - 2-wire Serial Interface Bit Rate Register
ASSR      = 0xB6; Mem/EXT-IO - Asynchronous Status Register
OCR2B     = 0xB4; Mem/EXT-IO - Timer/Counter2 Output Compare Register B
OCR2A     = 0xB3; Mem/EXT-IO - Timer/Counter2 Output Compare Register A
TCNT2     = 0xB2; Mem/EXT-IO - Timer/Counter2 8-bit
TCCR2B    = 0xB1; Mem/EXT-IO - Timer/Counter2 Control Register B
TCCR2A    = 0xB0; Mem /EXT-IO- Timer/Counter2 Control Register A
OCR1BH    = 0x8B; Mem/EXT-IO - Timer/Counter1 - Output Compare Register B High Byte
OCR1BL    = 0x8A; Mem/EXT-IO - Timer/Counter1 - Output Compare Register B Low Byte
OCR1AH    = 0x89; Mem/EXT-IO - Timer/Counter1 - Output Compare Register A High Byte
OCR1AL    = 0x88; Mem/EXT-IO - Timer/Counter1 - Output Compare Register A Low Byte
ICR1H     = 0x87; Mem/EXT-IO - Timer/Counter1 - Input Capture Register High Byte
ICR1L     = 0x86; Mem/EXT-IO - Timer/Counter1 - Input Capture Register Low Byte
TCNT1H    = 0x85; Mem/EXT-IO - Timer/Counter1 - Counter Register High Byte
TCNT1L    = 0x84; Mem/EXT-IO - Timer/Counter1 - Counter Register Low Byte
TCCR1C    = 0x82; Mem/EXT-IO - Timer/Counter1 Output Compare Register C
TCCR1B    = 0x81; Mem/EXT-IO - Timer/Counter1 Output Compare Register B
TCCR1A    = 0x80; Mem/EXT-IO - Timer/Counter1 Output Compare Register A
DIDR1     = 0x7F; Mem/EXT-IO - Digital Input Disable Register 1
DIDR0     = 0x7E; Mem/EXT-IO - Digital Input Disable Register 0
ADMUX     = 0x7C; Mem/EXT-IO - ADC multiplexer
ADCSRB    = 0x7B; Mem/EXT-IO - ADC Control and Status Register B
ADCSRA    = 0x7A; Mem/EXT-IO - ADC Control and Status Register A
ADCH      = 0x79; Mem/EXT-IO - ADC Data Register High byte
ADCL      = 0x78; Mem/EXT-IO - ADC Data Register Low byte
TIMSK2    = 0x70; Mem/EXT-IO - Timer2 Interrupt Mask Register
TIMSK1    = 0x6F; Mem/EXT-IO - Timer1 Interrupt Mask Register
TIMSK0    = 0x6E; Mem/EXT-IO - Timer0 Interrupt Mask Register
PCMSK2    = 0x6D; Mem/EXT-IO - Pin Change Mask Register 2
PCMSK1    = 0x6C; Mem/EXT-IO - Pin Change Mask Register 1
PCMSK0    = 0x6B; Mem/EXT-IO - Pin Change Mask Register 0
EICRA     = 0x69; Mem/EXT-IO - External Interrupt Control Register A
PCICR     = 0x68; Mem/EXT-IO - Pin Change Interrupt Control Register
OSCCAL    = 0x66; Mem/EXT-IO - Oscillator Calibration Register
PRR       = 0x64; Mem/EXT-IO - Power Reduction Register
CLKPR     = 0x61; Mem/EXT-IO - Clock Prescale Register
WDTCSR    = 0x60; Mem/EXT-IO - Watchdog Timer Control Register
SREG       = 0x3F; I/O - AVR Status Register
SREG_M    = 0x5F; Mem - AVR Status Register
SPH        = 0x3E; I/O - Stack Pointer High
SPH_M     = 0x5E; Mem - Stack Pointer High
SPL        = 0x3D; I/O - Stack Pointer Low Register
SPL_M     = 0x5D; Mem - Stack Pointer Low Register
SPMCSR     = 0x37; I/O - Store Program Memory Control and Status Register
SPMCSR_M  = 0x57; Mem - Store Program Memory Control and Status Register
MCUCR      = 0x35; I/O - MCU Control Register
MCUCR_M   = 0x55; Mem - MCU Control Register
MCUSR      = 0x34; I/O - MCU Status Register
MCUSR_M   = 0x54; Mem - MCU Status Register
SMCR       = 0x33; I/O - Sleep Mode Control Register
SMCR_M    = 0x53; Mem - Sleep Mode Control Register
ACSR       = 0x30; I/O - Analog Comparator Control and Status Register
ACSR_M    = 0x50; Mem - Analog Comparator Control and Status Register
SPDR       = 0x2E; I/O - SPI Data Register
SPDR_M    = 0x4E; Mem - SPI Data Register
SPSR       = 0x2D; I/O - SPI Status Register
SPSR_M    = 0x4D; Mem - SPI Status Register
SPCR       = 0x2C; I/O - SPI Control Register
SPCR_M    = 0x4C; Mem - SPI Control Register
GPIOR2     = 0x2B; I/O - General Purpose I/O Register 2
GPIOR2_M  = 0x4B; Mem - General Purpose I/O Register 2
GPIOR1     = 0x2A; I/O -  General Purpose I/O Register 1
GPIOR1_M  = 0x4A; Mem -  General Purpose I/O Register 1
OCR0B      = 0x28; I/O - Timer/Counter0 Output Compare Register B
OCR0B_M   = 0x48; Mem - Timer/Counter0 Output Compare Register B
OCR0A      = 0x27; I/O - Timer/Counter0 Output Compare Register A
OCR0A_M   = 0x47; Mem - Timer/Counter0 Output Compare Register A
TCNT0      = 0x26; I/O - Timer/Counter0 8-bit
TCNT0_M   = 0x46; Mem - Timer/Counter0 8-bit
TCCR0B     = 0x25; I/O - Timer/Counter0 Control Register B
TCCR0B_M  = 0x45; Mem - Timer/Counter0 Control Register B
TCCR0A     = 0x24; I/O - Timer/Counter0 Control Register A
TCCR0A_M  = 0x44; Mem - Timer/Counter0 Control Register A
GTCCR      = 0x23; I/O - General Timer/Counter Control Register
GTCCR_M   = 0x43; Mem - General Timer/Counter Control Register
EEARH      = 0x22; I/O - EEPROM Address Register High Byte 5.
EEARH_M   = 0x42; Mem - EEPROM Address Register High Byte 5.
EEARL      = 0x21; I/O - EEPROM Address Register Low Byte
EEARL_M   = 0x41; Mem - EEPROM Address Register Low Byte
EEDR       = 0x20; I/O - EEPROM Data Register
EEDR_M    = 0x40; Mem - EEPROM Data Register
EECR       = 0x1F; I/O - EEPROM Control Register
EECR_M    = 0x3F; Mem - EEPROM Control Register
GPIOR0     = 0x1E; I/O - General Purpose I/O Register 0
GPIOR0_M  = 0x3E; Mem - General Purpose I/O Register 0
EIMSK      = 0x1D; I/O - External Interrupt Mask Register
EIMSK_M   = 0x3D; Mem - External Interrupt Mask Register
EIFR       = 0x1C; I/O - External Interrupt Flag Register
EIFR_M    = 0x3C; Mem - External Interrupt Flag Register
PCIFR      = 0x1B; I/O - Pin Change Interrupt Flag Register
PCIFR_M   = 0x3B; Mem - Pin Change Interrupt Flag Register
TIFR2      = 0x17; I/O - Timer/Counter 2 Interrupt Flag Register
TIFR2_M   = 0x37; Mem - Timer/Counter 2 Interrupt Flag Register
TIFR1      = 0x16; I/O - Timer/Counter 1 Interrupt Flag Register
TIFR1_M   = 0x36; Mem - Timer/Counter 1 Interrupt Flag Register
TIFR0      = 0x15; I/O - Timer/Counter 0 Interrupt Flag Register
TIFR0_M   = 0x35; Mem - Timer/Counter 0 Interrupt Flag Register
PORTD      = 0x0B; I/O - The Port D Data Register
PORTD_M   = 0x2B; Mem - The Port D Data Register
DDRD       = 0x0A; I/O - The Port D Data Direction Register
DDRD_M    = 0x2A; Mem - The Port D Data Direction Register
PIND       = 0x09; I/O - The Port D Input Pins Address
PIND_M    = 0x29; Mem - The Port D Input Pins Address
PORTC      = 0x08; I/O - Port C Data Register
PORTC_M   = 0x28; Mem - Port C Data Register
DDRC       = 0x07; I/O - Port C Data Direction Register
DDRC_M    = 0x27; Mem - Port C Data Direction Register
PINC       = 0x06; I/O - Port C Input Pins Address
PINC_M    = 0x26; Mem - Port C Input Pins Address
PORTB      = 0x05; I/O - Port B Data Register
PORTB_M   = 0x25; Mem - Port B Data Register
DDRB       = 0x04; I/O - Port B Data Direction Register
DDRB_M    = 0x24; Mem - Port B Data Direction Register
PINB       = 0x03; I/O - Port B Input Pins Address
PINB_M    = 0x23; Mem - Port B Input Pins Address
; Bits
; UCSR0C Bits - USART MSPIM Control and Status Register 0 C
UMSEL01   = 7
UMSEL00   = 6
UPM01     = 5
UPM00     = 4
USBS0     = 3
UCSZ01    = 2
UDORD0    = 2
UCSZ00    = 1
UCPHA0    = 1
UCPOL0    = 0
; UCSR0B Bits - USART MSPIM Control and Status Register 0 B
RXCIE0    = 7
TXCIE0    = 6
UDRIE0    = 5
RXEN0     = 4
TXEN0     = 3
UCSZ02    = 2
RXB80     = 1
TXB80     = 0
; UCSR0A Bits - USART MSPIM Control and Status Register 0 A
RXC0      = 7
TXC0      = 6
UDRE0     = 5
FE0       = 4
DOR0      = 3
UPE0      = 2
U2X0      = 1
MPCM0     = 0
; TWAMR Bits - 2-wire Serial Interface  Address Mask
TWAM6     = 7
TWAM5     = 6
TWAM4     = 5
TWAM3     = 4
TWAM2     = 3
TWAM1     = 2
TWAM0     = 1
; TWCR Bits - 2-wire Serial Interface  Control Register
TWINT     = 7
TWEA      = 6
TWSTA     = 5
TWSTO     = 4
TWWC      = 3
TWEN      = 2
TWIE      = 0
; TWAR Bits - 2-wire Serial Interface  Address Register
TWA6      = 7
TWA5      = 6
TWA4      = 5
TWA3      = 4
TWA2      = 3
TWA1      = 2
TWA0      = 1
TWGCE     = 0
; TWSR Bits - 2-wire Serial Interface
TWS7      = 7
TWS6      = 6
TWS5      = 5
TWS4      = 4
TWS3      = 3
TWPS1     = 1
TWPS0     = 0
; ASSR Bits - Asynchronous Status Register
EXCLK     = 6
AS2       = 5
TCN2UB    = 4
OCR2AUB   = 3
OCR2BUB   = 2
TCR2AUB   = 1
TCR2BUB   = 0
; TCCR2B Bits - Timer/Counter2 Control Register B
FOC2A     = 7
FOC2B     = 6
WGM22     = 3
CS22      = 2
CS21      = 1
CS20      = 0
; TCCR2A Bits - Timer/Counter2 Control Register A
COM2A1    = 7
COM2A0    = 6
COM2B1    = 5
COM2B0    = 4
WGM21     = 1
WGM20     = 0
; TCCR1C Bits - Timer/Counter1 Output Compare Register C
FOC1A     = 7
FOC1B     = 6
; TCCR1B Bits - Timer/Counter1 Output Compare Register B
ICNC1     = 7
ICES1     = 6
WGM13     = 4
WGM12     = 3
CS12      = 2
CS11      = 1
CS10      = 0
; TCCR1A Bits - Timer/Counter1 Output Compare Register A
COM1A1    = 7
COM1A0    = 6
COM1B1    = 5
COM1B0    = 4
WGM11     = 1
WGM10     = 0
; DIDR1 Bits - Digital Input Disable Register 1
AIN1D     = 1
AIN0D     = 0
; DIDR0 Bits - Digital Input Disable Register 0
ADC5D     = 5
ADC4D     = 4
ADC3D     = 3
ADC2D     = 2
ADC1D     = 1
ADC0D     = 0
; ADMUX Bits - ADC multiplexer
REFS1     = 7
REFS0     = 6
ADLAR     = 5
MUX3      = 3
MUX2      = 2
MUX1      = 1
MUX0      = 0
; ADCSRB Bits - ADC Control and Status Register B
ACME      = 6
ADTS2     = 2
ADTS1     = 1
ADTS0     = 0
; ADCSRA Bits - ADC Control and Status Register A
ADEN      = 7
ADSC      = 6
ADATE     = 5
ADIF      = 4
ADIE      = 3
ADPS2     = 2
ADPS1     = 1
ADPS0     = 0
; TIMSK2 Bits - Timer2 Interrupt Mask Register
OCIE2B    = 2
OCIE2A    = 1
TOIE2     = 0
; TIMSK1 Bits - Timer1 Interrupt Mask Register
ICIE1     = 5
OCIE1B    = 2
OCIE1A    = 1
TOIE1     = 0
; TIMSK0 Bits - Timer0 Interrupt Mask Register
OCIE0B    = 2
OCIE0A    = 1
TOIE0     = 0
; PCMSK2 Bits - Pin Change Mask Register 2
PCINT23   = 7
PCINT22   = 6
PCINT21   = 5
PCINT20   = 4
PCINT19   = 3
PCINT18   = 2
PCINT17   = 1
PCINT16   = 0
; PCMSK1 Bits - Pin Change Mask Register 1
PCINT14   = 6
PCINT13   = 5
PCINT12   = 4
PCINT11   = 3
PCINT10   = 2
PCINT9    = 1
PCINT8    = 0
; PCMSK0 Bits - Pin Change Mask Register 0
PCINT7    = 7
PCINT6    = 6
PCINT5    = 5
PCINT4    = 4
PCINT3    = 3
PCINT2    = 2
PCINT1    = 1
PCINT0    = 0
; EICRA Bits - External Interrupt Control Register A
ISC11     = 3
ISC10     = 2
ISC01     = 1
ISC00     = 0
; PCICR Bits - Pin Change Interrupt Control Register
PCIE2     = 2
PCIE1     = 1
PCIE0     = 0
; PRR Bits - Power Reduction Register
PRTWI     = 7
PRTIM2    = 6
PRTIM0    = 5
PRTIM1    = 3
PRSPI     = 2
PRUSART0  = 1
PRADC     = 0
; CLKPR Bits - Clock Prescale Register
CLKPCE    = 7
CLKPS3    = 3
CLKPS2    = 2
CLKPS1    = 1
CLKPS0    = 0
; WDTCSR Bits - Watchdog Timer Control Register
WDIF      = 7
WDIE      = 6
WDP3      = 5
WDCE      = 4
WDE       = 3
WDP2      = 2
WDP1      = 1
WDP0      = 0
; SREG Bits - AVR Status Register
I         = 7
T         = 6
H         = 5
S         = 4
V         = 3
N         = 2
Z         = 1
C         = 0
; SPH Bits - Stack Pointer High
SP10      = 2
SP9       = 1
SP8       = 0
; SPL Bits - Stack Pointer Low Register
SP7       = 7
SP6       = 6
SP5       = 5
SP4       = 4
SP3       = 3
SP2       = 2
SP1       = 1
SP0       = 0
; SPMCSR Bits - Store Program Memory Control and Status Register
SPMIE     = 7
RWWSB     = 6
SIGRD     = 5
RWWSRE    = 4
BLBSET    = 3
PGWRT     = 2
PGERS     = 1
SPMEN     = 0
; MCUCR Bits - MCU Control Register
BODS      = 6
BODSE     = 5
PUD       = 4
IVSEL     = 1
IVCE      = 0
; MCUSR Bits - MCU Status Register
WDRF      = 3
BORF      = 2
EXTRF     = 1
PORF      = 0
; SMCR Bits - Sleep Mode Control Register
SM2       = 3
SM1       = 2
SM0       = 1
SE        = 0
; ACSR Bits - Analog Comparator Control and Status Register
ACD       = 7
ACBG      = 6
ACO       = 5
ACI       = 4
ACIE      = 3
ACIC      = 2
ACIS1     = 1
ACIS0     = 0
; SPSR Bits - SPI Status Register
SPIF      = 7
WCOL      = 6
SPI2X     = 0
; SPCR Bits - SPI Control Register
SPIE      = 7
SPE       = 6
DORD      = 5
MSTR      = 4
CPOL      = 3
CPHA      = 2
SPR1      = 1
SPR0      = 0
; TCCR0B Bits - Timer/Counter0 Control Register B
FOC0A     = 7
FOC0B     = 6
WGM02     = 3
CS02      = 2
CS01      = 1
CS00      = 0
; TCCR0A Bits - Timer/Counter0 Control Register A
COM0A1    = 7
COM0A0    = 6
COM0B1    = 5
COM0B0    = 4
WGM01     = 1
WGM00     = 0
; GTCCR Bits - General Timer/Counter Control Register
TSM       = 7
PSRASY    = 1
PSRSYNC   = 0
; EECR Bits - EEPROM Control Register
EEPM1     = 5
EEPM0     = 4
EERIE     = 3
EEMPE     = 2
EEPE      = 1
EERE      = 0
; EIMSK Bits - External Interrupt Mask Register
INT1      = 1
INT0      = 0
; EIFR Bits - External Interrupt Flag Register
INTF1     = 1
INTF0     = 0
; PCIFR Bits - Pin Change Interrupt Flag Register
PCIF2     = 2
PCIF1     = 1
PCIF0     = 0
; TIFR2 Bits - Timer/Counter 2 Interrupt Flag Register
OCF2B     = 2
OCF2A     = 1
TOV2      = 0
; TIFR1 Bits - Timer/Counter 1 Interrupt Flag Register
ICF1      = 5
OCF1B     = 2
OCF1A     = 1
TOV1      = 0
; TIFR0 Bits - Timer/Counter 0 Interrupt Flag Register
OCF0B     = 2
OCF0A     = 1
TOV0      = 0
; PORTD Bits - The Port D Data Register
PORTD7    = 7
PORTD6    = 6
PORTD5    = 5
PORTD4    = 4
PORTD3    = 3
PORTD2    = 2
PORTD1    = 1
PORTD0    = 0
; DDRD Bits - The Port D Data Direction Register
DDD7      = 7
DDD6      = 6
DDD5      = 5
DDD4      = 4
DDD3      = 3
DDD2      = 2
DDD1      = 1
DDD0      = 0
; PIND Bits - The Port D Input Pins Address
PIND7     = 7
PIND6     = 6
PIND5     = 5
PIND4     = 4
PIND3     = 3
PIND2     = 2
PIND1     = 1
PIND0     = 0
; PORTC Bits - Port C Data Register
PORTC6    = 6
PORTC5    = 5
PORTC4    = 4
PORTC3    = 3
PORTC2    = 2
PORTC1    = 1
PORTC0    = 0
; DDRC Bits - Port C Data Direction Register
DDC6      = 6
DDC5      = 5
DDC4      = 4
DDC3      = 3
DDC2      = 2
DDC1      = 1
DDC0      = 0
; PINC Bits - Port C Input Pins Address
PINC6     = 6
PINC5     = 5
PINC4     = 4
PINC3     = 3
PINC2     = 2
PINC1     = 1
PINC0     = 0
; PORTB Bits - Port B Data Register
PORTB7    = 7
PORTB6    = 6
PORTB5    = 5
PORTB4    = 4
PORTB3    = 3
PORTB2    = 2
PORTB1    = 1
PORTB0    = 0
; DDRB Bits - Port B Data Direction Register
DDB7      = 7
DDB6      = 6
DDB5      = 5
DDB4      = 4
DDB3      = 3
DDB2      = 2
DDB1      = 1
DDB0      = 0
; PINB Bits - Port B Input Pins Address
PINB7     = 7
PINB6     = 6
PINB5     = 5
PINB4     = 4
PINB3     = 3
PINB2     = 2
PINB1     = 1
PINB0     = 0

; Arduino ports
; Porta D -> D0-D7
; Porta B -> D8-D13
; Porta C -> A0-A5 (a0-A6 for Nano)
;
D0_7_MODE = DDRD
D0_7_PORT = PORTD
D0_7_IN   = PIND

D0_BIT    = PORTD0
D1_BIT    = PORTD1
D2_BIT    = PORTD2
D3_BIT    = PORTD3
D4_BIT    = PORTD4
D5_BIT    = PORTD5
D6_BIT    = PORTD6
D7_BIT    = PORTD7

D8_13_MODE = DDRB
D8_13_PORT = PORTB
D8_13_IN  = PINB

D8_BIT    = PORTb0
D9_BIT    = PORTB1
D10_BIT    = PORTB2
D11_BIT    = PORTB3
D12_BIT    = PORTB4
D13_BIT    = PORTB5

A_MODE    = DDRC
A_PORT    = PORTC
A_IN      = PINC

A0_BIT    = PORTC0
A1_BIT    = PORTC1
A2_BIT    = PORTC2
A3_BIT    = PORTC3
A4_BIT    = PORTC4
A5_BIT    = PORTC5
; On Nano only
A6_BIT    = PORTC6
