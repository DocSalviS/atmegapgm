#ifndef __PGM_COMPONENT_H__
#define __PGM_COMPONENT_H__

#include "component.h"

class PgmComponent : public Component {
    public:
        PgmComponent(uint16_t* data, size_t size, std::map<uint16_t, uint16_t> *breakpoints);
        void draw() override;
        void set_cursor(size_t cursor);
        void inc_user_cursor();
        void dec_user_cursor();
        void hide_user_cursor();
        bool get_show_user_cursor();
        size_t get_user_cursor();
    private:

        void reset_user_cursor();

        uint16_t* data;
        size_t size;
        std::map<uint16_t, uint16_t> *breakpoints;
        size_t cursor;
        size_t user_cursor;
        size_t start_addr;
        size_t next_addr;
        bool show_user_cursor;
};

#endif
