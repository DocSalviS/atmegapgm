#ifndef __SERIAL_COMPONENT_H__
#define __SERIAL_COMPONENT_H__

#include "avr.h"
#include "component.h"

/*
 * Implements the 'serial' window in the interface
 * Receive characters to write through the callback 'write' method
 * Sends every char received from the user through the 'uart_receive' method of avr. component
 */

class SerialComponent : public Component
{
  public:
    SerialComponent(AVR* cpu);
    virtual ~SerialComponent();
    void read(uint8_t ch);
    void draw() override;
    void set_win(WINDOW* win, int x, int y, int width, int height);
    void refresh();

  private:
    char *inbuffer;
    int inbuffsize;
    int inbuffpos;
    bool scrolled;
    AVR* cpu;
    WINDOW* input;
    WINDOW* output;
};

#endif
