#ifndef __BASIC_IO_COMPONENT_H__
#define __BASIC_IO_COMPONENT_H__

#include "component.h"
#include "ioportelement.h"

#define NPORTS  3

class BasicIOComponent : public Component
{
    public:
        BasicIOComponent(uint8_t* iomem);
        ~BasicIOComponent();
        void draw() override;
        void set_elements_win(WINDOW* win) override;
        void update_io_state();
        void toggleInput(uint8_t button);
    private:
        uint8_t* iomem;
        IOPortElement* ports[NPORTS];

};

#endif
