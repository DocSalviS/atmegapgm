#ifndef __DATA_COMPONENT_H__
#define __DATA_COMPONENT_H__

#include "component.h"
#include "rwoperations.h"


class DataComponent : public Component {
    public:
        DataComponent(uint8_t* data, size_t size);
        void draw() override;
        void select(size_t index);
        void set_wr_address(size_t index);
        void set_rd_address(size_t index);
        void set_dreg_address(size_t index, uint8_t operation);
    private:
        size_t selected;
        size_t mem_wr;
        size_t mem_rd;
        size_t dreg;
        uint8_t dreg_op;
        uint8_t* data;
        size_t size;
        int get_num_bytes(int width);
};

#endif
