#ifndef __ELEMENT_H__
#define __ELEMENT_H__

#include <ncurses.h>

class Element {
    public:
        Element(WINDOW* win, int x, int y);
        virtual ~Element();
        virtual void draw();

        void set_win(WINDOW* win);
    protected:
        WINDOW* win;
        int x, y;
};

#endif
