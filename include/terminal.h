#ifndef __TERMINAL_H__
#define __TERMINAL_H__

// #include <unistd.h>
#include <termios.h>
// #include <stdio.h>
#include <ncurses.h>
#include <vector>
#include "tab.h"
#include "term_colors.h"

class Terminal {
    public:
        Terminal();
        ~Terminal();
        void add_tab(Tab* tab);
        void run();
    private:
        WINDOW* top_bar;
        WINDOW* center;
        struct termios old;
        std::vector<Tab*> tabs;
        size_t selected_tab;
        void raw_keyboard();
        void std_keyboard();

        void draw_top_bar();
};

#endif
