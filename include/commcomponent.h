#ifndef __COMM_COMPONENT_H__
#define __COMM_COMPONENT_H__

#include "component.h"
#include <string>
#include <vector>

class CommComponent : public Component {
    public:
        CommComponent();
        void draw() override;
        void add_command(char ch, std::string name);
    private:
        std::vector<std::pair<char, std::string>> commands;
};

#endif
