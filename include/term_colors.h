#ifndef __COLORS_H__
#define __COLORS_H__

// Colors for register and I/O space and to highlight in/read operations
#define REGISTER_BG             COLOR_BLUE
#define REGISTER_FG             COLOR_WHITE
#define IO_BG                   COLOR_CYAN
#define IO_FG                   COLOR_BLACK
#define STANDARD_PAIR           1
#define DISABLED_PAIR           2
#define DEFAULT_PAIR            3
#define REGISTERS_PAIR          4
#define IO_PAIR                 5
#define RD_PAIR                 6
#define RDWR_PAIR               7

#endif
