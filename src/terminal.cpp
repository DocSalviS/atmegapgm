#include "terminal.h"

Terminal::Terminal() {
    initscr();
    cbreak();
    noecho();
    curs_set(0);
    keypad(stdscr, TRUE);

    start_color();

    // chtype pair = getbkgd(stdscr);
    // short bg, fg;
    // pair_content(pair, &fg, &bg);
    init_pair(STANDARD_PAIR, COLOR_WHITE, COLOR_BLACK);
    init_pair(DISABLED_PAIR, COLOR_YELLOW, COLOR_BLACK);
    init_pair(DEFAULT_PAIR, COLOR_WHITE, COLOR_BLACK);
    init_pair(REGISTERS_PAIR, REGISTER_FG, REGISTER_BG);
    init_pair(IO_PAIR , COLOR_BLACK, COLOR_CYAN);

    init_pair(RD_PAIR, COLOR_CYAN, COLOR_BLACK);
    init_pair(RDWR_PAIR, COLOR_BLACK, COLOR_GREEN);

    use_default_colors();


    this->top_bar = newwin(1, COLS, 0, 0);
    this->center = newwin(LINES - 1, COLS, 1, 0);
}

Terminal::~Terminal() {
    delwin(this->top_bar);
    delwin(this->center);
    endwin();
}

void Terminal::add_tab(Tab* tab) {
    tab->set_win(this->center);
    this->tabs.push_back(tab);
}

void Terminal::raw_keyboard() {
    struct termios old, new1;
    tcgetattr(0, &old); /* grab old terminal i/o settings */
    new1 = old; /* make new settings same as old settings */
    new1.c_lflag &= ~ICANON; /* disable buffered i/o */
    new1.c_lflag &= ~ECHO; /* set echo mode */
    tcsetattr(0, TCSANOW, &new1); /* use these new terminal i/o settings now */
}

void Terminal::std_keyboard() {
    tcsetattr(0, TCSANOW, &old);
}

void Terminal::run() {

    if(this->tabs.size() == 0) {
        return;
    }

    bool need_tabs = this->tabs.size() > 1;

    int ch;
    this->selected_tab = 0;

    if(need_tabs) {
        this->draw_top_bar();
    } else {
        this->tabs[0]->set_win(stdscr);
    }

    Tab* current_tab = this->tabs[selected_tab];
    current_tab->clear();
    current_tab->draw();
    current_tab->refresh();

    bool done = false;
    raw_keyboard();
    while(!done) {
        ch = getch();
        switch(ch) {
            case '\t':
                if(need_tabs) {
                    current_tab->clear();
                    selected_tab = (selected_tab + 1) % this->tabs.size();
                    current_tab = this->tabs[selected_tab];
                    current_tab->draw();
                    current_tab->refresh();
                    this->draw_top_bar();
                }
                break;
            default:
                current_tab->handle_input(ch);
                // if(ch=='q')
                if(ch == 0x18)   // CTRL Q ^X
                    done = true;
                break;
        }
    }
    std_keyboard();
}

void Terminal::draw_top_bar() {
    int cursor = 0;
    for(const auto& tab : this->tabs) {

        if(tab == this->tabs[selected_tab]) {
            wattron(stdscr, A_REVERSE);
        }

        std::string name = "[" + tab->get_name() + "]";
        mvwprintw(stdscr, 0, cursor, name.c_str());
        cursor += name.length() + 1;

        wattroff(stdscr, A_REVERSE);
    }
    wrefresh(stdscr);
}
