#include "step_tab.h"
#include <bit>

StepTab::StepTab(AVR* cpu) {
    this->cpu = cpu;
    this->name = "Step Tab";
    this->data = new DataComponent(cpu->get_data_ptr(), DATA_SIZE);
    this->code = new PgmComponent(cpu->get_pgm_ptr(), PROG_SIZE, cpu->get_breakpoints());
    this->io = new BasicIOComponent(cpu->get_data_ptr()+0x20);
    this->serial = new SerialComponent(cpu);

    this->reg = new RegComponent();

    this->commands = new CommComponent();
    this->commands->add_command(0x01, "Step");  // ^A
    this->commands->add_command(0x08, "Restart"); // ^H
    this->commands->add_command(0x18, "Exit"); // ^X
    this->commands->add_command(0x14, "RUN/Stop"); // ^T
    this->commands->add_command(0x06, "Refresh"); // ^F
    this->commands->add_command(0x02, "BP +/-"); // ^B
    this->commands->add_command(0x15, "...Bottoni"); // ^B

    this->running = false;
    this->run_thread = nullptr;
}

StepTab::~StepTab() {
    delete this->data;
    delete this->code;
    delete this->commands;
    delete this->io;
    delete this->reg;
    delete this->serial;
}

void StepTab::draw() {

    ncurses_mutex.lock();

    this->data->draw();
    this->data->refresh();

    this->code->draw();
    this->code->refresh();

    this->commands->draw();
    this->commands->refresh();

    this->io->draw();
    this->io->refresh();

    this->serial->draw();
    this->serial->refresh();

    this->reg->draw();
    this->reg->refresh();

    ncurses_mutex.unlock();

}

/* Caratteri di controllo usabili:
 * ^A 01
 * ^B 02
 * ^C NO (break)
 * ^D 04
 * ^E 05
 * ^F 06
 * ^G 07
 * ^H 08
 * ^I NO TAB (usato per cambio tab - non servirebbe...)
 * ^J 0A
 * ^K 0B
 * ^L 0C
 * ^M 0A (=^J)
 * ^N 0E
 * ^O 0F
 * ^P 10
 * ^Q NO (nulla)
 * ^R 12
 * ^S NO (Blocca l'input - non riprende)
 * ^T 14
 * ^U 15
 * ^V 16
 * ^W 17
 * ^X 18
 * ^Y 10
 * ^Z NO (termina)
 *
 * ^A 01 -> Avanza (s Step)
 * ^X 18 ->  18 -> Esci (q quit)
 * ^H 08 -> reset (r)
 * ^T 14 -> Run/Stop (t)
 * ^F 06 -> Refresh (f)
 * ^B 02 -> Breackpoint (b)
 * ^U 15 -> Tasto 1 (1)
 * ^V 16 -> Tasto 2 (2)
 * ^W 17 -> Tasto 3 (3)
 */

void StepTab::handle_input(int ch) {
    // fprintf(stderr,"Char = %02X\n", ch);
    switch(ch) {
        case 0x01:  // ^A - avanza (step) (ex 's')
            stop_run();
            cpu->step();
            this->code->hide_user_cursor();
            this->update();
            break;
        case 0x08: // ^H Reset (ex 'r')
            this->cpu->reset();
            this->code->hide_user_cursor();
            this->update();
            break;
        case 0x14: // ^T (ex 't')
            if(running)
                stop_run();
            else
                continue_run();

            this->code->hide_user_cursor();
            this->update();
            break;
        case 0x06: // ^F Refresh (ex 'f')
            this->code->hide_user_cursor();
            this->update();
            break;
        case KEY_UP:
            this->code->dec_user_cursor();
            this->update();
            break;
        case KEY_DOWN:
            this->code->inc_user_cursor();
            this->update();
            break;
        case 0x02: // ^B Breackpoint (ex 'b')
            if(this->code->get_show_user_cursor()) {
                this->cpu->toggle_breakpoint(this->code->get_user_cursor());
                this->update();
            }
            break;
        case 0x019A:
            update_window(this->win);
            break;
        case 0x15: // ^U Bottone 1 (ex '1')
        case 0x16: // ^V Bottone 2 (ex '2')
        case 0x17: // ^W Bottone 3 (ex '3')
            this->io->toggleInput(ch- 0x15); // (ch-'0');
            break;
        case 0x18: // ^X Exit (ex'q')
            stop_run();
            this->running = false;
            break;
        default:
            this->serial->read((uint8_t) ch);
            break;
    }

    this->draw();
}

void StepTab::update() {
    this->code->set_cursor(this->cpu->get_pc());

    this->reg->set_pc(this->cpu->get_pc());
    this->reg->set_sp(this->cpu->watch_sp());   // Reads SP value, but doesn'mark a read operation
    this->reg->set_sreg(this->cpu->get_status());
    this->reg->set_sp_highlight(this->cpu->get_hl_sp());
    this->reg->set_sreg_highlight(this->cpu->get_hl_status());

    this->data->select(this->cpu->get_hl_mem_addr());
    this->data->set_wr_address(this->cpu->get_hl_wr_addr());
    this->data->set_rd_address(this->cpu->get_hl_rd_addr());
    this->data->set_dreg_address(this->cpu->get_dreg_addr(), this->cpu->get_dregop());

    this->io->update_io_state();
}

void StepTab::update_window(WINDOW* win) {

    unsigned int width, height;
    getmaxyx(win, height, width);

    int io_h = 7;
    int regs_h = 3;
    int comm_h = 3;
    int setial_h = 6;
    int mem_w = std::bit_floor(((width * 2/3)-9)/4)*4 + 9;
    int prog_w = width - mem_w;
    int prog_h = height - comm_h - io_h;
    int mem_h = height - comm_h - setial_h - regs_h;

    // Lato sinistro
    this->code->set_win(win, 0, 0, prog_w, prog_h);
    this->io->set_win(win, 0, prog_h, prog_w, io_h);

    // Lato desttro
    this->data->set_win(win, prog_w, 0, mem_w, mem_h);
    this->serial->set_win(win, prog_w, mem_h, mem_w, setial_h);
    this->reg->set_win(win, prog_w, mem_h + setial_h, mem_w, regs_h);

    // In fondo allo schermo (calcolo dal fondo dello schermo)
    this->commands->set_win(win, 0, height - comm_h, width, comm_h);

    wclear(win);

    this->update();
}

void StepTab::run() {
    while(this->running) {
        uint16_t pc = this->cpu->get_pc();
        Operation op = this->cpu->step();
        if(op.get_type() == Operation::BREAK ||
            pc == this->cpu->get_pc()) {
            this->running = false;
        }

        if(this->cpu->get_hl_wr_addr() < NOADDRESS &&
                this->cpu->get_hl_wr_addr() >= 0x20 &&
                this->cpu->get_hl_wr_addr() <= 0x5f) {

            this->ncurses_mutex.lock();
            this->io->update_io_state();
            this->io->draw();
            this->io->refresh();
            this->ncurses_mutex.unlock();
        }

    }

    this->update();
    set_active(false);
}

void StepTab::continue_run() {
    if(!running) {

        running = true;
        set_active(true);

        if(this->run_thread!=nullptr) {
            this->run_thread->join();
            delete this->run_thread;
        }
        this->run_thread = new std::thread(&StepTab::run, this);
    }
}

void StepTab::stop_run() {
    running = false;

    if(!this->run_thread) return;
    this->run_thread->join();

    delete this->run_thread;
    run_thread = nullptr;

    this->update();
    set_active(false);

}

void StepTab::set_active(bool active) {

    if(active) {
        this->code->set_name("Code (RUNNING)");
    } else {
        this->code->set_name("Code");
    }

    this->code->set_active(active);
    this->data->set_active(active);
    this->commands->set_active(active);
    this->io->set_active(active);
    this->reg->set_active(active);

    this->draw();
}
