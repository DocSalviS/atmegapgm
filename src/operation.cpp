#include <iostream>
#include "operation.h"
#include "slogpp.h"

const op_t Operation::op_list[] = { 
    {"ADD",    ADD,    0b1111110000000000, 0b0000110000000000, 1, 1, TWO_REG_ADDR_5},
    {"ADC",    ADC,    0b1111110000000000, 0b0001110000000000, 1, 1, TWO_REG_ADDR_5},
    {"ADIW",   ADIW,   0b1111111100000000, 0b1001011000000000, 2, 1, REG_IMM_WORD_ADDR},
    {"SUB",    SUB,    0b1111110000000000, 0b0001100000000000, 1, 1, TWO_REG_ADDR_5},
    {"SUBI",   SUBI,   0b1111000000000000, 0b0101000000000000, 1, 1, REG_IMM_ADDR},
    {"SBC",    SBC,    0b1111110000000000, 0b0000100000000000, 1, 1, TWO_REG_ADDR_5},
    {"SBCI",   SBCI,   0b1111000000000000, 0b0100000000000000, 1, 1, REG_IMM_ADDR},
    {"SBIW",   SBIW,   0b1111111100000000, 0b1001011100000000, 2, 1, REG_IMM_WORD_ADDR},
    {"AND",    AND,    0b1111110000000000, 0b0010000000000000, 1, 1, TWO_REG_ADDR_5},
    {"ANDI",   ANDI,   0b1111000000000000, 0b0111000000000000, 1, 1, REG_IMM_ADDR},
    {"OR",     OR,     0b1111110000000000, 0b0010100000000000, 1, 1, TWO_REG_ADDR_5},
    {"ORI",    ORI,    0b1111000000000000, 0b0110000000000000, 1, 1, REG_IMM_ADDR},
    {"EOR",    EOR,    0b1111110000000000, 0b0010010000000000, 1, 1, TWO_REG_ADDR_5},
    {"COM",    COM,    0b1111111000001111, 0b1001010000000000, 1, 1, REG_ADDR_5},
    {"NEG",    NEG,    0b1111111000001111, 0b1001010000000001, 1, 1, REG_ADDR_5},
    {"INC",    INC,    0b1111111000001111, 0b1001010000000011, 1, 1, REG_ADDR_5},
    {"DEC",    DEC,    0b1111111000001111, 0b1001010000001010, 1, 1, REG_ADDR_5},
    {"SER",    SER,    0b1111111100001111, 0b1110111100001111, 1, 1, REG_ADDR_4},
    {"MUL",    MUL,    0b1111110000000000, 0b1001110000000000, 2, 1, TWO_REG_ADDR_5},
    {"MULS",   MULS,   0b1111111100000000, 0b0000001000000000, 2, 1, TWO_REG_ADDR_4},
    {"MULSU",  MULSU,  0b1111111110001000, 0b0000001100000000, 2, 1, TWO_REG_ADDR_3},
    {"FMUL",   FMUL,   0b1111111110001000, 0b0000001100001000, 2, 1, TWO_REG_ADDR_3},
    {"FMULS",  FMULS,  0b1111111110001000, 0b0000001110000000, 2, 1, TWO_REG_ADDR_3},
    {"FMULSU", FMULSU, 0b1111111110001000, 0b0000001110001000, 2, 1, TWO_REG_ADDR_3},

    {"RJMP",   RJMP,   0b1111000000000000, 0b1100000000000000, 2, 1, REL_PROG_ADDR},
    {"IJMP",   IJMP,   0b1111111111111111, 0b1001010000001001, 2, 1, NONE_ADDR},
    {"JMP",    JMP,    0b1111111000001110, 0b1001010000001100, 3, 2, ABS_PROG_ADDR},
    {"RCALL",  RCALL,  0b1111000000000000, 0b1101000000000000, 3, 1, REL_PROG_ADDR},
    {"ICALL",  ICALL,  0b1111111111111111, 0b1001010100001001, 3, 1, NONE_ADDR},
    {"CALL",   CALL,   0b1111111000001110, 0b1001010000001110, 4, 2, ABS_PROG_ADDR},
    {"RET",    RET,    0b1111111111111111, 0b1001010100001000, 4, 1, NONE_ADDR},
    {"RETI",   RETI,   0b1111111111111111, 0b1001010100011000, 4, 1, NONE_ADDR},
    {"CPSE",   CPSE,   0b1111110000000000, 0b0001000000000000, 1, 1, TWO_REG_ADDR_5},
    {"CP",     CP,     0b1111110000000000, 0b0001010000000000, 1, 1, TWO_REG_ADDR_5},
    {"CPC",    CPC,    0b1111110000000000, 0b0000010000000000, 1, 1, TWO_REG_ADDR_5},
    {"CPI",    CPI,    0b1111000000000000, 0b0011000000000000, 1, 1, REG_IMM_ADDR},
    {"SBRC",   SBRC,   0b1111111000001000, 0b1111110000000000, 1, 1, REG_BIT_ADDR},
    {"SBRS",   SBRS,   0b1111111000001000, 0b1111111000000000, 1, 1, REG_BIT_ADDR},
    {"SBIC",   SBIC,   0b1111111100000000, 0b1001100100000000, 1, 1, IOR_BIT_ADDR},
    {"SBIS",   SBIS,   0b1111111100000000, 0b1001101100000000, 1, 1, IOR_BIT_ADDR},
    {"BRBC",   BRBC,   0b1111110000000000, 0b1111010000000000, 1, 1, REL_PROG_BIT_ADDR},
    {"BRBS",   BRBS,   0b1111110000000000, 0b1111000000000000, 1, 1, REL_PROG_BIT_ADDR},

    {"SBI",    SBI,    0b1111111100000000, 0b1001101000000000, 2, 1, IOR_BIT_ADDR},
    {"CBI",    CBI,    0b1111111100000000, 0b1001100000000000, 2, 1, IOR_BIT_ADDR},
    {"LSR",    LSR,    0b1111111000001111, 0b1001010000000110, 1, 1, REG_ADDR_5},
    {"ROR",    ROR,    0b1111111000001111, 0b1001010000000111, 1, 1, REG_ADDR_5},
    {"ASR",    ASR,    0b1111111000001111, 0b1001010000000101, 1, 1, REG_ADDR_5},
    {"SWAP",   SWAP,   0b1111111000001111, 0b1001010000000010, 1, 1, REG_ADDR_5},
    {"BSET",   BSET,   0b1111111110001111, 0b1001010000001000, 1, 1, BIT_ADDR},
    {"BCLR",   BCLR,   0b1111111110001111, 0b1001010010001000, 1, 1, BIT_ADDR},
    {"BST",    BST,    0b1111111000001000, 0b1111101000000000, 1, 1, REG_BIT_ADDR},
    {"BLD",    BLD,    0b1111111000001000, 0b1111100000000000, 1, 1, REG_BIT_ADDR},

    {"MOV",    MOV,    0b1111110000000000, 0b0010110000000000, 1, 1, TWO_REG_ADDR_5},
    {"MOVW",   MOVW,   0b1111111100000000, 0b0000000100000000, 1, 1, TWO_REG_PAIR_ADDR},
    {"LDI",    LDI,    0b1111000000000000, 0b1110000000000000, 1, 1, REG_IMM_ADDR},

    {"ST X",   ST_X,   0b1111111000001111, 0b1001001000001100, 2, 1, REG_INC_ADDR},
    {"ST X+",  ST_X,   0b1111111000001111, 0b1001001000001101, 2, 1, REG_INC_ADDR},
    {"ST -X",  ST_X,   0b1111111000001111, 0b1001001000001110, 2, 1, REG_INC_ADDR},

    {"ST Y",   ST_Y,   0b1111111000001111, 0b1000001000001000, 2, 1, REG_INC_ADDR},
    {"ST Y+",  ST_Y,   0b1111111000001111, 0b1001001000001001, 2, 1, REG_INC_ADDR},
    {"ST -Y",  ST_Y,   0b1111111000001111, 0b1001001000001010, 2, 1, REG_INC_ADDR},
    {"STD Y",  STD_Y,  0b1101001000001000, 0b1000001000001000, 2, 1, REG_OFFS_ADDR},

    {"ST Z",   ST_Z,   0b1111111000001111, 0b1000001000000000, 2, 1, REG_INC_ADDR},
    {"ST Z+",  ST_Z,   0b1111111000001111, 0b1001001000000001, 2, 1, REG_INC_ADDR},
    {"ST -Z",  ST_Z,   0b1111111000001111, 0b1001001000000010, 2, 1, REG_INC_ADDR},
    {"STD Z",  STD_Z,  0b1101001000001000, 0b1000001000000000, 2, 1, REG_OFFS_ADDR},

    {"STS",    STS,    0b1111111000001111, 0b1001001000000000, 2, 2, REG_ABS_ADDR},

    {"LPM",    LPM_R0, 0b1111111111111111, 0b1001010111001000, 2, 1, NONE_ADDR},
    {"LPM Z",  LPM,    0b1111111000001111, 0b1001000000000100, 2, 1, REG_INC_ADDR},
    {"LPM Z+", LPM,    0b1111111000001111, 0b1001000000000101, 2, 1, REG_INC_ADDR},

    {"LD X",   LD_X,   0b1111111000001111, 0b1001000000001100, 1, 1, REG_INC_ADDR},
    {"LD X+",  LD_X,   0b1111111000001111, 0b1001000000001101, 2, 1, REG_INC_ADDR},
    {"LD -X",  LD_X,   0b1111111000001111, 0b1001000000001110, 3, 1, REG_INC_ADDR},

    {"LD Y",   LD_Y,   0b1111111000001111, 0b1000000000001000, 2, 1, REG_INC_ADDR},
    {"LD Y+",  LD_Y,   0b1111111000001111, 0b1001000000001001, 2, 1, REG_INC_ADDR},
    {"LD -Y",  LD_Y,   0b1111111000001111, 0b1001000000001010, 2, 1, REG_INC_ADDR},
    {"LDD Y",  LDD_Y,  0b1101001000001000, 0b1000000000001000, 2, 1, REG_OFFS_ADDR},

    {"LD Z",   LD_Z,   0b1111111000001111, 0b1000000000000000, 2, 1, REG_INC_ADDR},
    {"LD Z+",  LD_Z,   0b1111111000001111, 0b1001000000000001, 2, 1, REG_INC_ADDR},
    {"LD -Z",  LD_Z,   0b1111111000001111, 0b1001000000000010, 2, 1, REG_INC_ADDR},
    {"LDD Z",  LDD_Z,  0b1101001000001000, 0b1000000000000000, 2, 1, REG_OFFS_ADDR},

    {"LDS",    LDS,    0b1111111000001111, 0b1001000000000000, 2, 2, REG_ABS_ADDR},

    {"SPM",    SPM,    0b1111111111111111, 0b1001010111101000, 5, 1, 0},

    {"IN",     IN,     0b1111100000000000, 0b1011000000000000, 1, 1, REG_IOR_ADDR},
    {"OUT",    OUT,    0b1111100000000000, 0b1011100000000000, 1, 1, REG_IOR_ADDR},
    {"PUSH",   PUSH,   0b1111111000001111, 0b1001001000001111, 2, 1, REG_ADDR_5},
    {"POP",    POP,    0b1111111000001111, 0b1001000000001111, 2, 1, REG_ADDR_5},

    {"NOP",    NOP,    0b1111111111111111, 0b0000000000000000, 1, 1, NONE_ADDR},
    {"SLEEP",  SLEEP,  0b1111111111111111, 0b1001010110001000, 1, 1, 0},
    {"WDR",    WDR,    0b1111111111111111, 0b1001010110101000, 1, 1, 0},
    {"BREAK",  BREAK,  0b1111111111111111, 0b1001010110011000, 0, 1, 0},

    {"???",    NONE,   0b1111111111111111, 0b1111111111111111, 1, 1, 0}, 
};

Operation::Operation(uint16_t opcode) {
    //this->opcode = ((opcode & 0xff)<<8) | ((opcode & 0xff00)>>8); //endianness workaround
    this->opcode = opcode;
    SLog::debug("Opcode: 0x%04x", this->opcode);
    
    for(int i=0; i< (int)(sizeof(op_list)/sizeof(op_t)); i++) {
        if((op_list[i].mask1 & opcode) == op_list[i].mask2) {
            SLog::info("Executing %s...", op_list[i].mnemonic);
            this->type = &op_list[i];
            return;
        }
    }

    SLog::error("Opcode not found, setting NONE");
    this->type = &op_list[sizeof(op_list)/sizeof(op_t) - 1];
}

uint8_t Operation::get_cycles() {
    return this->type->cycles;
}

uint8_t Operation::get_size() {
    return this->type->size;
}

const char* Operation::get_mnemonic() {
    return this->type->mnemonic;
}

uint16_t Operation::get_type() {
    return this->type->type;
}


void Operation::get_two_reg_addr_5(uint8_t *rr, uint8_t *rd) {
    *rr = (this->opcode & 0b1000000000)>>5 | (this->opcode & 0b1111);
    *rd = (this->opcode & 0b111110000)>>4;
}

void Operation::get_two_reg_addr_4(uint8_t *rr, uint8_t *rd) {
    *rr = (this->opcode & 0b1111) + 16;
    *rd = ((this->opcode & 0b11110000)>>4) + 16;
}

void Operation::get_two_reg_addr_3(uint8_t *rr, uint8_t *rd) {
    *rr = (this->opcode & 0b111) + 16;
    *rd = ((this->opcode & 0b1110000)>>4) + 16;
}

void Operation::get_reg_imm_addr(uint8_t *rd, uint8_t *imm) {
    *rd = ((this->opcode & 0b11110000)>>4) + 16;
    *imm = (this->opcode & 0b111100000000)>>4 | (this->opcode & 0b1111);
}

void Operation::get_reg_imm_word_addr(uint8_t *rd, uint8_t *imm) {
    *rd = ((this->opcode & 0b110000)>>4)*2 + 24;
    *imm = (this->opcode & 0b1111) | ((this->opcode & 0b11000000)>>2);
}

void Operation::get_reg_addr_5(uint8_t *rd) {
    *rd = (this->opcode & 0b111110000)>>4;
}

void Operation::get_reg_addr_4(uint8_t *rd) {
    *rd = ((this->opcode & 0b11110000)>>4) + 16;
}

void Operation::get_abs_prog_addr(uint32_t *addr, const uint16_t next) {
    *addr  = (this->opcode & 0b111110000)<<13 | (this->opcode & 0b1)<<16 | next;
}

void Operation::get_rel_prog_addr(int16_t *addr){
    *addr = (this->opcode & (0b1<<11)) ? -(((~this->opcode) & 0b111111111111)+1)
                                       :     (this->opcode  & 0b111111111111); 
}

void Operation::get_reg_abs_addr(uint8_t *rd, uint16_t *addr, const uint16_t next) {
    *rd = (this->opcode & 0b111110000)>>4;
    *addr  = next;
}

void Operation::get_reg_bit_addr(uint8_t *rr, uint8_t *bit) {
    *rr = (this->opcode & 0b111110000)>>4;
    *bit = (this->opcode & 0b111);
}

void Operation::get_ior_bit_addr(uint8_t *ior, uint8_t *bit) {
    *ior = ((this->opcode & 0b11111000)>>3) + IO_REG_OFFSET;
    *bit = (this->opcode & 0b111);
}

void Operation::get_rel_prog_bit_addr(int8_t *addr, uint8_t *bit) {
    uint8_t k = (this->opcode & 0b1111111000)>>3;
    *addr = (k & (0b1<<6)) ? -(((~k) & 0b1111111)+1)
                           :     (k  & 0b1111111); 
    *bit = (this->opcode & 0b111);
}

void Operation::get_bit_addr(uint8_t *bit) {
    *bit = (this->opcode & 0b1110000)>>4;
}

void Operation::get_two_reg_pair_addr(uint8_t *rr, uint8_t *rd) {
    *rr = (this->opcode & 0b11110000)>>4;
    *rd = (this->opcode & 0b1111);
}

void Operation::get_reg_ior_addr(uint8_t *rd, uint8_t *ior) {
    *rd = ((this->opcode & 0b111110000)>>4);
    *ior = (((this->opcode & 0b11000000000)>>5) | (this->opcode & 0b1111)) + IO_REG_OFFSET;
}

void Operation::get_reg_inc_addr(uint8_t *rr, uint8_t *inc) {
    *rr = (this->opcode & 0b111110000)>>4;
    *inc = (this->opcode & 0b11);
}

void Operation::get_reg_offs_addr(uint8_t *rr, uint8_t *q) {
    *rr = (this->opcode & 0b111110000)>>4;
    *q =  (this->opcode & 0b111) |
            ((this->opcode & 0b110000000000)>>7) |
            ((this->opcode & 0b10000000000000)>>8);
}

std::string Operation::to_string(uint16_t* prog, uint16_t pc) {

    char str[64];

    switch(this->type->addr_mode) {
        case NONE_ADDR: {
            snprintf(str, 64, "%s", this->get_mnemonic());
            break;
        }
        case REG_ADDR_5: {
            uint8_t rd;
            this->get_reg_addr_5(&rd);
            snprintf(str, 64, "%s r%d", this->get_mnemonic(), rd);
            break;
        }
        case REG_ADDR_4: {
            uint8_t rd;
            this->get_reg_addr_4(&rd);
            snprintf(str, 64, "%s r%d", this->get_mnemonic(), rd);
            break;
        }
        case REG_IMM_ADDR: {
            uint8_t rd, imm;
            this->get_reg_imm_addr(&rd, &imm);
            snprintf(str, 64, "%s r%d, 0x%02x", this->get_mnemonic(), rd, imm);
            break;
        }
        case REG_IMM_WORD_ADDR: {
            uint8_t rd, imm;
            this->get_reg_imm_word_addr(&rd, &imm);
            snprintf(str, 64, "%s r%d, 0x%04x", this->get_mnemonic(), rd, imm);
            break;
        }
        case TWO_REG_ADDR_5: {
            uint8_t rr, rd;
            this->get_two_reg_addr_5(&rr, &rd);
            snprintf(str, 64, "%s r%d, r%d", this->get_mnemonic(), rd, rr);
            break;
        }
        case TWO_REG_ADDR_4: {
            uint8_t rr, rd;
            this->get_two_reg_addr_4(&rr, &rd);
            snprintf(str, 64, "%s r%d, r%d", this->get_mnemonic(), rd, rr);
            break;
        }
        case TWO_REG_ADDR_3: {
            uint8_t rr, rd;
            this->get_two_reg_addr_3(&rr, &rd);
            snprintf(str, 64, "%s r%d, r%d", this->get_mnemonic(), rd, rr);
            break;
        }
        case ABS_PROG_ADDR: {
            uint32_t addr;
            this->get_abs_prog_addr(&addr, prog[pc+1]);
            snprintf(str, 64, "%s 0x%06x", this->get_mnemonic(), addr*2);
            break;
        }
        case REL_PROG_ADDR: {
            int16_t addr;
            this->get_rel_prog_addr(&addr);
            snprintf(str, 64, "%s %+d", this->get_mnemonic(), addr+1);
            break;
        }
        case REG_ABS_ADDR: {
            uint8_t rd;
            uint16_t addr;
            this->get_reg_abs_addr(&rd, &addr, prog[pc+1]);
            snprintf(str, 64, "%s 0x%06x,r%d", this->get_mnemonic(), addr*2, rd);
            break;
        }
        case REG_BIT_ADDR: {
            uint8_t rr, bit;
            this->get_reg_bit_addr(&rr, &bit);
            snprintf(str, 64, "%s r%d(%d)", this->get_mnemonic(), rr, bit);
            break;
        }
        case IOR_BIT_ADDR: {
            uint8_t ior, bit;
            this->get_ior_bit_addr(&ior, &bit);
            snprintf(str, 64, "%s IO%d(%d)", this->get_mnemonic(), ior-IO_REG_OFFSET, bit);
            break;
        }
        case REL_PROG_BIT_ADDR: {
            int8_t addr;
            uint8_t bit;
            this->get_rel_prog_bit_addr(&addr, &bit);
            snprintf(str, 64, "%s(%d) %+d", this->get_mnemonic(), bit, addr+1);
            break;
        }
        case BIT_ADDR: {
            uint8_t bit;
            this->get_bit_addr(&bit);
            snprintf(str, 64, "%s(%d)", this->get_mnemonic(), bit);
            break;
        }
        case TWO_REG_PAIR_ADDR: {
            uint8_t rr, rd;
            this->get_two_reg_pair_addr(&rr, &rd);
            snprintf(str, 64, "%s r%d:r%d, r%d:r%d", this->get_mnemonic(), rd+1, rd, rr+1, rr);
            break;
        }
        case REG_IOR_ADDR: {
            uint8_t rd, ior;
            this->get_reg_ior_addr(&rd, &ior);
            snprintf(str, 64, "%s r%d, IO%d", this->get_mnemonic(), rd, ior-IO_REG_OFFSET);
            break;
        }
        case REG_INC_ADDR: {
            uint8_t rr, inc;
            this->get_reg_inc_addr(&rr, &inc);
            snprintf(str, 64, "%s r%d", this->get_mnemonic(), rr);
            break;
        }
        case REG_OFFS_ADDR: {
            uint8_t rr;
            uint8_t q;
            this->get_reg_offs_addr(&rr, &q);
            snprintf(str, 64, "%s+%d r%d", this->get_mnemonic(), q, rr);
            break;
        }
    }

    return std::string(str);

}
