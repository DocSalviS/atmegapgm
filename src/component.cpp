#include "component.h"
#include "term_colors.h"


Component::~Component() {
    if (this->win != nullptr)
        delwin(this->win);
}

void Component::draw() {
    if(this->active)
        wattron(this->win, COLOR_PAIR(DISABLED_PAIR));
    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);
    mvwprintw(this->win, 0, 1, "[%s]", this->name.c_str());

    wattroff(this->win, COLOR_PAIR(DISABLED_PAIR));
}

void Component::refresh() {
    wrefresh(this->win);
}

void Component::set_win(WINDOW* win, int x, int y, int width, int height) {
    this->win = derwin(win, height, width, y, x);
    set_elements_win(this->win);
}

void Component::set_elements_win(WINDOW* win) {
    (void)win;    // To avoid parameter unused warning
}

void Component::set_name(std::string name) {
    this->name = name;
}

void Component::set_active(bool active) {
    this->active = active;
}
