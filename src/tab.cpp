#include "tab.h"

Tab::Tab() {
    this->win = nullptr;
    this->name = "Tab";
}

void Tab::draw() {
    mvwprintw(this->win, 0, 0, this->name.c_str());
}

void Tab::refresh() {
    wrefresh(this->win);
}

void Tab::handle_input(int ch) {
    // Do nothing
    [[maybe_unused]]int x = ch;
}

void Tab::set_win(WINDOW* win) {
    this->win = win;
    this->update_window(win);
}

void Tab::clear() {
    werase(this->win);
    wrefresh(this->win);
}

std::string Tab::get_name() {
    return this->name;
}

void Tab::update_window(WINDOW* win) {
    [[maybe_unused]]WINDOW* x = win;
}

void Tab::set_name(std::string name) {
    this->name = name;
}
