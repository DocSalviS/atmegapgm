#include "ioportelement.h"

/* IO Ports 8 bits each
 *
 * Arduino ports      PORT    DDR     BIT
 * Porta C -> A0-A5   0x08    0x07    0x06
 * Porta B -> D8-D13  0x05    0x04    0x03
 * Porta D -> D0-D7   0x0B    0x0A    0x09
 *
 */


IOPortElement::IOPortElement(WINDOW* win, int x, int y, uint8_t* iomem, portdata data) : Element(win, x, y), iomem(iomem), data(data) {
}

void IOPortElement::draw() {

    for(int i = 0; i < INTLINES; i++) {
      mvwprintw(win, y+i, x+2, data.lines[i]);
    }
    mvwaddch(win, y+3, x+1, data.port);

    for(int i = 0; i < PORTBITS; i++) {
      uint8_t mask = 0x80 >> i;
      mvwaddch(win, y+3, x+2+i, (iomem[data.base]&mask) ? ((iomem[data.base-1]&mask) ? '*' : 'U') : ((iomem[data.base-1]&mask) ? 'O' : '-'));
      mvwaddch(win, y+4, x+2+i, (iomem[data.base-2]&mask) ? ((iomem[data.base-1]&mask) ? ' ' : 'I') : ((iomem[data.base-1]&mask) ? ' ' : '_'));
    }
}

void IOPortElement::set_elements_win(WINDOW* win) {
    [[maybe_unused]] WINDOW *w = win;
}

void IOPortElement::toggleInput(uint8_t bit) {
    uint8_t mask = 1 << bit;
    if(!(iomem[data.base-1]&mask)) {
        iomem[data.base-2]^=mask;
        this->draw();
    }
}

void IOPortElement::update_io_state() {
}

