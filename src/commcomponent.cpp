#include "commcomponent.h"
#include "term_colors.h"

CommComponent::CommComponent() {
    this->commands = std::vector<std::pair<char, std::string>>();
}

void CommComponent::draw() {
    wattron(this->win, COLOR_PAIR((active) ? DISABLED_PAIR : STANDARD_PAIR));
    wbkgd(this->win, COLOR_PAIR((active) ? DISABLED_PAIR : STANDARD_PAIR));

    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);
    mvwprintw(this->win, 0, 1, "[Commands]");

    wattroff(this->win, COLOR_PAIR((active) ? DISABLED_PAIR : STANDARD_PAIR));
    wattron(this->win, COLOR_PAIR(STANDARD_PAIR));

    int cursor = 1;
    for (size_t i = 0; i < this->commands.size(); i++) {
        mvwprintw(this->win, 1, cursor, "^%c:%s",
                this->commands[i].first + 0x40, // Decodifica i 'control'
                this->commands[i].second.c_str()
        );
        cursor += this->commands[i].second.length() + 4;
    }
    wattroff(this->win, COLOR_PAIR(STANDARD_PAIR));
}

void CommComponent::add_command(char ch, std::string name) {
    this->commands.push_back(std::make_pair(ch, name));
}
