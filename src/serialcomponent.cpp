/* serialelement.cpp
 */

#include "serialcomponent.h"
#include "term_colors.h"
#include <ncurses.h>

SerialComponent::SerialComponent(AVR* cpu) {
    this->cpu = cpu;
    inbuffer = 0;
    inbuffsize = 0;
    inbuffpos = 0;
    scrolled = FALSE;
}

void SerialComponent::set_win(WINDOW* win, int x, int y, int width, int height) {
    Component::set_win(win, x, y, width, height);
    // mvwprintw(this->win, 1, 3, ">");
    cpu->setBufferSize((width - 2)*(height - 3)); // Calcola il massimo numero di carattteri che stanno nel pannello di output)
    this->input = derwin(this->win, 1, width-2, 1, 1);
    wattron(this->input, COLOR_PAIR(REGISTERS_PAIR));
    wbkgd(this->input, COLOR_PAIR(REGISTERS_PAIR));
    scrollok(this->input,FALSE);
    this->output = derwin(this->win, height-3, width-2, 2, 1);
    scrollok(this->output,TRUE);
    inbuffsize = width-2;
    inbuffer = new char[inbuffsize];
    inbuffer[0] = '\0';
}

// Gets characters from intrface and sends to USART
void SerialComponent::read(uint8_t ch) {
    if(ch == '\n' || ch == '\r') {
        inbuffpos = 0;
        scrolled = FALSE;
    } else {
        if (inbuffpos >= inbuffsize-1) {
            char *d = inbuffer, *s = d+1;
            while ((*d++ = *s++));
            inbuffpos--;
            scrolled = TRUE;
        }
        inbuffer[inbuffpos++] = ch;
    }
    inbuffer[inbuffpos] = '\0';
    if(scrolled) {
        wattron(this->input, A_REVERSE);
        mvwprintw(this->input, 0, 0,">");
        wattroff(this->input, A_REVERSE);
        mvwprintw(this->input, 0, 1, "%s", inbuffer + 1);
    } else {
        mvwprintw(this->input, 0, 0, "%s", inbuffer);
    }
    wclrtoeol(this->input);
    wattron(this->input, A_REVERSE | A_UNDERLINE);
    mvwprintw(this->input, 0, inbuffpos," ");
    wattroff(this->input, A_REVERSE | A_UNDERLINE);
    wrefresh(this->input);
    this->cpu->serial_receive((uint8_t) ch);
}

void SerialComponent::draw() {
    int width, height;
    getmaxyx(this->win, height, width);

    wattron(this->win, COLOR_PAIR(STANDARD_PAIR));

    wborder(this->win, 0, 0, 0, 0, 0, 0, 0, 0);

    mvwprintw(this->win, 0, 1, "[Serial]");

    wattroff(this->win, COLOR_PAIR(STANDARD_PAIR));

    height -= 2;

    wclrtoeol(this->input);

    wattron(this->input, A_REVERSE | A_UNDERLINE);
    mvwprintw(this->input, 0, inbuffpos," ");
    wattroff(this->input, A_REVERSE | A_UNDERLINE);

    mvwprintw(this->output, 0, 0, this->cpu->outputData);
}

void SerialComponent::refresh() {
    Component::refresh();
    wrefresh(this->input);
    wrefresh(this->output);
}

SerialComponent::~SerialComponent() {
    if (inbuffer) delete(inbuffer);
}

