#include "avr.h"
#include <cstring>
#include <fstream>
#include <string.h>
#include "slogpp.h"

#define OP_TODO() pc += op->get_size(); SLog::warn("%s NOT IMPLEMENTED", op->get_mnemonic());

#ifdef WEB_BUILD

#include <emscripten/bind.h>
using namespace emscripten;

EMSCRIPTEN_BINDINGS(avr) {
    emscripten::class_<AVR>("AVR")
        .constructor<>()
        .function("step", &AVR::step_js)
        .function("dump", &AVR::dump)
        // .function("get_data_ptr", &AVR::get_data_ptr, emscripten::allow_raw_pointers()) // Evitato perché complicato da gestire
        // .function("get_pgm_ptr", &AVR::get_pgm_ptr, emscripten::allow_raw_pointers()) // Evitato perché complicato da gestire
        .function("get_pc", &AVR::get_pc)
        .function("set_pc", &AVR::set_pc)
        .function("get_sp", &AVR::get_sp)
        .function("watch_sp", &AVR::watch_sp)
        .function("set_sp", &AVR::set_sp)
        .function("get_status", &AVR::get_status)
        .function("set_status", &AVR::set_status)
        .function("reset", &AVR::reset)
        .function("clear_hl", &AVR::clear_hl)
        .function("get_hl_mem_addr", &AVR::get_hl_mem_addr)
        .function("get_hl_wr_addr", &AVR::get_hl_wr_addr)
        .function("get_hl_rd_addr", &AVR::get_hl_rd_addr)
        .function("get_dreg_addr", &AVR::get_dreg_addr)
        .function("get_dregop", &AVR::get_dregop)
        .function("get_hl_status", &AVR::get_hl_status)
        .function("get_hl_sp", &AVR::get_hl_sp)
        // Aggiungere le funzioni di interfaccia perla USART
        .function("serial_receive", &AVR::serial_receive)
        .function("setBufferSize", &AVR::setBufferSize)
        .function("getSerialCharsAvailable", &AVR::getSerialCharsAvailable)
        .function("getSerialTransmittedCharAt", &AVR::getSerialTransmittedCharAt)
        .function("serialConfigurationOk", &AVR::serialConfigurationOk)
        // Funzioni per interfaccia Javascript
        .function("getDataByte", &AVR::getDataByte)
        .function("setDataByte", &AVR::setDataByte)
        .function("getCodeWord", &AVR::getCodeWord)
        .function("getFlashByte", &AVR::getFlashByte)
        .function("getInstructionAtAsmLen", &AVR::getInstructionAtAsmLen)
        .function("getInstructionAtAsmChar", &AVR::getInstructionAtAsmChar)
        .function("getInstructionAtSize", &AVR::getInstructionAtSize)
        .function("clearCode", &AVR::clearCode)
        .function("setCodeByte", &AVR::setCodeByte);
}

void AVR::step_js() {
    this->step();       // Avoid returning the 'opcode' object, that is difficult to use in javascrpt
}

int AVR::getSerialCharsAvailable() {
    return strlen(this->outputData);
}

uint8_t AVR::getSerialTransmittedCharAt(int offset) {
    return this->outputData[offset];
}

uint8_t AVR::getDataByte(uint16_t address) {
    return this->data[address];
}

void AVR::setDataByte(uint16_t address, uint8_t data) {
    this->data[address] = data;
}

uint16_t AVR::getCodeWord(uint16_t address) {
    return this->program[address];
}

uint8_t AVR::getFlashByte(uint16_t address) {
    return this->eeprom[address];
}

int AVR::getInstructionAtAsmLen(uint16_t address) {
    Operation op(program[address]);
    strcpy(javasriptBuf, op.to_string(this->program, address).c_str());
    return  strlen(javasriptBuf);
}
char AVR::getInstructionAtAsmChar(int index) {
    return this->javasriptBuf[index];
}

int AVR::getInstructionAtSize(uint16_t address) {
    Operation op(program[address]);
    return op.get_size();
}

void AVR::clearCode() {
    memset(program, 0, sizeof(program));
}

void AVR::setCodeByte(uint16_t address, uint8_t byte) {
    ((unsigned char *)program)[address] = byte;
}

#endif

AVR::AVR() {
    memset(program, 0, sizeof(program));
    memset(data, 0, sizeof(data));
    memset(eeprom, 0, sizeof(eeprom));
    status = 0;
    pc = 0;
    set_sp(DATA_SIZE - 1);

    clear_hl();

    // Serial interface
    maxdata = MAXDATA;
    outputData[0] = '\0';
}

int AVR::get_hex(char **s, int len) {
    int res = 0;
    while (len--) {
        char c = *(*s)++;
        res *= 16;
        res += (c >= '0' && c <= '9') ? c - '0' : ((c >= 'A' &&  c <= 'F') ? c - 'A' + 10 : ((c >= 'a' && c <= 'f') ? c - 'a' + 10 : 0));
    }
    return res;
}

void AVR::serial_receive(uint8_t ch) {
    this->data[UDR0] = ch;           // Mette il dato nel registro
    this->data[UCSR0A] |= RXC0_MASK; // E setta il flag di ricevuto
}

int AVR::scrollOutput(int len) {
    int i = strlen(this->outputData);
    this->outputData[this->maxdata] = 0;    // To avoid segmentation fault during sprintf
    if (i + len >= maxdata) {
        char *d =  this->outputData, *s =  this->outputData + len;
        while((*d++ = *s++)); // strcpy espansa
        i -= len;
    }
    return i;
}

void AVR::setBufferSize(int size) {
    this->maxdata = size;
    if ( this->maxdata >= MAXDATA)  this->maxdata = MAXDATA - 1;
}

// Prints characters from USART
void AVR::serialWrite(char c) {
    if ((c >= 32 && c < 127) || c == '\r' || c == '\n') {    // Stampabile
        int i = this->scrollOutput(1);
        this->outputData[i++] = c;
        this->outputData[i] = '\0';
    } else { // non stampabile
        int i = this->scrollOutput(4);
        sprintf( this->outputData + i,"[%02x]", c & 0xff);  // treat as unsigned
    }
}

bool AVR::serialConfigurationOk() {
    return true;
}
// End USART management routines

#define MAXHEXLINE 64

size_t AVR::load_program(const char *filename) {

    std::ifstream file(filename, std::ios::binary);

    if (!file.is_open()) {
        return 0;
    }

    /* SALVI 30/03/2024
     * Aggiunta lettura di file HEX Intel (formato usato da Arduino/avrdude)
     */
    std::streampos file_size;
    int l = strlen(filename);
    if (l > 4 && !strcmp(filename + l - 4, ".hex")) {
        // File hex Intel
        char line[MAXHEXLINE];
        file_size = 0;
        while(file.getline(line, MAXHEXLINE)) {
            char *l = line;
            cout << l << endl;
            if(*l++ == ':') {    // Data
                int len = get_hex(&l, 2);
                uint16_t addr = get_hex(&l, 4);
                if (get_hex(&l, 2) == 1) {  // Record Fine dati
                    break;
                }
                for (int i = 0;i < len; i++) {
                    if (addr < PROG_SIZE) {
                        ((char *)program)[addr++] = get_hex(&l, 2);
                        if (addr > file_size) {
                            file_size = addr;
                        }
                    }
                }
                // Non verifico il checksum - mi fido
           }
        }
    } else {
        file.seekg(0, std::ios::end);
        file_size = file.tellg();
        file.seekg(0, std::ios::beg);

        if (file_size > PROG_SIZE) {
            return 0;
        }

        file.read((char*)program, file_size);

        if (!file) {
            return 0;
        }
    }

    file.close();

    return file_size;
}

Operation AVR::step() {
    clear_hl();

    SLog::debug("0x%02x ", pc*2);
    Operation op(program[pc]);
    SLog::info("%s", op.to_string(program, pc).c_str());
    exeop(&op);

    return op;
}

void AVR::step_n(int n) {
    for(int i = 0; i < n; i++) {
        step();
    }
}

void AVR::exeop(Operation *op) {
    switch(op->get_type()) {
        case Operation::ADD: {
            uint8_t rr, rd, rd_pre;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("ADD R%d, R%d", rd, rr);
            set_data_word(rd, (rd_pre = get_data_word(rd)) + get_data_word(rr));
            pc+=op->get_size();

            set_flag(HALF_CARRY, ((get_data_word(rr) & 0x8 || rd_pre & 0x8) && !(get_data_word(rd) & 0x8)) || (rd_pre & 0x8 && get_data_word(rr) & 0x8));
            set_flag(OVER , (0x80 & rd_pre & get_data_word(rr) & ~get_data_word(rd)) || (0x80 & ~rd_pre & ~get_data_word(rr) & get_data_word(rd)));
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
            set_flag(CARRY, get_data_word(rd) < get_data_word(rr));
        } break;

        case Operation::ADC: {
            uint8_t rr, rd, rd_pre;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("ADC R%d, R%d", rd, rr);
            set_data_word(
                    rd,
                    (rd_pre = get_data_word(rd)) + get_data_word(rr) + get_flag(CARRY)
            );
            pc+=op->get_size();

            set_flag(HALF_CARRY, ((get_data_word(rr) & 0x8 || rd_pre & 0x8) && !(get_data_word(rd) & 0x8)) || (rd_pre & 0x8 && get_data_word(rr) & 0x8));
            set_flag(OVER , (0x80 & rd_pre & get_data_word(rr) & ~get_data_word(rd)) || (0x80 & ~rd_pre & ~get_data_word(rr) & get_data_word(rd)));
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
            set_flag(CARRY, get_data_word(rd) < get_data_word(rr));
        } break;

        case Operation::ADIW: {
            uint8_t rd, imm, dp_pre;
            op->get_reg_imm_word_addr(&rd, &imm);
            SLog::debug("ADIW R%d:%d, 0x%02x", rd+1, rd, imm);
            set_data_pair(rd, (dp_pre = get_data_pair(rd)) + imm);
            pc+=op->get_size();

            set_flag(OVER, !(dp_pre & 0x8000) && (get_data_pair(rd) & 0x8000));
            set_flag(NEGATIVE, get_data_pair(rd) & 0b1000000000000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_pair(rd) == 0);
            set_flag(CARRY, (dp_pre & 0x8000) && !(get_data_pair(rd) & 0x8000));
        } break;

        case Operation::SUB: {
            uint8_t rr, rd, rd_pre;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("SUB R%d, R%d", rd, rr);

            set_flag(CARRY, get_data_word(rd) < get_data_word(rr));

            set_data_word(rd, (rd_pre = get_data_word(rd)) - get_data_word(rr));
            pc+=op->get_size();

            set_flag(HALF_CARRY, ((get_data_word(rr) & 0x8 || !(rd_pre & 0x8)) && (get_data_word(rd) & 0x8)) || (!(rd_pre & 0x8) && (get_data_word(rr) & 0x8)));
            set_flag(OVER , (0x80 & rd_pre & ~get_data_word(rr) & ~get_data_word(rd)) || (0x80 & ~rd_pre & get_data_word(rr) & get_data_word(rd)));
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::SUBI: {
            uint8_t rd, imm, rd_pre;
            op->get_reg_imm_addr(&rd, &imm);
            SLog::debug("SUBI R%d, 0x%02x", rd, imm);

            set_flag(CARRY, get_data_word(rd) < imm);

            set_data_word(rd, (rd_pre = get_data_word(rd)) - imm);
            pc+=op->get_size();

            set_flag(HALF_CARRY, ((imm & 0x8 || !(rd_pre & 0x8)) && (get_data_word(rd) & 0x8)) || (!(rd_pre & 0x8) && (imm & 0x8)));
            set_flag(OVER , (0x80 & rd_pre & ~imm & ~get_data_word(rd)) || (0x80 & ~rd_pre & imm & get_data_word(rd)));
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::SBC: {
            uint8_t rr, rd, rd_pre;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("SBC R%d, R%d", rd, rr);

            set_flag(
                CARRY,
                get_data_word(rd) < get_data_word(rr)+get_flag(CARRY)
            );

            set_data_word(
                rd, (rd_pre = get_data_word(rd)) - get_data_word(rr) - get_flag(CARRY)
            );
            pc+=op->get_size();

            set_flag(HALF_CARRY, ((get_data_word(rr) & 0x8 || !(rd_pre & 0x8)) && (get_data_word(rd) & 0x8)) || (!(rd_pre & 0x8) && (get_data_word(rr) & 0x8)));
            set_flag(OVER , (0x80 & rd_pre & ~get_data_word(rr) & ~get_data_word(rd)) || (0x80 & ~rd_pre & get_data_word(rr) & get_data_word(rd)));
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::SBCI: {
            uint8_t rd, imm, rd_pre;
            op->get_reg_imm_addr(&rd, &imm);
            SLog::debug("SBCI R%d, 0x%02x", rd, imm);

            set_flag(CARRY, get_data_word(rd) < imm+get_flag(CARRY));

            set_data_word(rd, (rd_pre = get_data_word(rd)) - imm - get_flag(CARRY));
            pc+=op->get_size();

            set_flag(HALF_CARRY, ((imm & 0x8 || !(rd_pre & 0x8)) && (get_data_word(rd) & 0x8)) || (!(rd_pre & 0x8) && (imm & 0x8)));
            set_flag(OVER , (0x80 & rd_pre & ~imm & ~get_data_word(rd)) || (0x80 & ~rd_pre & imm & get_data_word(rd)));
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::SBIW: {
            uint8_t rd, imm, dp_pre;
            op->get_reg_imm_word_addr(&rd, &imm);
            SLog::debug("SBIW R%d:%d, 0x%02x", rd+1, rd, imm);
            set_data_pair(rd, (dp_pre = get_data_pair(rd)) - imm);
            pc+=op->get_size();

            set_flag(OVER, (dp_pre & 0x8000) && !(get_data_pair(rd) & 0x8000));
            set_flag(NEGATIVE, get_data_pair(rd) & 0b1000000000000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_pair(rd) == 0);
            set_flag(CARRY, !(dp_pre & 0x8000) && (get_data_pair(rd) & 0x8000));
        } break;

        case Operation::AND: {
            uint8_t rr, rd;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("AND R%d, R%d", rd, rr);
            set_data_word(rd, get_data_word(rd) & get_data_word(rr));
            pc+=op->get_size();

            set_flag(OVER, 0);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE));
            set_flag(ZERO, get_data_word(rd) == 0);

        } break;

        case Operation::ANDI: {
            uint8_t rd, imm;
            op->get_reg_imm_addr(&rd, &imm);
            SLog::debug("ANDI R%d, 0x%02x", rd, imm);
            set_data_word(rd, get_data_word(rd) & imm);
            pc+=op->get_size();


            set_flag(OVER, 0);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::OR: {
            uint8_t rr, rd;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("OR R%d, R%d", rd, rr);
            set_data_word(rd, get_data_word(rd) | get_data_word(rr));
            pc+=op->get_size();


            set_flag(OVER, 0);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::ORI: {
            uint8_t rd, imm;
            op->get_reg_imm_addr(&rd, &imm);
            SLog::debug("ORI R%d, 0x%02x", rd, imm);
            set_data_word(rd, get_data_word(rd) & imm);
            pc+=op->get_size();


            set_flag(OVER, 0);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::EOR: {
            uint8_t rr, rd;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("EOR R%d, R%d", rd, rr);
            set_data_word(rd, get_data_word(rd) ^ get_data_word(rr));
            pc+=op->get_size();

            set_flag(OVER, 0);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::COM: {
            uint8_t rd;
            op->get_reg_addr_5(&rd);
            set_data_word(rd, 0xff-get_data_word(rd));
            pc+=op->get_size();
            SLog::debug("COM R%d", rd);


            set_flag(OVER, 0);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE));
            set_flag(ZERO, get_data_word(rd) == 0);
            set_flag(CARRY, 1);
        } break;

        case Operation::NEG: {
            uint8_t rd, rd_pre;
            op->get_reg_addr_5(&rd);
            set_data_word(rd, -(rd_pre = get_data_word(rd)));
            pc+=op->get_size();
            SLog::debug("NEG R%d", rd);

            set_flag(HALF_CARRY, rd_pre & 0x8 || get_data_word(rd) & 0x8);
            set_flag(OVER, rd_pre == 0x80);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
            set_flag(CARRY, get_data_word(rd) != 0);
        } break;

        case Operation::INC: {
            uint8_t rd;
            op->get_reg_addr_5(&rd);
            SLog::debug("INC R%d", rd);
            set_data_word(rd, get_data_word(rd) + 1);
            pc+=op->get_size();

            set_flag(OVER, get_data_word(rd) == 0x80);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::DEC: {
            uint8_t rd;
            op->get_reg_addr_5(&rd);
            SLog::debug("DEC R%d", rd);
            set_data_word(rd, get_data_word(rd) - 1);
            pc+=op->get_size();

            set_flag(OVER, get_data_word(rd) == 0x7f);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, get_data_word(rd) == 0);
        } break;

        case Operation::SER: {
            uint8_t rd;
            op->get_reg_addr_4(&rd);
            SLog::debug("SER R%d", rd);
            set_data_word(rd, 0xff);
            pc+=op->get_size();
        } break;

        case Operation::MUL: {
            uint8_t rr, rd;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("MUL R%d, R%d", rd, rr);
            set_data_pair(0, uint16_t(get_data_word(rd)) * uint16_t(get_data_word(rr)));
            pc+=op->get_size();

            set_flag(CARRY, get_data_pair(0) & 0x8000);
            set_flag(ZERO, get_data_pair(0) == 0);
        } break;

        case Operation::MULS: {
            uint8_t rr, rd;
            op->get_two_reg_addr_4(&rr, &rd);
            SLog::debug("MULS R%d, R%d", rd, rr);
            set_data_pair(0, int16_t(int8_t(get_data_word(rd))) * int16_t(int8_t(get_data_word(rr))));
            pc+=op->get_size();

            set_flag(CARRY, get_data_pair(0) & 0x8000);
            set_flag(ZERO, get_data_pair(0) == 0);
        } break;

        case Operation::MULSU: {
            uint8_t rr, rd;
            op->get_two_reg_addr_3(&rr, &rd);
            SLog::debug("MULSU R%d, R%d", rd, rr);
            set_data_pair(0, int16_t(int8_t(get_data_word(rd))) * uint16_t(get_data_word(rr)));
            pc+=op->get_size();

            set_flag(CARRY, get_data_pair(0) & 0x8000);
            set_flag(ZERO, get_data_pair(0) == 0);
        } break;

        case Operation::FMUL: {
            uint8_t rr, rd;
            op->get_two_reg_addr_3(&rr, &rd);
            SLog::debug("FMUL R%d, R%d", rd, rr);
            set_data_pair(0, (uint16_t(get_data_word(rd)) * uint16_t(get_data_word(rr))) << 1);
            pc+=op->get_size();

            set_flag(CARRY, get_data_pair(0) & 0x8000);
            set_flag(ZERO, get_data_pair(0) == 0);
        } break;

        case Operation::FMULS: {
            uint8_t rr, rd;
            op->get_two_reg_addr_3(&rr, &rd);
            SLog::debug("FMULS R%d, R%d", rd, rr);
            set_data_pair(0, (int16_t(int8_t(get_data_word(rd))) * int16_t(int8_t(get_data_word(rr)))) << 1);
            pc+=op->get_size();

            set_flag(CARRY, get_data_pair(0) & 0x8000);
            set_flag(ZERO, get_data_pair(0) == 0);
        } break;

        case Operation::FMULSU: {
            uint8_t rr, rd;
            op->get_two_reg_addr_3(&rr, &rd);
            SLog::debug("FMULSU R%d, R%d", rd, rr);
            set_data_pair(0, (int16_t(int8_t(get_data_word(rd))) * uint16_t(get_data_word(rr))) << 1);
            pc+=op->get_size();

            set_flag(CARRY, get_data_pair(0) & 0x8000);
            set_flag(ZERO, get_data_pair(0) == 0);
        } break;

        case Operation::RJMP: {
            int16_t reladdr;
            op->get_rel_prog_addr(&reladdr);
            pc += reladdr + 1;
            SLog::debug("RJMP %+d", reladdr + 1);
        } break;

        case Operation::IJMP: {
            pc = get_Z();
            SLog::debug("IJMP 0x%04x", get_Z());
        } break;

        case Operation::JMP: {
            uint32_t absaddr;
            op->get_abs_prog_addr(&absaddr, program[pc+1]);
            SLog::debug("JMP 0x%06x", absaddr * 2);
            pc = absaddr;
        } break;

        case Operation::RCALL: {
            int16_t reladdr;
            op->get_rel_prog_addr(&reladdr);
            uint16_t s_p = get_sp();
            set_sp(s_p-2);
            set_data_pair(s_p-1, pc+op->get_size());
            pc += reladdr + 1;
            SLog::debug("RCALL %+d", reladdr + 1);
        } break;

        case Operation::ICALL: {
            uint16_t s_p = get_sp();
            set_sp(s_p-2);
            set_data_pair(s_p-1, pc+op->get_size());
            pc = get_Z();
            SLog::debug("ICALL 0x%06x", get_Z());
        } break;

        case Operation::CALL: {
            uint32_t absaddr;
            op->get_abs_prog_addr(&absaddr, program[pc+1]);
            uint16_t s_p = get_sp();
            set_sp(s_p-2);
            set_data_pair(s_p-1, pc+op->get_size());

            pc = absaddr;
            SLog::debug("CALL 0x%06x", absaddr * 2);

        } break;

        case Operation::RET: {
            uint16_t s_p = get_sp();
            set_sp(s_p+2);
            pc = get_data_pair(s_p+1);
            SLog::debug("RET 0x%x", pc*2);

        } break;

        case Operation::RETI: {
            uint16_t s_p = get_sp();
            set_sp(s_p+2);
            pc = get_data_pair(s_p+1);
            SLog::debug("RET 0x%x", pc*2);

            set_flag(INTERRUPT, 1);
        } break;

        case Operation::CPSE: {
            uint8_t rr, rd;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("CPSE R%d, R%d", rd, rr);
            if(get_data_word(rr) == get_data_word(rd)) {
                Operation nextop(program[pc+op->get_size()]);
                pc+=nextop.get_size()+op->get_size();
                break;
            }
            pc+=op->get_size();
        } break;

        case Operation::CP: {
            uint8_t rr, rd;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("CP R%d, R%d", rd, rr);
            uint8_t res = get_data_word(rd) - get_data_word(rr);

            pc+=op->get_size();

            set_flag(HALF_CARRY, ((get_data_word(rr) & 0x8 || !(get_data_word(rd) & 0x8)) && (res & 0x8)) || (!(get_data_word(rd) & 0x8) && (get_data_word(rr) & 0x8)));
            set_flag(OVER , (0x80 & get_data_word(rd) & ~get_data_word(rr) & ~res) || (0x80 & ~get_data_word(rd) & get_data_word(rr) & res));
            set_flag(NEGATIVE, res & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, res == 0);
            set_flag(CARRY, get_data_word(rd) < get_data_word(rr));
        } break;

        case Operation::CPC: {
            uint8_t rr, rd;
            op->get_two_reg_addr_5(&rr, &rd);
            SLog::debug("CPC R%d, R%d", rd, rr);
            uint8_t res = get_data_word(rd) - get_data_word(rr) - get_flag(CARRY);

            pc+=op->get_size();

            set_flag(HALF_CARRY, ((get_data_word(rr) & 0x8 || !(get_data_word(rd) & 0x8)) && (res & 0x8)) || (!(get_data_word(rd) & 0x8) && (get_data_word(rr) & 0x8)));
            set_flag(OVER , (0x80 & get_data_word(rd) & ~get_data_word(rr) & ~res) || (0x80 & ~get_data_word(rd) & get_data_word(rr) & res));
            set_flag(NEGATIVE, res & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(ZERO, res == 0);
            set_flag(CARRY, get_data_word(rd) < (get_data_word(rr)+get_flag(CARRY)));
        } break;

        case Operation::CPI: {
            uint8_t rd, imm;
            op->get_reg_imm_addr(&rd, &imm);
            SLog::debug("CPI R%d, 0x%02x", rd, imm);
            uint8_t res = get_data_word(rd) - imm;

            pc+=op->get_size();

            set_flag(HALF_CARRY, ((imm & 0x8 || !(get_data_word(rd) & 0x8)) && (res & 0x8)) || (!(get_data_word(rd) & 0x8) && (imm & 0x8)));
            set_flag(OVER , (0x80 & get_data_word(rd) & ~imm & ~res) || (0x80 & ~get_data_word(rd) & imm & res));
            set_flag(NEGATIVE, res & 0b10000000);
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
            set_flag(NEGATIVE, res & 0b10000000);
            set_flag(ZERO, res == 0);
            set_flag(CARRY, get_data_word(rd) < imm);
        } break;

        case Operation::SBRC: {
            uint8_t rr, bit;
            op->get_reg_bit_addr(&rr, &bit);
            SLog::debug("SBRC R%d(%d)", rr, bit);
            if((get_data_word(rr) & (1<<bit)) == 0) {
                Operation nextop(program[pc+op->get_size()]);
                pc+=nextop.get_size()+op->get_size();
                break;
            }
            pc+=op->get_size();
        } break;

        case Operation::SBRS: {
            uint8_t rr, bit;
            op->get_reg_bit_addr(&rr, &bit);
            SLog::debug("SBRC R%d(%d)", rr, bit);
            if(get_data_word(rr) & (1<<bit)) {
                Operation nextop(program[pc+op->get_size()]);
                pc+=nextop.get_size()+op->get_size();
                break;
            }
            pc+=op->get_size();
        } break;

        case Operation::SBIC: {
            uint8_t ior, bit;
            op->get_ior_bit_addr(&ior, &bit);
            SLog::debug("SBRC IO%d(%d)", ior-IO_REG_OFFSET, bit);
            if(!(get_data_word(ior) & (1<<bit))) {
                Operation nextop(program[pc+op->get_size()]);
                pc+=nextop.get_size()+op->get_size();
                break;
            }
            pc+=op->get_size();
        } break;

        case Operation::SBIS: {
            uint8_t ior, bit;
            op->get_ior_bit_addr(&ior, &bit);
            SLog::debug("SBRC IO%d(%d)", ior-IO_REG_OFFSET, bit);
            if(get_data_word(ior) & (1<<bit)) {
                Operation nextop(program[pc+op->get_size()]);
                pc+=nextop.get_size()+op->get_size();
                break;
            }
            pc+=op->get_size();
        } break;

        case Operation::BRBC: {
            int8_t reladdr;
            uint8_t bit;
            op->get_rel_prog_bit_addr(&reladdr, &bit);
            SLog::debug("BRBC %d, %+d", bit, reladdr+1);
            if(get_flag(Flag(1<<bit)) == 0) {
                pc += reladdr + 1;
                break;
            }
            pc+=op->get_size();
        } break;

        case Operation::BRBS: {
            int8_t reladdr;
            uint8_t bit;
            op->get_rel_prog_bit_addr(&reladdr, &bit);
            SLog::debug("BRBS %d, %+d", bit, reladdr+1);
            if(get_flag(Flag(1<<bit))) {
                pc += reladdr + 1;
                break;
            }
            pc+=op->get_size();
        } break;

        case Operation::SBI: {
            uint8_t ior, bit;
            op->get_ior_bit_addr(&ior, &bit);
            SLog::debug("SBI IO%d(%d)", ior-IO_REG_OFFSET, bit);
            set_data_word(ior, get_data_word(ior) | (1<<bit));
            pc+=op->get_size();
        } break;

        case Operation::CBI: {
            uint8_t ior, bit;
            op->get_ior_bit_addr(&ior, &bit);
            SLog::debug("CBI IO%d(%d)", ior-IO_REG_OFFSET, bit);
            set_data_word(ior, get_data_word(ior) & ~(1<<bit));
            pc+=op->get_size();
        } break;

        case Operation::LSR: {
            uint8_t rd, rd_pre;
            op->get_reg_addr_5(&rd);
            SLog::debug("LSR R%d", rd);
            set_data_word(rd, (rd_pre = get_data_word(rd)) >> 1);
            pc+=op->get_size();

            set_flag(CARRY, rd_pre & 0x1);
            set_flag(ZERO, get_data_word(rd) == 0);
            set_flag(NEGATIVE, 0);
            set_flag(OVER, get_flag(CARRY) ^ get_flag(NEGATIVE));
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
        } break;

        case Operation::ROR: {
            uint8_t rd, rd_pre;
            op->get_reg_addr_5(&rd);
            SLog::debug("ROR R%d", rd);
            set_data_word(rd, ((rd_pre = get_data_word(rd)) >> 1) | (get_flag(CARRY)<<7));
            pc+=op->get_size();

            set_flag(CARRY, rd_pre & 0x1);
            set_flag(ZERO, get_data_word(rd) == 0);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(OVER, get_flag(CARRY) ^ get_flag(NEGATIVE));
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
        } break;

        case Operation::ASR: {
            uint8_t rd, rd_pre;
            op->get_reg_addr_5(&rd);
            SLog::debug("ASR R%d", rd);
            rd_pre = get_data_word(rd);
            set_data_word(rd, (rd_pre >> 1) | (rd_pre & 0b10000000));
            pc+=op->get_size();

            set_flag(CARRY, rd_pre & 0x1);
            set_flag(ZERO, get_data_word(rd) == 0);
            set_flag(NEGATIVE, get_data_word(rd) & 0b10000000);
            set_flag(OVER, get_flag(CARRY) ^ get_flag(NEGATIVE));
            set_flag(SIGN, get_flag(NEGATIVE) ^ get_flag(OVER));
        } break;

        case Operation::SWAP: {
            uint8_t rd;
            op->get_reg_addr_5(&rd);
            SLog::debug("SWAP R%d", rd);
            set_data_word(rd, (get_data_word(rd) << 4) | (get_data_word(rd) >> 4));
            pc+=op->get_size();
        } break;

        case Operation::BSET: {
            uint8_t bit;
            op->get_bit_addr(&bit);
            SLog::debug("BSET %d", bit);
            set_flag(Flag(1<<bit), 1);
            pc+=op->get_size();
        } break;

        case Operation::BCLR: {
            uint8_t bit;
            op->get_bit_addr(&bit);
            SLog::debug("BCLR %d", bit);
            set_flag(Flag(1<<bit), 0);
            pc+=op->get_size();
        } break;

        case Operation::BST: {
            uint8_t rd, bit;
            op->get_reg_bit_addr(&rd, &bit);
            SLog::debug("BST R%d(%d)", rd, bit);
            set_flag(TRANSFER, get_data_word(rd) & (1<<bit));
            pc+=op->get_size();
        } break;

        case Operation::BLD: {
            uint8_t rd, bit;
            op->get_reg_bit_addr(&rd, &bit);
            SLog::debug("BLD R%d(%d)", rd, bit);
            set_data_word(rd, (get_data_word(rd) & ~(1<<bit)) | (get_flag(TRANSFER)<<bit));
            pc+=op->get_size();
        } break;

        case Operation::MOV: {
            uint8_t rr, rd;
            op->get_two_reg_addr_5(&rr, &rd);
            set_data_word(rd, get_data_word(rr));
            pc+=op->get_size();
            SLog::debug("MOV R%d, R%d", rd, rr);
        } break;

        case Operation::MOVW: {
            uint8_t rr, rd;
            op->get_two_reg_pair_addr(&rr, &rd);
            set_data_pair(rd, get_data_pair(rr));
            pc+=op->get_size();
            SLog::debug("MOVW R%d:R%d, R%d:R%d", rd+1, rd, rr+1, rr);
        } break;

        case Operation::LDI: {
            uint8_t rd, imm;
            op->get_reg_imm_addr(&rd, &imm);
            SLog::debug("LDI R%d, 0x%02x", rd, imm);
            set_data_word(rd, imm);
            pc+=op->get_size();
        } break;

        case Operation::ST_X: {
            uint8_t rr, inc;
            op->get_reg_inc_addr(&rr, &inc);
            switch (inc){
                case 0b00:
                    SLog::debug("ST X, R%d", rr);
                    set_data_word(get_X(), get_data_word(rr));
                    break;
                case 0b01:
                    SLog::debug("ST X+, R%d", rr);
                    set_data_word(get_X(), get_data_word(rr));
                    set_X(get_X()+1);
                    break;
                case 0b10:
                    SLog::debug("ST -X, R%d", rr);
                    set_X(get_X()-1);
                    set_data_word(get_X(), get_data_word(rr));
                    break;
            }
            pc+=op->get_size();
        } break;

        case Operation::ST_Y: {
            uint8_t rr, inc;
            op->get_reg_inc_addr(&rr, &inc);
            switch (inc){
                case 0b00:
                    SLog::debug("ST Y, R%d", rr);
                    set_data_word(get_Y(), get_data_word(rr));
                    break;
                case 0b01:
                    SLog::debug("ST Y+, R%d", rr);
                    set_data_word(get_Y(), get_data_word(rr));
                    set_Y(get_Y()+1);
                    break;
                case 0b10:
                    SLog::debug("ST -Y, R%d", rr);
                    set_Y(get_Y()-1);
                    set_data_word(get_Y(), get_data_word(rr));
                    break;
            }
            pc+=op->get_size();
        } break;

        case Operation::STD_Y: {
            uint8_t rr, q;
            op->get_reg_offs_addr(&rr, &q);
            SLog::debug("STD Y+%d, R%d", q, rr);
            set_data_word(get_Y()+q, get_data_word(rr));
            pc+=op->get_size();
        } break;

        case Operation::ST_Z: {
            uint8_t rr, inc;
            op->get_reg_inc_addr(&rr, &inc);
            switch (inc){
                case 0b00:
                    SLog::debug("ST Z, R%d", rr);
                    set_data_word(get_Z(), get_data_word(rr));
                    break;
                case 0b01:
                    SLog::debug("ST Z+, R%d", rr);
                    set_data_word(get_Z(), get_data_word(rr));
                    set_Z(get_Z()+1);
                    break;
                case 0b10:
                    SLog::debug("ST -Z, R%d", rr);
                    set_Z(get_Z()-1);
                    set_data_word(get_Z(), get_data_word(rr));
                    break;
            }
            pc+=op->get_size();
        } break;

        case Operation::STD_Z: {
            uint8_t rr, q;
            op->get_reg_offs_addr(&rr, &q);
            SLog::debug("STD Z+%d, R%d", q, rr);
            set_data_word(get_Z()+q, get_data_word(rr));
            pc+=op->get_size();
        } break;

        case Operation::STS: {
            uint16_t addr;
            uint8_t rd;
            op->get_reg_abs_addr(&rd, &addr, program[pc+1]);
            SLog::debug("STS 0x%04x,R%d", addr, rd);
            set_data_word(addr, get_data_word(rd));
            pc+=op->get_size();
        } break;

        case Operation::LPM_R0: {
            SLog::debug("LPM R0, Z");
            set_data_word(0, get_data_word(get_Z()));
            pc+=op->get_size();
        } break;

        case Operation::LPM: {
            uint8_t rd, inc;
            op->get_reg_inc_addr(&rd, &inc);
            set_data_word(rd, get_data_word(get_Z()));
            pc+=op->get_size();
            if(inc == 0b01) {
                set_Z(get_Z()+1);
                SLog::debug("LPM R%d, Z+", rd);
                break;
            }
            SLog::debug("LPM R%d, Z", rd);
        } break;

        case Operation::LD_X: {
            uint8_t rd, inc;
            op->get_reg_inc_addr(&rd, &inc);
            switch (inc){
                case 0b00:
                    SLog::debug("LD R%d, X", rd);
                    set_data_word(rd, get_data_word(get_X()));
                    break;
                case 0b01:
                    SLog::debug("LD R%d, X+", rd);
                    set_data_word(rd, get_data_word(get_X()));
                    set_X(get_X()+1);
                    break;
                case 0b10:
                    SLog::debug("LD R%d, -X", rd);
                    set_X(get_X()-1);
                    set_data_word(rd, get_data_word(get_X()));
                    break;
            }
            pc+=op->get_size();
        } break;

        case Operation::LD_Y: {
            uint8_t rd, inc;
            op->get_reg_inc_addr(&rd, &inc);
            switch (inc){
                case 0b00:
                    SLog::debug("LD R%d, Y", rd);
                    set_data_word(rd, get_data_word(get_Y()));
                    break;
                case 0b01:
                    SLog::debug("LD R%d, Y+", rd);
                    set_data_word(rd, get_data_word(get_Y()));
                    set_Y(get_Y()+1);
                    break;
                case 0b10:
                    SLog::debug("LD R%d, -Y", rd);
                    set_Y(get_Y()-1);
                    set_data_word(rd, get_data_word(get_Y()));
                    break;
            }
            pc+=op->get_size();
        } break;

        case Operation::LDD_Y: {
            uint8_t rd, q;
            op->get_reg_offs_addr(&rd, &q);
            SLog::debug("LDD R%d, Y+%d", rd, q);
            set_data_word(rd, get_data_word(get_Y()+q));
            pc+=op->get_size();
        } break;

        case Operation::LD_Z: {
            uint8_t rd, inc;
            op->get_reg_inc_addr(&rd, &inc);
            switch (inc){
                case 0b00:
                    SLog::debug("LD R%d, Z", rd);
                    set_data_word(rd, get_data_word(get_Z()));
                    break;
                case 0b01:
                    SLog::debug("LD R%d, Z+", rd);
                    set_data_word(rd, get_data_word(get_Z()));
                    set_Z(get_Z()+1);
                    break;
                case 0b10:
                    SLog::debug("LD R%d, -Z", rd);
                    set_Z(get_Z()-1);
                    set_data_word(rd, get_data_word(get_Z()));
                    break;
            }
            pc+=op->get_size();
        } break;

        case Operation::LDD_Z: {
            uint8_t rd, q;
            op->get_reg_offs_addr(&rd, &q);
            SLog::debug("LDD R%d, Z+%d", rd, q);
            set_data_word(rd, get_data_word(get_Z()+q));
            pc+=op->get_size();
        } break;

        case Operation::LDS: {
            uint16_t addr;
            uint8_t rd;
            op->get_reg_abs_addr(&rd, &addr, program[pc+1]);
            SLog::debug("LDS R%d, 0x%04x", rd, addr);
            set_data_word(rd, get_data_word(addr));
            pc+=op->get_size();
        } break;

        case Operation::SPM: {
            OP_TODO();
            /**
             * What the actual fuck????
             * statements dreamed up by the utterly deranged
             * Rip riccardo questa mi rifiuto di iniziare
             * a provare a capire cosa dice la documentazione, tantomeno implementarla
            */
        } break;

        case Operation::IN: {
            uint8_t rd, ior;
            op->get_reg_ior_addr(&rd, &ior);
            SLog::debug("IN R%d, IO%d", rd, ior-IO_REG_OFFSET);
            set_data_word(rd, get_data_word(ior));
            pc+=op->get_size();
        } break;

        case Operation::OUT: {
            uint8_t ior, rr;
            op->get_reg_ior_addr(&rr, &ior);
            SLog::debug("OUT IO%d, R%d", ior-IO_REG_OFFSET, rr);
            set_data_word(ior, get_data_word(rr));
            pc+=op->get_size();
        } break;

        case Operation::PUSH: {
            uint8_t rd;
            uint16_t s_p = get_sp();
            op->get_reg_addr_5(&rd);
            set_data_word(s_p, get_data_word(rd));
            set_sp(s_p-1);
            pc+=op->get_size();
            SLog::debug("PUSH R%d", rd);
        } break;

        case Operation::POP: {
            uint8_t rd;
            uint16_t s_p = get_sp();
            op->get_reg_addr_5(&rd);
            set_sp(s_p+1);
            set_data_word(rd, get_data_word(s_p+1));
            pc+=op->get_size();
            SLog::debug("POP R%d", rd);
        } break;

        case Operation::NOP: {
            SLog::debug("NOP");
            pc+=op->get_size();
        } break;

        case Operation::SLEEP: {
            OP_TODO();
        } break;

        case Operation::WDR: {
            OP_TODO();
        } break;

        case Operation::BREAK: {
            SLog::debug("BREAK");
            if(!handle_breakpoint()){
                pc+=op->get_size();
            }
        } break;

        case Operation::NONE: {
            SLog::error("INVALID OPCODE");
            pc+=op->get_size(); // Anyway goes to the next
       } break;
    }
}

void AVR::handle_interrupts() {
    if(!get_flag(INTERRUPT)) return;

}

void AVR::interrupt(Interrupt Vector){
    /**
     * @todo complete implementation
    */

    uint16_t s_p = get_sp();
    set_sp(s_p - 2);
    set_data_pair(s_p - 1, pc);
    pc = Vector;
}

uint8_t AVR::get_data_word(uint16_t addr) {
    this->hl_rd_addr = addr;
    if(addr == UDR0 && this->data[UCSR0B] & RXEN0_MASK) {  // Letto dato dall'USART
        // Spegne RXC0 -> dato letto
        this->data[UCSR0A] &= ~RXC0_MASK;
    } else if(addr == UCSR0A) { // Gestione dei flag in trasmissione
        this->data[UCSR0A] |= TXC0_MASK | UDRE0_MASK; // Pronto a spedire e trasmissione precedente finita
    }
    return this->data[addr];
}

void AVR::set_data_word(uint16_t addr, uint8_t data) {
    if (addr == UDR0) { // Invio dato rtramite USART, non metto nel registro
        if (this->data[UCSR0B] & TXEN0_MASK) {
            this->serialWrite((char) data);
            // Eventualmente gestire i dure bit di  UCSR0A
        }
    } else {
        this->hl_wr_addr = addr;
        this->data[addr] = data;
    }
}

uint16_t AVR::get_data_pair(uint16_t addr) {
    if (this->hl_dreg_addr != addr) {   // If there's anather operation pendong elsewere
        this->dregop = 0;
    }
    this->hl_dreg_addr = addr;
    this->dregop |= RD_OPERATION;
    return this->data[addr] | (this->data[addr+1]<<8);
}

void AVR::set_data_pair(uint16_t addr, uint16_t value) {
    if (this->hl_dreg_addr != addr) {   // If there's anather operation pendong elsewere
        this->dregop = 0;
    }
    this->hl_dreg_addr = addr;
    this->dregop |= WR_OPERATION;
    this->data[addr] = value & 0xff;
    this->data[addr+1] = value >> 8;
}

uint8_t AVR::get_flag(Flag flag) {
    return (status & flag);
}

void AVR::set_flag(Flag flag, uint8_t value) {
    this->hl_status = flag;
    if(value) {
        status |= flag;
    } else {
        status &= ~flag;
    }
}

uint16_t AVR::get_X() {
    return get_data_pair(X_ADDR);
}

uint16_t AVR::get_Y() {
    return get_data_pair(Y_ADDR);
}

uint16_t AVR::get_Z() {
    return get_data_pair(Z_ADDR);
}

void AVR::set_X(uint16_t value) {
    set_data_pair(X_ADDR, value);
}

void AVR::set_Y(uint16_t value) {
    set_data_pair(Y_ADDR, value);
}

void AVR::set_Z(uint16_t value) {
    set_data_pair(Z_ADDR, value);
}

void AVR::dump() {
    printf("PC 0x%06x\tSP 0x%04x\n", pc, get_sp());
    printf("Status %d%d%d%d%d%d%d%d\n",
            (status & 0b10000000) != 0,
            (status & 0b01000000) != 0,
            (status & 0b00100000) != 0,
            (status & 0b00010000) != 0,
            (status & 0b00001000) != 0,
            (status & 0b00000100) != 0,
            (status & 0b00000010) != 0,
            (status & 0b00000001) != 0
    );
    printf("R0 0x%02x\tR1 0x%02x\tR2 0x%02x\tR3 0x%02x\n",
            data[0], data[1], data[2], data[3]);

    printf("X %04x\tY %04x\tZ %04x\n", get_X(), get_Y(), get_Z());
}

uint8_t* AVR::get_data_ptr() {
    return data;
}

uint16_t* AVR::get_pgm_ptr() {
    return program;
}

uint16_t AVR::get_pc() {
    return pc;
}

void AVR::set_pc(uint16_t pc) {
    this->pc = pc % DATA_SIZE;
}

uint16_t AVR::get_sp() {
    return get_data_pair(SP_ADDR);
}

uint16_t AVR::watch_sp() {
    return this->data[SP_ADDR] | (this->data[SP_ADDR+1]<<8);
}

void AVR::set_sp(uint16_t sp) {
    this->hl_sp = true;
    set_data_pair(SP_ADDR, sp % DATA_SIZE);
}

uint8_t AVR::get_status() {
    return status;
}

void AVR::set_status(uint8_t status) {
    hl_status = this->status ^ status;
    this->status = status;
}

void AVR::reset() {
    memset(data, 0, sizeof(data));
    status = 0;
    pc = 0;
    set_sp(DATA_SIZE - 1);

    clear_hl();
}

void AVR::clear_hl() {
    hl_sp = false;
    hl_status = 0;

    hl_wr_addr = NOADDRESS;
    hl_rd_addr = NOADDRESS;
    hl_dreg_addr = NOADDRESS;
    dregop = 0;
}

size_t AVR::get_hl_wr_addr() {
    return hl_wr_addr;
}

size_t AVR::get_hl_rd_addr() {
    return hl_rd_addr;
}

size_t AVR::get_dreg_addr() {
    return hl_dreg_addr;
}

uint8_t AVR::get_dregop() {
    return dregop;
}

bool AVR::get_hl_sp() {
    return hl_sp;
}

/* returns in order:
 *  hl_wr_addr if present
 *  else, hl_rd_addr if present
 *  else, hl_dreg_addr if present
 *  else 0
 */
size_t AVR::get_hl_mem_addr() {
    return (hl_wr_addr < NOADDRESS) ? hl_wr_addr :
        (hl_rd_addr < NOADDRESS) ? hl_rd_addr :
            (hl_dreg_addr < NOADDRESS) ? hl_dreg_addr :
                0;
}

uint8_t AVR::get_hl_status() {
    return hl_status;
}

uint8_t AVR::get_int_flag(Interrupt interrupt){
    switch (interrupt){
    case INT0:
        return (get_data_word(_EIFR) & (1<<_EIFR_INTF0)) != 0;
    case INT1:
        return (get_data_word(_EIFR) & (1<<_EIFR_INTF1)) != 0;
    case PCINT0:
        return (get_data_word(_PCIFR) & (1<<_PCIF0)) != 0;
    case PCINT1:
        return (get_data_word(_PCIFR) & (1<<_PCIF1)) != 0;
    case PCINT2:
        return (get_data_word(_PCIFR) & (1<<_PCIF2)) != 0;
    case WDT:
        return (get_data_word(_WDTCSR) & (1<<_WDTCSR_WDIF)) != 0;
    case TIMER2_COMPA:
        return (get_data_word(_TIFR2) & (1<<_TIFR2_OCF2A)) != 0;
    case TIMER2_COMPB:
        return (get_data_word(_TIFR2) & (1<<_TIFR2_OCF2B)) != 0;
    case TIMER2_OVF:
        return (get_data_word(_TIFR2) & (1<<_TIFR2_TOV2)) != 0;
    case TIMER1_CAPT:
        return (get_data_word(_TIFR1) & (1<<_TIFR1_ICF1)) != 0;
    case TIMER1_COMPA:
        return (get_data_word(_TIFR1) & (1<<_TIFR1_OCF1A)) != 0;
    case TIMER1_COMPB:
        return (get_data_word(_TIFR1) & (1<<_TIFR1_OCF1B)) != 0;
    case TIMER1_OVF:
        return (get_data_word(_TIFR1) & (1<<_TIFR1_TOV1)) != 0;
    case TIMER0_COMPA:
        return (get_data_word(_TIFR0) & (1<<_TIFR0_OCF0A)) != 0;
    case TIMER0_COMPB:
        return (get_data_word(_TIFR0) & (1<<_TIFR0_OCF0B)) != 0;
    case TIMER0_OVF:
        return (get_data_word(_TIFR0) & (1<<_TIFR0_TOV0)) != 0;
    case SPI_STC:
        return (get_data_word(_SPSR) & (1<<_SPSR_SPIF)) != 0;
    case USART_RX:
        return (get_data_word(_UCSR0A) & (1<<_UCSR0A_RXC0)) != 0;
    case USART_UDRE:
        return (get_data_word(_UCSR0A) & (1<<_UCSR0A_UDRE0)) != 0;
    case USART_TX:
        return (get_data_word(_UCSR0A) & (1<<_UCSR0A_TXC0)) != 0;
    case ADC:
        return (get_data_word(_ADCSRA) & (1<<_ADCSRA_ADIF)) != 0;
    case EE_READY:
        //return get_data_word(_EECR) | (1<<_EECR_EERIE) != 0;
        /**
         * @todo check if this is correct
        */
    case ANALOG_COMP:
        return (get_data_word(_ACSR) & (1<<_ACSR_ACI)) != 0;
    case TWI:
        return (get_data_word(_TWCR) & (1<<_TWCR_TWINT)) != 0;
    case SPM_READY:
        //return get_data_word(_SPMCSR) | (1<<_SPMCSR_SPMIE) != 0;
        /**
         * @todo check if this is correct
        */
    default:
        return 0;
    }
}

void AVR::set_int_flag(Interrupt interrupt){
    (void)interrupt;    // To avoid parameter unused warning
}

uint8_t AVR::get_int_mask(Interrupt interrupt){
    (void)interrupt;    // To avoid parameter unused warning
    return 0;
}

void AVR::set_int_mask(Interrupt interrupt){
    (void)interrupt;    // To avoid parameter unused warning
}

void AVR::set_breakpoint(uint16_t bpc){
    breakpoints[bpc] = program[bpc];
    program[bpc] = 0b1001010110011000;
}

void AVR::remove_breakpoint(uint16_t bpc){
    program[bpc] = breakpoints[bpc];
    breakpoints.erase(bpc);
}

void AVR::toggle_breakpoint(uint16_t bpc){
    if(breakpoints.contains(bpc)){
        remove_breakpoint(bpc);
    }else{
        set_breakpoint(bpc);
    }
}

bool AVR::handle_breakpoint(){

    if(breakpoints.contains(pc)){
        Operation op = Operation(breakpoints[pc]);
        exeop(&op);
        return true;
    }
    return false;
}

std::map<uint16_t, uint16_t> *AVR::get_breakpoints(){
    return &breakpoints;
}
