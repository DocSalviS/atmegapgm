
PACKAGE := atmegapgm
VERSION := 1.1
prefix := /usr

INC_PATH := -Ilibs/slogpp/src -Iinclude
# added /usr/lib/x86_64-linux-gnu for ncurses
LIB_PATH := -Llibs/slogpp
LIBS := -lslogpp -lncurses -lpthread
CXX := g++
CXXFLAGS := -std=c++20 -Wall -Wextra -pedantic -g
MAKE := make
TARGET := atmegapgm
SRC_DIR := src
WEB_DIR := web
WASM_DIR := $(WEB_DIR)/js
WEB_JS := $(WASM_DIR)/avr.js
WEB_WASM := $(WASM_DIR)/avr.wasm
TEST_DIR := examples
INC_DIR := include
OBJ_DIR := obj
DEB_DIR := tmp_deb
FILES_DIR := files
SRCS := $(wildcard $(SRC_DIR)/*.cpp)
OBJS := $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRCS))
ORIGDIR := $(PACKAGE)-$(VERSION)
ORIGTAR := $(DEB_DIR)/$(PACKAGE)_$(VERSION).orig.tar.bz2


all: $(TARGET)

web: $(WASM_DIR)/avr.js

test:
	$(MAKE) -C $(TEST_DIR) all

# Dipendenze tra header files
$(INC_DIR)/avr.h: $(INC_DIR)/operation.h $(INC_DIR)/registers.h $(INC_DIR)/rwoperations.h
$(INC_DIR)/basiciocomponent.h: $(INC_DIR)/component.h $(INC_DIR)/ioportelement.h
$(INC_DIR)/commcomponent.h: $(INC_DIR)/component.h
$(INC_DIR)/datacomponent.h: $(INC_DIR)/component.h $(INC_DIR)/rwoperations.h
$(INC_DIR)/ioportelement.h: $(INC_DIR)/element.h
$(INC_DIR)/pgmcomponent.h: $(INC_DIR)/component.h
$(INC_DIR)/regcomponent.h: $(INC_DIR)/component.h
$(INC_DIR)/step_tab.h: $(INC_DIR)/tab.h $(INC_DIR)/avr.h $(INC_DIR)/datacomponent.h $(INC_DIR)/commcomponent.h $(INC_DIR)/pgmcomponent.h $(INC_DIR)/regcomponent.h $(INC_DIR)/basiciocomponent.h $(INC_DIR)/rwoperations.h
$(INC_DIR)/tab.h: $(INC_DIR)/component.h
$(INC_DIR)/terminal.h: $(INC_DIR)/tab.h $(INC_DIR)/term_colors.h

#dipendemze dei sorgenti
$(SRC_DIR)/avr.cpp: $(INC_DIR)/avr.h ./libs/slogpp/src/slogpp.h
$(SRC_DIR)/basiciocomponent.cpp: $(INC_DIR)/basiciocomponent.h
$(SRC_DIR)/commcomponent.cpp: $(INC_DIR)/commcomponent.h
$(SRC_DIR)/component.cpp: $(INC_DIR)/component.h
$(SRC_DIR)/datacomponent.cpp: $(INC_DIR)/datacomponent.h  $(INC_DIR)/term_colors.h
$(SRC_DIR)/element.cpp: $(INC_DIR)/element.h
$(SRC_DIR)/ioportelement.cpp: $(INC_DIR)/ioportelement.h
$(SRC_DIR)/atmegapgm.cpp: $(INC_DIR)/operation.h $(INC_DIR)/avr.h ./libs/slogpp/src/slogpp.h $(INC_DIR)/terminal.h $(INC_DIR)/step_tab.h $(INC_DIR)/tab.h
$(SRC_DIR)/operation.cpp: $(INC_DIR)/operation.h ./libs/slogpp/src/slogpp.h
$(SRC_DIR)/pgmcomponent.cpp: $(INC_DIR)/pgmcomponent.h $(INC_DIR)/operation.h $(INC_DIR)/regcomponent.h
$(SRC_DIR)/step_tab.cpp: $(INC_DIR)/step_tab.h
$(SRC_DIR)/tab.cpp: $(INC_DIR)/tab.h
$(SRC_DIR)/terminal.cpp: $(INC_DIR)/terminal.h
$(SRC_DIR)/serialcomponent.cpp: $(INC_DIR)/serialcomponent.h $(INC_DIR)/element.h $(INC_DIR)/avr.h

$(TARGET): $(OBJS) libs/slogpp/libslogpp.a
	$(CXX) $(CXXFLAGS) $(LIB_PATH) -o $@ $^ $(LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp | $(OBJ_DIR)
	$(CXX) $(CXXFLAGS) $(INC_PATH) -c -o $@ $<

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(WASM_DIR):
	mkdir -p $(WASM_DIR)

$(WASM_DIR)/avr.js: $(SRC_DIR)/avr.cpp | $ $(SRC_DIR)/operation.cpp $(WASM_DIR)
	emcc -std=c++20 -D WEB_BUILD -lembind src/avr.cpp libs/slogpp/src/slogpp.cpp src/operation.cpp -Iinclude -Ilibs/slogpp/src -o $(WASM_DIR)/avr.js

libs/slogpp/libslogpp.a: libs/slogpp
	$(MAKE) -C libs/slogpp

$(TEST_DIR)/$(T).bin:
	$(MAKE) -C $(TEST_DIR) $(T).bin

install: $(TARGET)
	mkdir -p $(DESTDIR)$(prefix)/share/doc/atmegapgm/examples
	install -D atmegapgm $(DESTDIR)$(prefix)/bin/atmegapgm
	strip $(DESTDIR)$(prefix)/bin/atmegapgm
	install -D scripts/avrdude-wrapper $(DESTDIR)$(prefix)/bin/avrdude-wrapper
	install -D scripts/makefile-adjust $(DESTDIR)$(prefix)/bin/makefile-adjust
	install -D asm_include/atmega_registers.hs $(DESTDIR)$(prefix)/share/atmegapgm/include/atmega_registers.hs
	chmod a-x $(DESTDIR)$(prefix)/share/atmegapgm/include/atmega_registers.hs
	install -D docs/atmegapgm.1 $(DESTDIR)$(prefix)/share/man/man1/atmegapgm.1
	install -D docs/avrdude-wrapper.1 $(DESTDIR)$(prefix)/share/man/man1/avrdude-wrapper.1
	install -D docs/makefile-adjust.1 $(DESTDIR)$(prefix)/share/man/man1/makefile-adjust.1
	cp -r examples/* $(DESTDIR)$(prefix)/share/doc/atmegapgm/examples

uninstall:
	rm -f $(DESTDIR)$(prefix)/bin/atmegapgm $(DESTDIR)$(prefix)/bin/avrdude-wrapper $(DESTDIR)$(prefix)/bin/makefile-adjust
	rm -rf $(DESTDIR)$(prefix)/share/man1/atmegapgm.1 $(DESTDIR)$(prefix)/share/man1/avrdude-wrapper.1
	rm -rf $(DESTDIR)$(prefix)/share/man1/atmegapgm.1 $(DESTDIR)$(prefix)/share/man1/makefile-adjust.1
	rm -rf $(DESTDIR)$(prefix)/share/doc/atmegapgm/
	rm -rf $(DESTDIR)$(prefix)/share/atmegapgm/

deb: $(TARGET)
	utils/mkdeb $(PACKAGE) $(DEB_DIR) $(ORIGDIR) $(FILES_DIR)

clean:
	make -C examples clean
	if [ -d $(ORIGDIR) ]; then rm -rf $(ORIGDIR); fi
	if [ -f $(ORIGTAR) ]; then rm -f $(ORIGTAR); fi
	if [ -f $(WEB_JS) ]; then rm -f $(WEB_JS); fi
	if [ -f $(WEB_WASM) ]; then rm -f $(WEB_WASM); fi
	rm -rf $(OBJ_DIR)/* $(DEB_DIR)/* $(TARGET)
	$(MAKE) -C $(TEST_DIR) clean
	$(MAKE) -C libs/slogpp clean

.PHONY: all clean test
